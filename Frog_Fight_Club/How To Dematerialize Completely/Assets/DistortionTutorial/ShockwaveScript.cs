﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockwaveScript : MonoBehaviour {

	public float x = 0.01f; 
	public float y = 0.01f; 
	public float z = 0.01f;

	public float ratio = 5f;
	public float speed;

	float time;

	void Start () 
	{

	}
	
	
	void Update ()
    {
		time = Time.deltaTime * speed;

		transform.localScale += new Vector3(x, y, z) * speed;
        
		if(transform.localScale.x > ratio)
        {
			x = 0.01f; 
			y = 0.01f; 
			z = 0.01f;

			transform.localScale = new Vector3(x, y, z);
        }
		
	}
}
    