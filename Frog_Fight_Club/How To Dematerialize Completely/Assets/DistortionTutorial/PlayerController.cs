﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	// Set all Layers of the Character to a Layer Called "Character"
	// Set the Sphere to a Layer Called "Ball" or whataver you like
	// in the Physics Manager (Edit>ProjectSettings>Physics) make sure that the "Character" layer is not colldiging witht the "Ball" Layer
	//

	// some things to look at:

	// the Joint and Sphere gameobject inside the character, its got a config. joint - pay attention to its settings and limits
	// this script is placed on the sphere
	// on the head and chest is a constant force up component
	// the sphere got a pretty high mass of 50
	// the sphere and joint got a drag value of 1

	public float speed;
	private Rigidbody rb;
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
	}
	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * speed);
	}
}