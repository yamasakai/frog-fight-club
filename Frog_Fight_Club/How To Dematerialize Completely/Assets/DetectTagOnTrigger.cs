﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectTagOnTrigger : MonoBehaviour
{

    public string tagToDetect = "Enemy";
    public Transform enemyTransform;
    public bool didTouchTag;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == tagToDetect)
        {
            didTouchTag = true;


            enemyTransform = other.transform;

        }
        else
        {
            didTouchTag = false;
           
        }
        Debug.Log("Did Tongue Touch Enemy: " + didTouchTag);
    }


}
