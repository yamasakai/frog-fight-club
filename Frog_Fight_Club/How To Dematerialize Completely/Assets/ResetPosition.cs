﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour {

    public GameObject player;
    public GameObject resetPoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
	
        if(Input.GetKeyDown(KeyCode.R))
        {
            player.transform.position = resetPoint.transform.position;
        }
	}
}
