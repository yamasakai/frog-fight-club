﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TongueShot : MonoBehaviour
{

    public LineRendererDrag lineRendererDrag;
    public GameObject hookStartPoint;
    public PlayerMoveOnDrag player;
    public Transform hand;

    public LineRenderer lineRenderer;
    public LayerMask layerMask;

    
    public RaycastHit hit;

    public bool isFlying;
    
    public Vector3 lockPos;

    public int maxDistance;
    public float speed;

   



    // Use this for initialization
    void Start ()
    {
        //Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(lineRendererDrag.isRelease)//(Input.GetKey(KeyCode.Space))
        FindSpot();

        if (isFlying)
            Flying();

        if(Input.GetKey(KeyCode.S))
        {
            ResetHookShot();
        }
        
	}

    public void ResetHookShot()
    {
        if (isFlying)
        {
            isFlying = false;
            player.canMove = true;
            lineRenderer.enabled = false;
        }
    }

    public void FindSpot()
    {
        if(Physics.Raycast(hookStartPoint.transform.position, hookStartPoint.transform.forward, out hit, maxDistance, layerMask))
        {
            
            isFlying = true;
            lockPos = hit.point;

            //Player shouldn't be able to move
            player.canMove = false;

            lineRenderer.enabled = true;
            lineRenderer.SetPosition(1, lockPos);

        }

        Debug.DrawRay(hookStartPoint.transform.position, hookStartPoint.transform.forward, Color.red);
    }

    /// <summary>
    /// Will make sure that the player will be flying toward the position that was hooked
    /// </summary>
    public void Flying()
    {
        //transform.position = Vector3.Lerp(transform.position, lockPos, speed * Time.deltaTime / Vector3.Distance(transform.position, lockPos) );
        //Helper.EaseLerpAToB();
        iTweenExtensions.MoveTo(this.gameObject, lockPos, speed, 0, EaseType.spring);
        lineRenderer.SetPosition(0, hand.position);

        if(Vector3.Distance(transform.position, lockPos) < 0.5f)
        {
            isFlying = false;
            player.canMove = true;
            lineRenderer.enabled = false;
        }

    }

}
