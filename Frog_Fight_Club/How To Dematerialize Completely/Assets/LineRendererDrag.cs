﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererDrag : MonoBehaviour
{
    public GameObject destinationStart;
    //public ShootTongue shootTongueScript;
    public bool isDragging;
    public bool isRelease;
    public bool isTongueReset;
    //public float timeToBlockDrag = 1f;
    public float minUnitToDetectDrag = 3;
    public LineRenderer lineDrag;
    public GameObject objAtClickedPos;
    public float lineMaxLenght = 10.0f;
    public Vector3 originalMousePosOnClick;
    public Vector3 currentMousePosOnClick;
    public Vector3 lastMousePosOnClick;
    public Vector3 previousMousePosOnClick;

    private void Init()
    {
        if (lineDrag != null)
            lineDrag.enabled = false;
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        //Debug.Log("Distance: " + Vector3.Distance(originalMousePosOnClick, currentMousePosOnClick));

        if (Vector3.Distance(originalMousePosOnClick, currentMousePosOnClick) == 0)
        {
            isDragging = false;
        }
        else if (Vector3.Distance(currentMousePosOnClick, originalMousePosOnClick) > minUnitToDetectDrag )
        {
            isDragging = true;
        }
        DrawLineOnDrag();

        if (Input.GetMouseButtonUp(1))
        {
            currentMousePosOnClick = Vector3.zero;
            originalMousePosOnClick = Vector3.zero;

            isDragging = false;

            if (lineDrag != null)
            {
                lineDrag.enabled = false;

            }

            Debug.Log("Right click released...isDragging = false");
        }

        #region this part will reset the tongue to its original position
        if(isTongueReset )//&& !shootTongueScript.didTongueTouchEnemy)
        {
            //MoveToPosition(shootTongueScript.tongueBall.transform, shootTongueScript.tongueBall.transform.position, 1f);
            //shootTongueScript.tongueBall.transform.position = Vector3.Lerp(shootTongueScript.tongueBall.transform.position, destinationStart.transform.position, Time.deltaTime * 5f);
            //   iTweenExtensions.MoveTo(shootTongueScript.tongueBall, destinationStart.transform.position,
            //shootTongueScript.moveDuration, shootTongueScript.delay, shootTongueScript.moveEase);
            //Debug.Log("Tongue goes back to init pos");
        }
        #endregion


        //if(isRelease)
        //StartCoroutine(BlockDrag());

        // Vector2 newPosition = MousePointPos();
        //Vector2 newPosition = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //lineDrag.material.SetTextureOffset("_MainTex", new Vector2(Time.timeSinceLevelLoad * -4f, 0f));
        //lineDrag.material.SetTextureScale("_MainTex", new Vector2(newPosition.magnitude, 1f));
        //lineDrag.SetPosition(0, newPosition);

        //Helper.EaseLerpAToB(shootTongueScript.tongueBall.transform, shootTongueScript.tongueBall.transform.position, destinationStart.transform.position, shootTongueScript.moveDuration);
    }
    
   

    public Vector3 MousePointPos()
    {
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane plane = new Plane(Vector3.up, Vector3.zero);

        // Generate a ray from the cursor position
        Ray rayMousePos = Camera.main.ScreenPointToRay(Input.mousePosition);


        float hitdist = 0.0f;

        // If the ray is parallel to the plane, Raycast will return false.
        if (plane.Raycast(rayMousePos, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            targetPoint = rayMousePos.GetPoint(hitdist);

        }

        return targetPoint;
    }

    public IEnumerator ResetRelease()
    {
        //Should be equal to moveDuration
        yield return new WaitForSeconds(0.15f);
        isTongueReset = true;
      
        if (isRelease)
        isRelease = false;

       

        #region Oooo
        //if(!shootTongueScript.didTongueTouchEnemy )

        //shootTongueScript.ReturnTongueToStartPos(new Vector3(shootTongueScript.startPoint.position.x, 
        //    shootTongueScript.startPoint.position.y, shootTongueScript.startPoint.position.z), 
        //    shootTongueScript.moveDuration, shootTongueScript.delay, shootTongueScript.moveEase);

        // (new Vector3(shootTongueScript.startPoint.position.x, shootTongueScript.startPoint.position.y, shootTongueScript.startPoint.position.z),
        //shootTongueScript.moveDuration, shootTongueScript.delay, shootTongueScript.moveEase);
        // }
        #endregion
    }

    //public IEnumerator BlockDrag()
    //{
        
    //        isDragging = false;

    //        yield return new WaitForSeconds(timeToBlockDrag);

    //        isDragging = true;
        
    //}


    void DrawLineOnDrag()
    {

        RaycastHit hit = new RaycastHit();


        if (Input.GetMouseButtonDown(1))
        {
            originalMousePosOnClick = Input.mousePosition;
            isTongueReset = false;
        }
        else if(Input.GetMouseButtonUp(1))
        {
            if (isDragging)
                isRelease = true;

            isDragging = false;

          

            
            StartCoroutine(ResetRelease());
            
          
        }

        if(Input.GetMouseButton(1))
        {
            isRelease = false;
            currentMousePosOnClick = Input.mousePosition;
            isTongueReset = false;

            #region old shit
            //if (currentMousePosOnClick.magnitude > previousMousePosOnClick.magnitude || currentMousePosOnClick.magnitude < previousMousePosOnClick.magnitude)
            //{
            //    isDragging = true;


            //}
            //else
            //{
            //    isDragging = false;
            //}
            #endregion

            if (lineDrag != null)
            {
                float dist = Vector3.Distance(objAtClickedPos.transform.position, MousePointPos());


                if (dist > lineMaxLenght)
                    return;

                else
                {

                    if (isDragging)
                    {
                        ///set positions of lines

                        lineDrag.enabled = true;

                        Physics.Raycast(objAtClickedPos.transform.position, transform.forward, out hit, dist);

                        //Right here I'm locking the y position of the linerenderer so that it doesn't point down or up
                        lineDrag.SetPosition(0, new Vector3(objAtClickedPos.transform.position.x, objAtClickedPos.transform.position.y, objAtClickedPos.transform.position.z));

                       // lineDrag.SetPosition(1, new Vector3(MousePointPos().x, objAtClickedPos.transform.position.y, MousePointPos().z));
                         
                        if (hit.collider)
                        {

                            lineDrag.SetPosition(1, hit.point);


                        }
                        else
                        {
                            lineDrag.SetPosition(1, new Vector3(MousePointPos().x, objAtClickedPos.transform.position.y, MousePointPos().z));

                        }
                    }

                    
                }

            }
        }
        //else if (Input.GetMouseButtonUp(1))
        //{
        //    isDragging = false;
        //    isRelease = true;
        //    StartCoroutine(ResetRelease());
        //}
    }


    private Vector3 targetPoint;

}
