// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "Wings/Refractive Cloak" {
Properties {
_Color ("Main Color", Color) = (1,1,1,1)
_AddMap ("Base (RGB)", 2D) = "white" {}
_MainTex ("Normalmap", 2D) = "bump" {}
_DistAmt ("Distortion", range (0,12) = 10
_Abberation ("Abberation", range (.9,1.1)) = 1
}
SubShader {
GrabPass { }

Tags { "Queue"="Transparent+1" "IgnoreProjector"="True" "RenderType"="Opaque" }
LOD 200

CGPROGRAM
#pragma exclude_renderers gles
#pragma vertex vert
#pragma surface surf BlinnPhong
#include "UnityCG.cginc"

float4 _Color;
float _Abberation;
sampler2D _AddMap;
sampler2D _MainTex;
sampler2D _GrabTexture;
float _DistAmt;
float4 _GrabTexture_TexelSize;

struct Input {
float2 uv_AddMap;
float2 uv_MainTex;
float4 proj : TEXCOORD;
};

void vert (inout appdata_full v, out Input o) {
float4 oPos = UnityObjectToClipPos(v.vertex);
#if UNITY_UV_STARTS_AT_TOP
float scale = -1.0;
#else
float scale = 1.0;
#endif
o.proj.xy = (float2(oPos.x, oPos.y*scale) + oPos.w) * 0.5;
o.proj.zw = oPos.zw;
}

void surf (Input IN, inout SurfaceOutput o) {
half3 nor = UnpackNormal(tex2D(_MainTex, IN.uv_MainTex));
half4 tex = tex2D(_AddMap, IN.uv_AddMap);

float2 offset = nor * _DistAmt * _GrabTexture_TexelSize.xy;
float2 offset2 = nor * _DistAmt * ((_GrabTexture_TexelSize.xy-.5)*_Abberation+.5);


IN.proj.xy = offset * IN.proj.z + IN.proj.xy;
half4 col = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(IN.proj));
IN.proj.xy = offset2 + IN.proj.xy;
half4 abberTex = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(IN.proj));

o.Emission = half3(abberTex.r, col.g, col.b+.001)+(tex.rgb*_DistAmt/12;
o.Normal = nor.rgb;
o.Alpha = _Color.a*tex.a;

}
ENDCG
}
FallBack "Diffuse"
}