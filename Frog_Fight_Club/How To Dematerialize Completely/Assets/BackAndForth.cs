﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Call this class firstBackAndForth or FistAndTongue. Prolly the latter...
/// </summary>
public class BackAndForth : MonoBehaviour
{
    public LineRendererDrag lineRendererDrag;
    public bool canPunch;
    public GameObject otherFist;
    //public bool isThisFirstFist;
    //public float delayBeforePunch;
    public bool didReachFinalDestination = false;
    public Transform originDestination;
    public Transform finalDestination;
    private Vector3 fromPos;
    private Vector3 toPos;
    public float pingPongLenght = 1f;
    public float secondsForOneLength = 1f;

    void Start()
    {
        fromPos = transform.position;
        toPos = finalDestination.position;
    }

    void FixedUpdate()
    {
        if (canPunch && !lineRendererDrag.isDragging)
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {


                transform.position = Vector3.Lerp(originDestination.position, finalDestination.position, Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.time / secondsForOneLength, pingPongLenght)));
                Debug.Log(Mathf.PingPong(Time.time / secondsForOneLength, 1f));

            }
            else if (Input.GetKeyUp(KeyCode.Mouse1))
            {

                transform.position = Vector3.Lerp(finalDestination.position, originDestination.position, Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.time / secondsForOneLength, pingPongLenght)));
            }
            else
            {

                transform.position = Vector3.Lerp(transform.position, originDestination.position, Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.time / secondsForOneLength, pingPongLenght)));
            }
        }
    }

    #region og coroutine throwfist
    /// <summary>
    /// This would work nicely if we didn't have to hold the button but tap instead
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="destination"></param>
    /// <returns></returns>
    //IEnumerator ThrowFirst(Transform origin, Transform destination)
    //{
    //    yield return new WaitForSeconds(delayBeforePunch);

    //    Debug.Log("Punching after delay!!");
    //    transform.position = Vector3.Lerp(origin.position, destination.position, Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.time / secondsForOneLength, 1f)));

    //}
    #endregion

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Crosshair")
        {
            Debug.Log("Touch In!!!");
            didReachFinalDestination = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Crosshair")
        {
            Debug.Log("Touch Out!!!");
            didReachFinalDestination = false;
        }
    }
}
