﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveOnDrag : MonoBehaviour
{

    public GameObject playerChild;
    public float smooth;
    public float speed;
    public Vector3 amount;
    public bool canMove;
    public bool jumpWalk;
    Vector3 newPos;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        RotationControl();

    }

    //Orientation with the mouse. Work with isometric now HAAAAAAN.
    public void MovementControl()
    {

        #region Using RigidBodyMovement
        //We are using GetAxisRaw to avoid the sliding when we press a moving buttons
        //movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        #region AddForce Movement
        //GetComponent<Rigidbody>().AddForce(movement * speed / Time.deltaTime);
        #endregion

        //Vector3 smootMove = new Vector3(Mathf.Lerp(0, movement.x, smooth), 0, Mathf.Lerp(0, movement.z, smooth));

        //GetComponent<Rigidbody>().velocity = Vector3.ClampMagnitude(smootMove, 1) * speed;


        ////We are using SmoothDamp to smooth out the movement, as we stop pressing the velocity will deaccelerate
        //if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
        //{    
        //    GetComponent<Rigidbody>().velocity = new Vector3(Mathf.SmoothDamp(GetComponent<Rigidbody>().velocity.x, 0, 

        //        ref walkDeaccelerationOnX,      
        //        walkDeacceleration),       
        //        0,       
        //        Mathf.SmoothDamp(GetComponent<Rigidbody>().velocity.z, 0,  
        //        ref walkDeaccelerationOnZ,
        //        walkDeacceleration));
        //}
        #endregion

        //Character Controller Movement
        // m_controller.Move(movement * speed);


        #region Isometric Movement

        #region Explanation
        //For the camera position set-up, I had to isometrically go down. 
        //That means moving negatively in both X and Z axes by the same amount.
        //Moving up is just the opposite. Moving right means moving positively on the X axis and negatively on the Z axis 
        //by the same amount. As you may have already guessed, moving left is the opposite to moving right.  
        //As you can see, it  just means rotating the movement vector 45 degrees (thanks math for so much fun). 
        //In a nutshell:

        //Up: translate (1, 0, 1).
        //Down: translate (-1, 0, -1).
        //Right: translate (1, 0, -1).
        //Left: translate (-1, 0, 1).
        #endregion

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
        {
            //if (percReach == 1)
            //{
            //    lerpTime = 1;

            //    currentLerpTime = 0;
            //    firstInput = true;
            jumpWalk = true;
            // }
        }
        else if (!Input.GetKey(KeyCode.W) || !Input.GetKey(KeyCode.A) || !Input.GetKey(KeyCode.D) || !Input.GetKey(KeyCode.S))
        {
            jumpWalk = false;
        }


        if (Input.GetKey(KeyCode.W))//&& !isJumping)
        {
            amount.x += amount.z = 1f;
            // amount = new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z + 1f);
            //Debug.Log("W: " + amount);
        }

        if (Input.GetKey(KeyCode.S)) //&& !isJumping)
        {
            amount.x += amount.z = -1f;
            //amount = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z - 1f);
            // Debug.Log("S: " + amount);
        }

        if (Input.GetKey(KeyCode.A)) //&& !isJumping)
        {
            amount.x += -1;
            amount.z += 1;
            // amount = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z + 1f);
            // Debug.Log("A: " + amount);
        }

        if (Input.GetKey(KeyCode.D))// && !isJumping)
        {
            amount.x += 1;
            amount.z += -1;

            //amount = new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z - 1f);

            // Debug.Log("D: " + amount);
        }

        // Normalize to prevent high speed diagonals
        float length = Mathf.Sqrt((amount.x * amount.x) + (amount.z * amount.z));

        if (length != 0)
        {
            amount.x /= length;
            amount.z /= length;
        }

        amount.x = Mathf.Clamp(amount.x, -1, 1);
        amount.z = Mathf.Clamp(amount.z, -1, 1);

        #region (Clamping amount) this is clamp underneath
        //amount.x = amount.x < 0 ? -1 : (amount.x > 0 ? 1 : 0);
        //amount.z = amount.z < 0 ? -1 : (amount.z > 0 ? 1 : 0);
        #endregion

        //if (firstInput == true)
        //{
        //    currentLerpTime += Time.deltaTime * smooth;

        //    percReach = currentLerpTime / lerpTime;

        //    transform.position = Vector3.Lerp(transform.position, transform.position + amount, percReach);

        transform.position = Vector3.Lerp(transform.position, transform.position + amount * speed, smooth * Time.fixedDeltaTime);


        //    if (percReach > 0.8f)
        //    {
        //        percReach = 1;
        //    }

        //    if (Mathf.Round(percReach) == 1)
        //    {
        //        jumpWalk = false;
        //        firstInput = false;
        //    }
        //}

        #endregion

        // yield return null;
    }

    void makeCharacterWalk()
    {
        CharacterController controller = GetComponent<CharacterController>();

        Vector3 diff = transform.TransformDirection(newPos - transform.position);
        controller.SimpleMove(diff * speed);
    }

    public void RotationControl()
    {
        //mousePos = Input.mousePosition;
        //float rotSpeed = 5;

        //amount = Vector3.zero;

        //#region other way of rotating the character using the mouse
        ////Other way of rotating the character using the mouse
        ////		mousePos = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.transform.position.y - transform.position.y +5f));
        ////
        ////	
        ////		m_targetRotation = Quaternion.LookRotation(mousePos - new Vector3(transform.position.x, transform.position.y,  transform.position.z));
        ////		
        ////
        ////		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, m_targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime );
        //#endregion


        #region Mouse Rotate Player
        //SMOOTH WAY of rotating
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, playerChild.transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;

        RaycastHit hit = new RaycastHit();

        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion m_targetRotation = Quaternion.LookRotation(targetPoint - playerChild.transform.position);


            if (Input.GetKey(KeyCode.Mouse1))
            {
                // Smoothly rotate towards the target point.
                //playerChild.transform.rotation = Quaternion.Slerp(playerChild.transform.rotation, m_targetRotation, rotSpeed * Time.fixedDeltaTime); // WITH SPEED
                transform.rotation = Quaternion.Slerp(transform.rotation, m_targetRotation, 1); // WITHOUT SPEED!!!
            }
           

        }

        #endregion
      

    }
}
