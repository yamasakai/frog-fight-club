// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "SickShader" 
{
	Properties 
	{
	    _Color ("Main Color", Color) = (1,1,1,1)
	    _MainTex ("Texture", 2D) = "white" {}
	}

	SubShader 
	{
		Tags 
		{ 
			"RenderType" = "Opaque"
			"Queue" = "Geometry" 
		}
	    
	    Pass 
	    {
			Cull off
	        Blend SrcAlpha OneMinusSrcAlpha 
	        Zwrite on
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			#pragma multi_compile_fwdbase
			
			float4 _Color;
			sampler2D _MainTex;
				
			struct v2f 
			{
			    float4  pos : SV_POSITION;
			    float2  uv : TEXCOORD0;
			    float4 	posWorld : TEXCOORD1;
			};
			
			float4 _MainTex_ST;
			
			v2f vert ( appdata_base v )
			{
			    v2f o;
			    o.pos = UnityObjectToClipPos (v.vertex);
			    o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
			    o.posWorld = mul(unity_ObjectToWorld, v.vertex);
	
			    return o;
			}
			
			half4 frag ( v2f i ) : COLOR
			{
			    half4 texcol = tex2D (_MainTex, i.uv);
			    
			   	//float dist = (1.05 - (length(i.posWorld.xyz - _WorldSpaceCameraPos.xyz) * .005));
			   	
			   	//_Color.r *= clamp(dist,.125,1);
			   	//_Color.g *= clamp(dist,.125,1);
			   	//_Color.b *= clamp(dist * 1.3,.125,1);
			   	
			    return texcol * _Color;
			}
			ENDCG
	    }
	}
	Fallback "VertexLit"
} 