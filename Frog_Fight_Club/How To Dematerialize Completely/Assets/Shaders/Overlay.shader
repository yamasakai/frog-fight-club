// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Overlay" 
{
	Properties 
	{
	    _Color ("Main Color", Color) = (1,1,1,1)
	}

	SubShader 
	{
		Tags 
		{ 
			"Queue" = "Overlay" 
			"RenderType" = "Transparent"
		}
	    
	    Pass 
	    {
			Cull off
	        Blend SrcAlpha OneMinusSrcAlpha 
	        Zwrite on
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#pragma target 3.0
			
			float4 _Color;
			
			struct v2f 
			{
			    float4 vertex : SV_POSITION;
			};
			
			v2f vert ( appdata_base v )
			{
			    v2f o;
			    o.vertex = UnityObjectToClipPos(v.vertex);
			    return o;
			}
			
			half4 frag ( v2f i ) : COLOR
			{
				fixed4 col = _Color;
			    return col;
			}
			ENDCG
	    }
	}
	Fallback "VertexLit"
} 
