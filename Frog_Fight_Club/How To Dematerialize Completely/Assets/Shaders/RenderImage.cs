﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RenderImage : MonoBehaviour 
{
	public Shader curShader;
	public float brightnessAmount = 1f;
	public float saturationAmount = 1f;
	public float contrastAmount = 1f;
	private Material curMaterial;

	Material material
	{
		get
		{
			if ( curMaterial == null )
			{
				curMaterial = new Material(curShader);
				curMaterial.hideFlags = HideFlags.HideAndDontSave;	
			}
			return curMaterial;
		}
	}
	
	void Start () 
	{
		if ( !SystemInfo.supportsImageEffects )
		{
			enabled = false;
			return;
		}
	}
	
	void OnRenderImage ( RenderTexture sourceTexture, RenderTexture destTexture )
	{
		if ( curShader != null )
		{
			material.SetFloat("_BrightnessAmount",brightnessAmount);
			material.SetFloat("_SaturationAmount",saturationAmount);
			material.SetFloat("_ContrastAmount",contrastAmount);
			Graphics.Blit(sourceTexture,destTexture,material);
		}
		else
		{
			Graphics.Blit(sourceTexture,destTexture);	
		}
	}

	void Update () 
	{
		brightnessAmount = Mathf.Clamp(brightnessAmount,0f,10f);
		saturationAmount = Mathf.Clamp(saturationAmount,0f,10f);
		contrastAmount = Mathf.Clamp(contrastAmount,0f,10f);
	}
	
	void OnDisable ()
	{
		if ( curMaterial )
			DestroyImmediate(curMaterial);	
	}
}
