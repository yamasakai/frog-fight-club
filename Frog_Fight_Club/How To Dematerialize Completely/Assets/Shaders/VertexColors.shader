﻿Shader "Custom/VertexColors" 
{
	Subshader 
	{
	    BindChannels 
	    {
	        Bind "vertex", vertex
	        Bind "color", color 
	    }
	    
	    Tags 
	    {
			"Queue" = "Transparent" 
			"RenderType" = "Transparent"
	    }
	    
	    Pass 
	    {
			Cull off
	        Blend SrcAlpha OneMinusSrcAlpha 
	        Zwrite on
	    }
	}
}
