﻿using UnityEngine;
using System.Collections;

public class Shuriken : MonoBehaviour 
{

	public LayerMask collisionMask;
	public float speed = 15;
	public float rotSpeed = 800;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
		transform.Translate(new Vector3(1,0,0) * speed * Time.deltaTime);
		//transform.Rotate(Vector3.up * Time.deltaTime * rotSpeed );

		Ray ray = new Ray(transform.position, transform.right);

		RaycastHit hit;

		if(Physics.Raycast(ray, out hit, speed * Time.deltaTime + 0.5f, collisionMask))
		{
			Vector3 reflectDir = Vector3.Reflect(ray.direction, hit.normal);
			float rot =  Mathf.Atan2(reflectDir.z, reflectDir.x) * Mathf.Rad2Deg; //90 - mathf if ur in 3d
			transform.eulerAngles = new Vector3(0, rot, 0);
		}
	}
}
