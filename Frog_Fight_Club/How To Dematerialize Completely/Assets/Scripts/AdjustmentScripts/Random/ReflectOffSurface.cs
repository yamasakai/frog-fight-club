﻿using UnityEngine;
using System.Collections;

public class ReflectOffSurface : MonoBehaviour
{
    [Space(3)]
    public LayerMask collisionMask;


    [Space(3)]
    [Range(0, 100)]
    public float speed = 15;
   
    [Space(3)]
    [Range(0, 100)]
    public float startRandXMin;

    [Range(0, 100)]
    public float startRandXMax;

    [Range(0, 100)]
    public float startRandYMin;

    [Range(0, 100)]
    public float startRandYMax;

    //[Header("Juicy")]
    //[Space(10)]
    //public EaseType stopEase;
    //[Range(0, 100)]
    //public float stopTweenDuration;
    //[Range(0, 100)]
    //public float stopDuration;
    //public Vector3 stopScale = Vector3.one;
    
    private Vector2 randomVec = Vector2.zero;
    //private bool didCollide = false;
   

    void Start()
    {
        randomVec = new Vector2(Random.RandomRange(startRandXMin, startRandXMax), 
            Random.RandomRange(startRandYMin, startRandYMax));

        GetComponent<Rigidbody2D>().velocity = randomVec * speed;
    }

    void Update()
    {
        
       
    }

    float hitFactor(Vector2 ballPos, Vector2 racketPos,
                float racketHeight)
    {
        // ascii art:
        // ||  1 <- at the top of the racket
        // ||
        // ||  0 <- at the middle of the racket
        // ||
        // || -1 <- at the bottom of the racket
        return (ballPos.y - racketPos.y) / racketHeight;
    }

    //IEnumerator Scaling()
    //{
    //    while (true)
    //    {
    //        if (stopTweenDuration > 0)
    //            StartCoroutine(transform.ScaleFrom(stopScale, stopTweenDuration, stopEase));
    //        if (stopDuration > 0)
    //            yield return StartCoroutine(Auto.Wait(stopDuration));
    //    }
    //}

    void OnCollisionTrigger2D(Collision2D other)
    {
        if (other.gameObject)
        {
            
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject)
        {
           
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        float y;
        Vector2 dir;

        switch (other.gameObject.name)
        {
            case "LeftWall":

                // Calculate hit Factor
                y = hitFactor(transform.position,
                other.transform.position,
                other.collider.bounds.size.y);

                // Calculate direction, make length=1 via .normalized
                dir = new Vector2(1, y).normalized;

                // Set Velocity with dir * speed
                GetComponent<Rigidbody2D>().velocity = dir * speed;

                break;

            case "RightWall":

                // Calculate hit Factor
                y = hitFactor(transform.position,
               other.transform.position,
               other.collider.bounds.size.y);

                // Calculate direction, make length=1 via .normalized
                dir = new Vector2(-1, y).normalized;

                // Set Velocity with dir * speed
                GetComponent<Rigidbody2D>().velocity = dir * speed;

                break;

            default:
               
            break;
        }

        #region racket stuff

        // Note: 'col/other' holds the collision information. If the
        // Ball collided with a racket, then:
        //   col.gameObject is the racket
        //   col.transform.position is the racket's position
        //   col.collider is the racket's collider

        //    if (other.gameObject.name == "RacketLeft")
        //    {
        //        // Calculate hit Factor
        //        float y = hitFactor(transform.position,
        //                            other.transform.position,
        //                            other.collider.bounds.size.y);

        //        // Calculate direction, make length=1 via .normalized
        //        Vector2 dir = new Vector2(1, y).normalized;

        //        // Set Velocity with dir * speed
        //        GetComponent<Rigidbody2D>().velocity = dir * speed;
        //    }

        //    // Hit the right Racket?
        //    if (other.gameObject.name == "RacketRight")
        //    {
        //        // Calculate hit Factor
        //        float y = hitFactor(transform.position,
        //                            other.transform.position,
        //                            other.collider.bounds.size.y);

        //        // Calculate direction, make length=1 via .normalized
        //        Vector2 dir = new Vector2(-1, y).normalized;

        //        // Set Velocity with dir * speed
        //        GetComponent<Rigidbody2D>().velocity = dir * speed;
        //    }
        //}

        #endregion

    }
}

