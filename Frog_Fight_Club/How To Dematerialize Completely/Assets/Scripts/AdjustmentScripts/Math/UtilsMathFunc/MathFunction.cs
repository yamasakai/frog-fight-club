﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class MathFunction
{
    public enum FuncType
    {
        Linear,
        InverseSquared,
        Power
    }

    //The type of function to use
    public FuncType Type = FuncType.InverseSquared;

    //The parameters for the function.  Note that not every parameter will be used
    //for every parameter type.
    public float Power = 2.0f;

    public float MinInputValue = 0.0f;
    public float MaxInputValue = 1.0f;

    public float MinOutputValue = 0.0f;
    public float MaxOutputValue = 1.0f;

    public MathFunction()
    {
    }

    public float Evaluate(float value)
    {
        switch (Type)
        {
            case FuncType.Linear:
                return CalcLinear(value);

            case FuncType.InverseSquared:
                return CalcInverseSquared(value);

            case FuncType.Power:
                return CalcPower(value);

            default:
			
                //DebugUtils.LogError("Invalid Function type: {0}", Type);
                return 0.0f;
        }
    }

    //This will decrease linearly starting at MinInputValue and hitting zero at MaxInputValue.
    float CalcLinear(float value)
    {
        float percent = Mathf.InverseLerp(MinInputValue, MaxInputValue, value);

        percent = 1.0f - Mathf.Clamp01(percent);

        return Mathf.Lerp(MinOutputValue, MaxOutputValue, percent);
    }

    float CalcInverseSquared(float value)
    {
        float percent = 1.0f / (value + 1 - MinInputValue);

        percent *= percent;

        percent = Mathf.Clamp01(percent);

        return Mathf.Lerp(MinOutputValue, MaxOutputValue, percent);
    }

    //This will decrease using a power function starting at MinInputValue and hitting zero at MaxInputValue.
    //A higher power will cause the volume to accelerate to zero faster.  
    float CalcPower(float value)
    {
        value = Mathf.Clamp(value, MinInputValue, MaxInputValue);

        float percent = (-value + MaxInputValue) / (MaxInputValue - MinInputValue);

        percent = Mathf.Pow(percent, Power);

        return Mathf.Lerp(MinOutputValue, MaxOutputValue, percent);
    }
}
