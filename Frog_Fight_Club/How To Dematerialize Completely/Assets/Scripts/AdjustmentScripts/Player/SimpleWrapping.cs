﻿using UnityEngine;
using System.Collections;

public class SimpleWrapping : MonoBehaviour 
{
    
    //Public 
    public float leftConstraint = 0.0f;
    public float offsetLeft;
    public float rightConstraint = 0.0f;
    public float offsetRight;
    public float buffer = 1.0f; // set this so the spaceship disappears offscreen before re-appearing on other side
    public float distanceZ = 10.0f;
    public GameObject player;

    //Private
    private float minRunFXRate = 0;
    private ParticleSystem.EmissionModule EmissionTurbo;


    [HideInInspector]
    public float screenWidth;
    public float newPositionX;

    

    void Start() 
    {
        // set Vector3 to ( camera left/right limits, spaceship Y, spaceship Z )
        // this will find a world-space point that is relative to the screen
 
        // using the camera's position from the origin (world-space Vector3(0,0,0)
        //leftConstraint = Camera.main.ScreenToWorldPoint( new Vector3(0.0f, 0.0f, 0 - Camera.main.transform.position.z) ).x;
        //rightConstraint = Camera.main.ScreenToWorldPoint( new Vector3(Screen.width, 0.0f, 0 - Camera.main.transform.position.z) ).x;
  
        // using a specific distance
        leftConstraint = Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).x + offsetLeft;
        rightConstraint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, distanceZ)).x + rightConstraint;

       // EmissionTurbo = player.runFX.emission;
        if (player != null)
        {
           // StartCoroutine(LowerRunFXEmission(player));
        }
         
    }
 
  
    void Update() 
    {
      
          if (transform.position.x < leftConstraint - buffer) 
          {
              if (player != null)
              {
                 // player.isWrapping = true; 
              }

            
                  // ship is past world-space view / off screen
                  newPositionX = rightConstraint + buffer;  // move ship to opposite side
                  transform.position = new Vector3(newPositionX, transform.position.y, transform.position.z);
              
              
          }
         

          if (transform.position.x > rightConstraint + buffer)
          {
              if (player != null)
              {
                 // player.isWrapping = true;
              }

          
                  newPositionX = leftConstraint - buffer;
                  transform.position = new Vector3(newPositionX, transform.position.y, transform.position.z);
              
          }
    }

    //public  IEnumerator LowerRunFXEmission(PlayerTwoD p)
    //{
    //    while (true)
    //    {
    //        if (p.isWrapping)
    //        {
    //            //p.runFX.rate = 
    //            p.isWrapping = true;
                
    //            EmissionTurbo.rate = new ParticleSystem.MinMaxCurve(minRunFXRate);
    //            //p.runFX.rate = new ParticleSystem.MinMaxCurve(minRunFXRate);
    //        }
    //        yield return new WaitForSeconds(0.2f);
    //        p.isWrapping = false;
    //    }
    }



