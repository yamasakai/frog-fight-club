﻿using UnityEngine;
using System.Collections;

public class TouchMoveButton : TouchLogicV2
{
    [Range(-1, 1)]
    public int moveDir = 1;//1=right;-1=left
//    PlayerTwoD player;

    void Start()
    {
       // player =  GameObject.FindObjectOfType<PlayerTwoD>();//This will find our player script, as long as there is only 1 GameObject with "PlayerTwoD" on it
    }

    public override void OnTouchBegan()
    {
        //watch this touch for when it ends anywhere so we can slow down the player
        touch2Watch = currTouch;
    }

    public override void OnTouchMoved()
    {
      
    }

    public override void OnTouchStayed()
    {
       
    }

    public override void OnTouchEndedAnywhere()
    {
        //run this check so other touches ending don't cause player to slow down
       // if (currTouch == touch2Watch)
           //player.Move(0);//do avoid annoying drift after letting go of button
    }
}