﻿using UnityEngine;
using System.Collections;

public class StarPickup : MonoBehaviour 
{
    //public GameObject circleOnImpact;
    public GameObject starPickupBurst;
    //public bool didCollide;
    //public float slowDownAmt;
    //public float slowDownTime;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            print("Player picked a star");

            if (starPickupBurst)
            {
                Instantiate(starPickupBurst, transform.position, Quaternion.identity);
            }

            Destroy(this.gameObject);

            #region OG Star pickup interaction
            //if (other.GetComponent<PlayerTwoD>().isDashing)
            //{
                //didCollide = true;
                //other.GetComponent<PlayerTwoD>().dashResetCount = 0;
                //other.GetComponent<PlayerTwoD>().isDashResetFlag = true;

                //print("Pacman eat star");
               
                //if (starPickupBurst)
                //{
                //    Instantiate(starPickupBurst, transform.position, Quaternion.identity);
                //}

                ////other.GetComponent<PlayerTwoD>().ignoreGravityForXTime = 0.666f;
                //CameraShakeManager.Instance.SmallCameraShake();

                //StartCoroutine(SlowDown(slowDownAmt));
            
                ////print("Slow down is called on starpickup");

                //if (this.gameObject.activeSelf)
                //{
                //    this.gameObject.GetComponent<Collider2D>().enabled = false;
                //    this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                //   // this.gameObject.SetActive(false);
                //  //  print("Object Collider desactivated & sprite renderer");
                //}
             //}
                #endregion
            
        }

    }

    #region Slow down on impact coroutine
    //public IEnumerator SlowDown(float amt)
    //{
       
    //    if (Time.timeScale == 1.0f)
    //    {
    //        Time.timeScale = amt;
    //    }

    //    yield return new WaitForSeconds(slowDownTime);

    //    Time.timeScale = 1.0f;
    //    Time.fixedDeltaTime = 0.02f * Time.timeScale;
    //    this.gameObject.SetActive(false);
    //    Destroy(this.gameObject);

    //}
    #endregion

    void OnEnable()
    {
      
    }


    void OnDisable()
    {
      
        //didCollide = false;
       
    }

    void OnDestroy()
    {
        
    }
        
}
