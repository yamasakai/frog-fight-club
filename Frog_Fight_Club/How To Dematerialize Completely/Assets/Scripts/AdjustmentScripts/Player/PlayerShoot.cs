﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour 
{
    public JuiceOnInteraction juice;
    public Transform shootingPoint;
    public GameObject bulletPrefab;
    public bool isShootAtMouseDir;

    public bool isShooterScaleOnHold = false;
    private float _counter = 0f;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isShootAtMouseDir)
        {
            Plane plane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float hit;
            if (plane.Raycast(ray, out hit))
            {
                Vector3 aimDirection = Vector3.Normalize(ray.GetPoint(hit) - transform.position);
                Quaternion targetRotation = Quaternion.LookRotation(aimDirection);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 360 * Time.deltaTime);

                if (Input.GetMouseButtonDown(0))
                    bulletPrefab.Spawn(shootingPoint.position, shootingPoint.rotation);

                CameraShakeManager.Instance.SmallCameraShake();
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            bulletPrefab.Spawn(shootingPoint.position, Quaternion.identity);
            CameraShakeManager.Instance.SmallCameraShake();

            juice.OnImpact();
        }
        else if (Input.GetMouseButton(0))
        {
            if (isShooterScaleOnHold == false)
            {
                // we add to the counter the delta time and then offset that by the speed with which we want to tween.             
                _counter += Time.deltaTime * juice.scaleToDurationOn;

                if (_counter >= 0.01f)
                {
                    _counter = 0f;
                    juice.OffImpact();
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _counter = 0f;
            juice.OffImpact();
        }
	}

    
}
