﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(LineRenderer))]

public class LaserPointer : MonoBehaviour {

	LineRenderer laser;
	Light flare;
	public float laserPowa = 15.5f;
	//cannot shoot for more than 2.5sec
	private float timer = 0.0f;

	// Use this for initialization
	void Start () 
	{
		laser = gameObject.GetComponent<LineRenderer> ();
		flare = gameObject.GetComponent<Light> ();
		timer = 2.5f;

		laser.enabled = false;
		flare.enabled = false;
		//put the mouse in the middle of the screen. lock it. make it dissapear.
		Screen.lockCursor = true;
	}
	
	// Update is called once per frame
	void Update () 
	{

		timer -= Time.deltaTime;

		if (Input.GetButtonDown ("Fire1")) 
		{
			//keep laser alive. coroutine will take care of that.
			StopCoroutine ("FireLaser");
			StartCoroutine ("FireLaser");
			timer = 2.5f;

		} 

       if (timer <= -1.0f) 
		{
			timer = 2.5f;
		}

	
	}


	IEnumerator FireLaser()
	{
		laser.enabled = true;
		flare.enabled = true;
		//Could keep the laser going for infinity. kukuku~
		while(Input.GetButton("Fire1")&& timer >= 0.0f)
		{
			//UV will be changing. UV are being offsetting.
			laser.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0,Time.time * laserPowa);
			//align with the gun barrel
			Ray ray = new Ray(transform.position, transform.forward);
			//Point of impact
			RaycastHit hit;

			//start of the laser.
			laser.SetPosition(0, ray.origin);

			//pass by ref of the raycast hit
			//if our ray did hit something.
			//hit = info of what we hit.
			if(Physics.Raycast(ray, out hit, 100))
		    {
				//Stop at the point it hit.
				laser.SetPosition(1, hit.point);

				//if target is a rigidBody
				if(hit.rigidbody)
				{
					//Adding 5 unit of force to the direction the laser is going.
					//AddForceAtPosition is way more precise than AddForce(add force to center) because of our ray cast it add force 
					//to the exact point of impact.
					hit.rigidbody.AddForceAtPosition(transform.forward * laserPowa, hit.point);

				}
			}
			else
			{    //100 unit foward. The end.
				laser.SetPosition(1, ray.GetPoint(100));
			}


			//it will loop again thx to this
			yield return null;
		}
		//we are done shooting. Bye laser beam.
		laser.enabled = false;
		flare.enabled = false;
		timer = 2.5f;
	
	}
	
	//@script RequireComponent(LineRenderer)
}
