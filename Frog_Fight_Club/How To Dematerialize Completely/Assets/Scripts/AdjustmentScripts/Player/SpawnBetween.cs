﻿using UnityEngine;
using System.Collections;

public class SpawnBetween : MonoBehaviour 
{
    
    public GameObject obj1, obj2;
    public Color obj1Col, obj2Col;
    public GameObject spawnObj;

	// Use this for initialization
	void Start () 
    {
        ChangeColor();
        SpawnBetweenObj();
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
    
    void ChangeColor()
    {
        obj1.GetComponent<Renderer>().material.color = obj1Col;

        obj2.GetComponent<Renderer>().material.color = obj2Col;
    }

    void SpawnBetweenObj()
    {
        float dist = Vector3.Distance(obj1.transform.position, obj2.transform.position);

        float midDist = dist / 2;

        Vector3 difDist = new Vector3((obj1.transform.position.x + obj2.transform.position.x)/2, obj1.transform.position.y , obj1.transform.position.z);

        GameObject cloneObj = Instantiate(spawnObj, difDist, Quaternion.identity) as GameObject;

        


    }


}
