﻿using UnityEngine;
using System.Collections;
using Flask;

public class PlayerBullet : MonoBehaviour 
{
   
    public Vector3 bulletDir = Vector3.zero;
    public bool isStopAtDist;
    public float shootDistance;
    public float shootSpeed;
    public GameObject impactFX;
    public CameraShake shake;
   
    //public FreezeFrame freeze;

    void OnEnable()
    {
        StartCoroutine(Shoot());
    }

    void OnDisable()
    {
        this.gameObject.transform.localScale = new Vector3(1.15f, 0.65f, 1);
        if (this.GetComponent<iTween>())
        {
            Destroy(this.GetComponent<iTween>());
        }
        StopAllCoroutines();
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    IEnumerator Shoot()
    {
        if (isStopAtDist)
        {
            float travelledDistance = 0;
          
           // iTweenExtensions.ScaleTo(this.gameObject, new Vector3(0.6f, 1.2f, 1f), 0.35f, 0.10f, EaseType.spring); //0.35, 0.15f
           
            while (travelledDistance < shootDistance)
            {
                travelledDistance += shootSpeed * Time.deltaTime;
                transform.position += bulletDir * (shootSpeed * Time.deltaTime);
                yield return 0;
            }
        }
    }


    void OnCollisionEnter2D(Collision2D c)
    {

        switch (c.gameObject.tag)
        {
           
            case "BouncingHazard":
        
            //Spawn a pooled explosion prefab
    
            impactFX.Spawn(transform.position);

            Camera.main.GetComponent<CameraShake>().Shaker(.2f, .15f);

            //CameraShakeManager.Instance.BigCameraShake();

            break;



            case "LevelStructure":

            impactFX.Spawn(transform.position);

            Camera.main.GetComponent<CameraShake>().Shaker(.1f, .05f);

            //CameraShakeManager.Instance.MediumCameraShake();
                  
            break;

            
            default:

                break;
        }
    }


   
}
