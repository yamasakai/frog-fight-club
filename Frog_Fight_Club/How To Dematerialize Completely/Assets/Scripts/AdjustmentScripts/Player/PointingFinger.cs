﻿using UnityEngine;
using System.Collections;

public class PointingFinger : MonoBehaviour 
{
    public JuiceOnInteraction juice;
    public GameObject IndexTip;
    //public bool isOnOffAutoMoverObj;
    public GameObject autoMoverObj;
    public bool isAnimScale;
    public bool didPressDown;
    public bool didPressUp;
    public bool shouldWait;
    public bool isCounterDone;
    public float timeWait = 0.666f;
    private float _counter;

	// Use this for initialization
	void Start () 
    {

        
	}



    IEnumerator OnOffTimer()
    {
        //juice.OffImpact();
        yield return new WaitForSeconds(2.1f);
        

        this.gameObject.GetComponent<Animator>().enabled = true;
    }

    
    GameObject checkCollision()
    {
        //Converting Mouse Pos to 2D (vector2) World Pos/ Use ScreenToWorldPoint for 2D and use ScreenPointToRay for 3D
        Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

        RaycastHit2D hit = Physics2D.Raycast(IndexTip.transform.position, Vector2.zero, 0f);

        //if (hit)
        //{
            if (hit.transform.gameObject.tag == "Zit")
            {
                //hit.transform.GetComponent<Zit>().ZitPop();

               // Debug.Log(" Pointing finger: " + hit.transform.name);
                return hit.transform.gameObject;
            }

            //Debug.Log(" Pointing finger: "+ hit.transform.name);
            //return hit.transform.gameObject;

           
       // }
        else return null;
    }


	// Update is called once per frame
	void Update () 
    {
        
        if (Input.GetMouseButtonDown(0))
        {
           // StopCoroutine(OnOffTimer());
            shouldWait = false;
            didPressUp = false;
            didPressDown = true;
            isCounterDone = false;
           // bulletPrefab.Spawn(shootingPoint.position, Quaternion.identity);
           // CameraShakeManager.Instance.SmallCameraShake();
            if (isAnimScale)
            {
                this.gameObject.GetComponent<Animator>().enabled = false;
            }

            //checkCollision();
            
            juice.OnImpact();

            //didPressDown = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            //_counter = 0f;
            //StartCoroutine(OnOffTimer());
            juice.OffImpact();
            shouldWait = true;
            didPressUp = true;
            didPressDown = false;

        }
        
        if (shouldWait)
        {
            // we add to the counter the delta time and then offset that by the speed with which we want to tween.             
            _counter += Time.deltaTime * juice.scaleToDurationOn;

            //if (shouldWait == true) return;

            isCounterDone = false;

            if (_counter >= timeWait)
            {
                isCounterDone = true;
                shouldWait = false;
                _counter = 0;
            }
        }
         
        if(isCounterDone)
        {
            //shouldWait = false;
            if (isAnimScale)
            {
                this.gameObject.GetComponent<Animator>().enabled = true;
            }
        }

        
        //if (didPressUp)
        //{
        //    StartCoroutine(OnOffTimer());

        //}

        #region garbage
        //if (didPressDown)
        //{
        //    shouldWait = true;

        //     // we add to the counter the delta time and then offset that by the speed with which we want to tween.             
        //        _counter += Time.deltaTime * juice.scaleToDurationOn;

        //        if (_counter >= 1.1f)
        //        {
        //            shouldWait = false;
        //            _counter = 0;
        //        }
        //}
        #endregion

    }
}
