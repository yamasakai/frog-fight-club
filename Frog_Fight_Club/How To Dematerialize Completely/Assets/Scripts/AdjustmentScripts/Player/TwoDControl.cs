﻿using UnityEngine;
using System.Collections;

public class TwoDControl : Entity {

	public float speed = 5.0f;
	public float jumpPower = 10.0f;
	public float dashPower = 12.5f;

	public float groundRadius = 0.2f;
	public float charFaceDir = 1;

	public string axisName = "Horizontal";
	public string jumpButtom = "Jump";

	public bool grounded = false;
	public bool doubleJump = false;
	
	public Animator anim;
	public Rigidbody2D rigidbody2D;
	public Transform groundCheck;
	public LayerMask whatIsGround;

    

	//#TODO Make one array to do that...Just need the name of all audio clips...

	public AudioClip sfxJump;
	public AudioClip sfxDash;
	public AudioClip sfxHurt;


	public GameObject dashFX;
	public Transform dashSpawn;

    //For planets gravity
    public Transform gravitySource; //Planet
    public float gravity = 9.99f;
    public float speedSnap = 1.0f;

	// Use this for initialization
	void Start () {

		//audio = gameObject.GetComponent <AudioClip> ();
		anim = gameObject.GetComponent<Animator>();
		rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
		//SO that our trail renderer appear in the right layer
		TrailRenderer trail = gameObject.GetComponent<TrailRenderer>();
		trail.sortingLayerName = "Character";





	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //Handle planet gravity
        Vector2 gravityVector = (gravitySource.position - transform.position ).normalized * gravity; //Our currentPosition - sourceOfGravity(Planet) 
        rigidbody2D.AddForce(gravityVector);
        transform.rotation = Quaternion.LookRotation(new Vector3(1, 0, 0), new Vector3(gravityVector.x,gravityVector.y, 0));

		//It will create a circle to check what we hit. groundCheck.position = where our circle is generated
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        if (anim)
        {
            anim.SetBool("Ground", grounded);
        }
		//another way to check for ground collision
		//grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")); 

	

		if(grounded)
		{
			doubleJump = false;

			anim.SetBool("dJump", doubleJump);
			//anim.SetBool("Ground", grounded);
		
		}

		// verticalSpeed = how fast we are moving down or up 
		anim.SetFloat("vSpeed", rigidbody2D.velocity.y);

		//if(!grounded) return; //if not grounded you cant control your direction

		float moveDir = Input.GetAxis(axisName);

	    // We use Abs to make sure we Axis stay positive
		anim.SetFloat("Speed", Mathf.Abs(moveDir));
		if(moveDir > 0)
		{
			Vector3 newScale = transform.localScale;
			newScale.x = 1.0f;
			transform.localScale = newScale;
			charFaceDir = 1.0f;


		}
		else if (moveDir < 0)
		{
			Vector3 newScale = transform.localScale;
			newScale.x = -1.0f;
			transform.localScale = newScale;
			charFaceDir = -1.0f;

		}

		//Dash will always goes the direction the player is facing.

		rigidbody2D.velocity = new Vector2(moveDir * speed * Time.deltaTime, rigidbody2D.velocity.y);
		//transform.position += transform.right * moveDir * speed * Time.deltaTime;


	}

	void Update()
	{


		//Grounded = true or DJump = false && jumpButton was pressed = JUMP
		//As long as doubleJump is not true you can jump again.
		if((grounded || !doubleJump)  && Input.GetButtonDown(jumpButtom))
		{

//			grounded = false;
//			anim.SetBool("Ground", grounded);
//
//			doubleJump = false;
//			anim.SetBool("dJump", doubleJump);

			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
			rigidbody2D.AddForce(new Vector2(0, jumpPower));

            if (GetComponent<AudioSource>())
            {
                GetComponent<AudioSource>().clip = sfxJump;
                GetComponent<AudioSource>().volume = 0.5f;
                GetComponent<AudioSource>().Play();
            }

			//If not grounded and did not use double Jump Then you can use double jump
			if(!doubleJump && !grounded)
			{

				rigidbody2D.AddForce(new Vector2(0, jumpPower * 0.888f));

//				grounded = false;
//				anim.SetBool("Ground", grounded);

				doubleJump = true;
                if (anim)
                {
                    anim.SetBool("dJump", doubleJump);
                }
                
            }

		
		}

		Dash(charFaceDir, dashPower, jumpPower);

	}
	

	void Dash(float dir, float dashP, float jumpP )
	{
		//#TODO Make a dash button....
		if(Input.GetKeyDown(KeyCode.Space))
		{
			//Just to make sure
			if(dir == 0){ dir = 1;}
			else if(dir <= -1) { dir = -1; }
            if (GetComponent<AudioSource>())
            {
                GetComponent<AudioSource>().clip = sfxDash;
                GetComponent<AudioSource>().volume = 3f;
                GetComponent<AudioSource>().Play();
            }
			rigidbody2D.AddForce(Vector2.right * dir * dashP * jumpPower);

			GameObject obj = ((GameObject)Instantiate(dashFX, transform.position, transform.rotation));
			//obj.transform.rigidbody2D.AddForce(Vector2.right * dir * dashP * jumpPower);
			Destroy(obj, 2f);
		}

	}





	
}
