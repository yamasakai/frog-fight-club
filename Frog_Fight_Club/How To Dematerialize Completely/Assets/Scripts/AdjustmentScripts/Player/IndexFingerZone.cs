﻿using UnityEngine;
using System.Collections;

public class IndexFingerZone : MonoBehaviour 
{
    [Range(0, 1)]
    public float timeFreeze = .002f;
    float  _counter;
    float _time = 1.0f;
    public JuiceOnInteraction juice;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && GetComponent<Collider2D>())
        {
            GetComponent<Collider2D>().isTrigger = true;

        }
        if (Input.GetMouseButton(0) && GetComponent<Collider2D>())
        {
            _counter += Time.deltaTime * _time;
            if (_counter > 1.0f)
            {
                GetComponent<Collider2D>().isTrigger = false;

                if (juice)
                {
                    juice.OffImpact();
                }

                }
        }
        else if (Input.GetMouseButtonUp(0) && GetComponent<Collider2D>())
        {
            GetComponent<Collider2D>().isTrigger = false;
            _counter = 0;
        }
    }

    public void FreezingFrame()
    {
        Time.timeScale = .001f;

        #region old stuff
        //float pauseEndTime = Time.realtimeSinceStartup + delay;

        //while (Time.realtimeSinceStartup < pauseEndTime)
        //{
        //    yield return null;
        //}

        ////yield return new WaitForSeconds(delay * Time.timeScale);

        ////Time.timeScale = 1;
        #endregion
    }

    private IEnumerator DelayCoroutine()
    {
        FreezingFrame();

        float waitTime = Time.realtimeSinceStartup + timeFreeze;
        yield return new WaitWhile(() => Time.realtimeSinceStartup < waitTime);

        //WakeUp();
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        StartCoroutine(DelayCoroutine());
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.tag == "Zit")
        {
            
            c.transform.GetComponent<Zit>().ZitPop();
            
  
        }
    }
}
