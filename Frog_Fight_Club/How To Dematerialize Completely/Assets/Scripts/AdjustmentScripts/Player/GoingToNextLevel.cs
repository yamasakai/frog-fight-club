﻿using UnityEngine;
using System.Collections;

public class GoingToNextLevel : MonoBehaviour 
{
    //public Transform portalPos;
    private GameManager1 manager;
    public int keyCount = 0;
    public int portalContactCount = 0;
    public bool isAllKey = false;
    public bool isLevelComplete = false;

    void Awake()
    {
       
    }

	// Use this for initialization
	void Start ()
    {  
        if (GameObject.FindGameObjectWithTag("MainCamera"))
        {
            manager = Camera.main.GetComponent<GameManager1>();
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
        if(GameObject.FindGameObjectWithTag("PortalLocation"))
        {
            this.transform.position = GameObject.FindGameObjectWithTag("PortalLocation").transform.position;
        }

        if(keyCount >= 3)
        {
            isAllKey = true;
        }

        if(isAllKey)
        {
            GetComponent<ParticleSystem>().emissionRate = 12;
            //GetComponent<CircleCollider2D>().enabled = true;
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "PortalKey")
        {
            keyCount++;
        }

        if(other.gameObject.tag == "Player" && isAllKey)
        {
            print("Going to the next level");

            isLevelComplete = true;
            other.gameObject.SetActive(false);
            manager.CallNextLevel();

            
        }
    }
}
