﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateClickPoint : MonoBehaviour
{

    public GameObject player;

    public float speed = 1.0f; //how fast the object should rotate
    public float rotX;
    public float rotY;

    #region to rotate obj with mouse 
    //void RotOjAroundWithMouse()
    //{
    //    //rotX = Input.GetAxis("Mouse X") * speed * Mathf.Deg2Rad;
    //    rotY = Input.GetAxis("Mouse Y") * speed * Mathf.Deg2Rad;

    //    transform.Rotate(Vector3.up, rotY);
    //}
    #endregion



    void Update()
    {

        RotateFacingMouse();
        // transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed);


    }


    public void RotateFacingMouse()
    {

        #region Mouse Rotate Player
        //SMOOTH WAY of rotating
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, this.transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;

        RaycastHit hit = new RaycastHit();
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion m_targetRotation = Quaternion.LookRotation(targetPoint - this.transform.position);



            // Smoothly rotate towards the target point.
            //playerChild.transform.rotation = Quaternion.Slerp(playerChild.transform.rotation, m_targetRotation, rotSpeed * Time.fixedDeltaTime); // WITH SPEED
            transform.rotation = Quaternion.Slerp(new Quaternion(player.transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w), m_targetRotation, 1); // WITHOUT SPEED!!!

        }

        #endregion
    }

}
