﻿//using UnityEngine;
//using System.Collections;
//using UnityStandardAssets.CrossPlatformInput;
//
//
//public class PlayerTwoD : MonoBehaviour
//{
//    #region Delegates
//
//    public delegate IEnumerator DashCoroutine(float dura, float dirX, float dirY);
//
//    #endregion
//
//    #region Variables/Attributes ect...
//
//    #region State Enums
//
//    /// <summary>
//    /// This are the states the character can be in
//    /// </summary>
//    public enum MotorState
//    {
//        None,
//        OnGround,
//        Dashing,
//        InAir,
//        Jumping,
//        Falling,
//        WallSliding,
//        WallSticking,
//        Transformed,
//        Dead
//    }
//   
//
//    /// <summary>
//    /// This is used to get info on the motor state. When you call this you can know if your character is in the air or on the ground. 
//    /// You also use it to set up your animation easily
//    /// You should use to get info on state but not to set it. 
//    /// </summary>
//    [Header("States Enum")]
//    [Space(3)]
//    public MotorState motorState; //{ get; private set; }
//
//    #endregion
//
//    #region Bool States
//
//    /// <summary>
//    /// Public states 
//    /// </summary>
//    [Space(3)]
//    [Header("States Bool")]
//    public bool inAirFlag;
//    public bool isJumpingFlag;
//    public bool isFallingFlag;
//    public bool didDashFlag;
//    public bool canDashFlag;
//    public bool isDashingFlag;
//    public bool isWrapping;
//    
//    #region isDashCDFull?
//    //[HideInInspector]
//    //public bool isDashCDFullFlag;
//    #endregion
//    public bool isGroundedFlag;
//    public bool isGravityIgnoredFlag;
//    public bool isDashing;
//    public bool isDashingUp;
//    public bool isDashingDown;
//    public bool isDashResetFlag;
//
//    #region Basic Abilities Bool
//    [Space(3)]
//    [Header("Can You Dash/Jump/WallJump?")]
//    public bool isDashAllowed;
//    public bool isJumpAllowed;
//    public bool isWallJumpAllowed;
//    #endregion
//
//    //Private Bool Function For States
//    private bool IsGrounded()
//    {
//        return ( controller.collisionInfo.below );
//    }
//
//	/// <summary>
//	/// Determines whether this instance is facing right. For keyboard and for touch control
//	/// </summary>
//	/// <returns><c>true</c> if this instance is facing right; otherwise, <c>false</c>.</returns>
//    private bool IsFacingRight()
//    {
//		return ( input.x > 0 || inputTouch.x > 0);
//    }
//
//    private bool IsFacingLeft()
//    {
//		return (input.x < 0 || inputTouch.x < 0);
//    }
//
//    private bool IsOnWall()
//    {
//        return ((controller.collisionInfo.right || controller.collisionInfo.left) && !controller.collisionInfo.below);
//    }
//
//    private bool IsWallSliding()
//    {
//        return (IsOnWall() && velocity.y < 0);
//    }
//
//    private bool IsInAir()
//    {
//        return (!controller.collisionInfo.below );
//    }
//
//    #endregion
//
//    #region Move Speed/ Jump / Acceleration
//
//    [Space(5)]
//    [Header("Overall moveSpeed/Jump/Acceleration")]
//    [Range(1, 12)]
//    public float moveSpeed = 5;
//
//    #region Touch Variables
//    //Input for horizontal touch input
//    //private float hInput = 0;
//	private bool jInput = CrossPlatformInputManager.GetButtonDown("Jump");
//	private bool dInput = CrossPlatformInputManager.GetButtonDown("Dash");
//    #endregion
//
//    [Range(1, 10)]
//    public float maxJumpHeight = 5;
//    [Range(0, 10)]
//    public float minJumpHeight = 0.1f;
//
//    [Tooltip("How long we want the character to take to reach the highest point")]
//    [Range(0, 5)]
//    public float timeToJumpApex = .35f;
//
//    [Tooltip("How it will take to go from our current velocity to our targetVelocity when in the air")]
//    [Range(0, 5f)]
//    public float accelerationAirBoneTime = .2f;
//
//    [Tooltip("How it will take to go from our current velocity to our targetVelocity when on the ground")]
//    [Range(0, 5)]
//    public float accelerationGroundedTime = .1f;
//
//    #endregion
//
//    #region Wall Jumping
//
//    #region Wall Vectors Variables
//    [Space(3)]
//    [Header("Walls Vectors")]
//    public Vector2 wallJumpClimb;
//    public Vector2 wallJumpOff;
//    public Vector2 wallLeap;
//    #endregion
//
//    #region Wall Jumping Speed/Timer Variables
//    [Space(1)]
//    [Header("Walls Jumping Speed/Timer")]
//    [Range(0, 5f)]
//    public float wallSlideSpeedMax = 3;
//    [Range(0, 5f)]
//    public float wallStickTime = .25f;
//    float wallUnstickTime;
//    float timeIgnoreGravity;
//    #endregion
//
//    #endregion
//
//    #region "Ignore GravityCD/TransformationCD
//    //[Space(3)]
//    //[Header("Ignore GravityCD/TransformationCD")]
//    [HideInInspector]
//    [Range(0, 5f)]
//    public float ignoreGravityForXTime;
//    [HideInInspector]
//    [Range(0, 5f)]
//    public float maxTimeIgnoreGravity = 1.5f;
//    [HideInInspector]
//    [Range(0, 5f)]
//    public float intoPackmanCD;
//
//    float velocityXSmoothing;
//    float maxJumpVelocity;
//    float minJumpVelocity;
//    float gravity = -20;
//    #endregion
//
//    #region Dashing Variables
//
//    #region Dash Speeds
//    [Space(5)]
//    [Header("Dash Speeds")]
//    [Range(0, 100f)]
//    public float dashSpeedMaxClamp = 20f;
//    [Range(0, 100f)]
//    public float dashSpeedMinClamp = 20f;
//    [Range(0, 100f)]
//    public float dashSpeed;
//    [Range(0, 100f)]
//    public float dashStartSpeed;
//    [Range(0, 100f)]
//    public float dashEndSpeed;
//    #endregion
//
//    #region Dash CD/Duration
//    [Space(3)]
//    [Header("Dash CD/Duration")]
//    [Range(0, 5f)]
//    public float dashDuration = 0.2f;
//    [Range(0, 5f)]
//    public float dashDurationMax = 0.2f;
//    [HideInInspector]
//    [Range(0, 5f)]
//    public float dashIgnoreGravityXTime;
//    [HideInInspector]
//    [Range(0, 5f)]
//    public float dashCD = 0.7f;
//    [HideInInspector]
//    [Range(0, 5f)]
//    public float dashCDMax = 0.7f;
//    public int dashResetCount = 0;
//
//    #endregion
//    #endregion
//
//    #region Controller2D/Velocity (Main components/variables for moving)
//
//    private Vector3 velocity;
//    //private Vector3 velocityRight;
//    private Vector2 input;
//	private Vector2 inputTouch; //Input for touch controller
//    private Controller2D controller;
//    private int wallDirectionX;
//    private float targetVelocityX;
//
//    public EaseTypeAuto easeType;
//
//
//    public Vector3 GetVelocity
//    {
//        get { return velocity; }
//        set { velocity = value; }
//    }
//
//    public Vector2 GetInput
//    {
//        get { return input; }
//        set { input = value; }
//    }
//
//	public Vector2 GetInputTouch
//	{
//		get { return inputTouch; }
//		set { inputTouch = value; }
//	}
//
//    public Controller2D GetController
//    {
//        get { return controller; }
//        set { controller = value; }
//    }
//
//    public int GetWallDirectionX
//    {
//        get { return wallDirectionX; }
//    }
//
//    public float GetTargetVelocityX
//    {
//        get { return targetVelocityX; }
//        set { targetVelocityX = value; }
//    }
//
//	/// <summary>
//	///  If input.x is at 0.
//	/// </summary>
//	/// <returns><c>true</c>, if keyboard input was checked, <c>false</c> otherwise.</returns>
//	public bool CheckIfKeyboardInput()
//	{
//		return (input.x == 0);
//	}
//
//    #endregion
//
//    #region FX
//    [Space(3)]
//    [Header("FX/Animations")]
//    
//    [Space(2)]
//    [Header("RunFX")]
//    //RunFX
//    public ParticleSystem runFX;
//    [Range(0, 100)]
//    public float runFXEmissionRateMax;
//    [Range(0, 100)]
//    public float runFXEmissionRateMin;
//    
//    [Space(1)]
//    [Header("JumpFX")]
//    public GameObject jumpFX;
//    public GameObject jumpDistortFX;
//    public GameObject wallJumpFX;
//    public GameObject wallJumpDistortFX;
//
//    [Space(1)]
//    [Header("DashFX")]
//    public ParticleSystem dashFX;
//    [Range(0, 100)]
//    public float dashFXEmissionRateMax;
//    [Range(0, 100)]
//    public float dashFXEmissionRateMin;
//
//    [Space(1)]
//    [Header("DeathFX")]
//    public GameObject deathFX;
//
//    [Space(1)]
//    [Header("PositionFX")]
//    public Transform runFXPosition;
//   
//    public Transform jumpFXPosition;
//    public Transform jumpDistortionFXPosition;
//    
//    public Transform wallJumpFXPosition;
//    public Transform wallJumpDistortionFXPosition;
//    #endregion
//
//    #region Animation
//    [Space(2)]
//    [Header("Animation")]
//    public Animator anim;
//    #endregion
//
//    #region Double Taping
//    private Vector3 posYAfterJump;
//    private float numberOfTapTimer;
//    private float tapSpeed = 0.3f;
//    private bool doneTaping;
//    #endregion
//
//    #endregion
//
//    #region Class State
//
//    #region Jumping
//
//    /// <summary>
//    /// Jump State class. Used to check conditions and details on jumping. 
//    /// </summary>
//    private class JumpingState
//    {
//        PlayerTwoD player;
//
//        public JumpingState(PlayerTwoD player2D)
//        {
//           this.player = player2D;
//        }
//
//		/// <summary>
//		/// Determines whether this instance is pressed. Will check our 2 different keyboard button assign for jump but also touch control button
//		/// </summary>
//		/// <returns><c>true</c> if this instance is pressed; otherwise, <c>false</c>.</returns>
//        public bool IsPressed()
//        {
//			return ((Input.GetKeyDown(KeyCode.Z) || Input.GetButtonDown("Jump") || CrossPlatformInputManager.GetButtonDown("Jump")));
//        }
//
//        public bool IsReleased()
//        {
//			return ((Input.GetKeyUp(KeyCode.Z) || Input.GetButtonUp("Jump") || CrossPlatformInputManager.GetButtonUp("Jump")));
//        }
//
//
//        public bool canDoubleJump;
//
//        public bool held;
//
//        public int graceJumpCount = 0; 
//
//        public int numAirJumps;
//
//        public float height;
//
//        public bool ignoreGravity;
//
//        
//
//
//        public enum JumpType
//        {
//            None,
//            Normal,
//            Grace,
//            RightWall,
//            LeftWall,
//        }
//
//        public JumpType jumpType;
//    }
//
//    private JumpingState _Jump;
//
//
//    #endregion
//
//    #region Dashing
//
//    /// <summary>
//    /// Dash State class. Used to check conditions and details on dashing. 
//    /// </summary>
//    private class DashState
//    {
//        //Player instance will be used to do the inside light work
//        PlayerTwoD player;
//
//        //By Using a class constructor with PlayerTwoD outer class 
//        //We can initiliase the instance and use it to access variables & component of the outer class
//        public DashState(PlayerTwoD player2D)
//        {
//            this.player = player2D;
//        }
//
//        public bool IsDashingDiagonalRight()
//        {
//            return ((player.IsInAir()) && (player.GetInput.x > 0.25f && player.GetInput.y > 1.20f
//                || player.GetInput.x < 0.25f && player.GetInput.y > 1.20f) && (Input.GetKey(KeyCode.X)
//                || Input.GetButton("Fire3")) && !player.IsOnWall() && player.canDashFlag);
//        }
//
//        public bool IsDashingDiagonalLeft()
//        {
//            return ((player.IsInAir()) && (player.GetInput.x < 0.25f && player.GetInput.y > 1.20f 
//                || player.GetInput.x > 0.25f && player.GetInput.y > 1.20f) && (Input.GetKey(KeyCode.X) 
//                || Input.GetButton("Fire3")) && !player.IsOnWall() && player.canDashFlag);
//
//        }
//
//        public bool IsDashingRight()
//        {
//                #region old retun (right)
//                // return ((Input.GetKey(KeyCode.X) || Input.GetButton("Fire3")) && (player.GetInput.x > 0 || Input.GetKeyDown(KeyCode.RightArrow)));
//                #endregion
//                
//			return (player.IsInAir() && (Input.GetKey(KeyCode.X) || Input.GetButton("Fire3") || CrossPlatformInputManager.GetButton("Dash"))  && (player.GetInput.x > 0 ||   Input.GetKeyDown(KeyCode.RightArrow)) 
//                    && !player.IsOnWall() && player.canDashFlag && player.GetInput.y <= 0);
//        }
//
//        public bool IsDashingLeft()
//        {
//            #region old return (left)
//            //return ((Input.GetKey(KeyCode.X) || Input.GetButton("Fire3")) && (player.GetInput.x < 0 || Input.GetKeyDown(KeyCode.LeftArrow)));
//            #endregion
//            
//			return (player.IsInAir() && (Input.GetKey(KeyCode.X) || Input.GetButton("Fire3") || CrossPlatformInputManager.GetButton("Dash")) && (player.GetInput.x < 0 ||  Input.GetKeyDown(KeyCode.LeftArrow )) 
//                    && !player.IsOnWall() && player.canDashFlag && player.GetInput.y <= 0);
//        }
//
//        public bool IsDashingUp()
//        {
//            #region old return (up)
//            //return ((Input.GetKey(KeyCode.X) || Input.GetButton("Fire3")) && (player.GetInput.y > 0 || Input.GetKeyDown(KeyCode.UpArrow)));
//            #endregion
//            
//			return (player.IsInAir() && (Input.GetKey(KeyCode.X) || Input.GetButton("Fire3") /*|| CrossPlatformInputManager.GetButton("Dash")*/ )  && (player.GetInput.y > 0 || Input.GetKeyDown(KeyCode.UpArrow))
//                && !player.IsOnWall() && player.canDashFlag);
//        }
//
//        public bool IsDashingDown()
//        {
//            #region old return (down)
//            //return ((Input.GetKey(KeyCode.X) || Input.GetButton("Fire3")) && (player.GetInput.y < 0|| Input.GetKeyDown(KeyCode.DownArrow)));
//            #endregion
//
//            return (player.IsInAir() && (Input.GetKeyDown(KeyCode.X) || Input.GetButtonDown("Fire3")) && (player.GetInput.y < 0 || Input.GetKeyDown(KeyCode.DownArrow))
//                && !player.IsOnWall() && player.canDashFlag);
//        }
//
//        public enum DashType
//        {
//            None,
//            Up,
//            Right,
//            Left,
//            Down
//        }
//
//        public DashType dashType;
//    }
//
//    private DashState _Dashing;
//
//    #endregion
//
//   #endregion
//
//    #region Init
//
//    /// <summary>
//    /// This function is used to init our component ect.. when the game start
//    /// </summary>
//    void Init()
//    {
//        //Component/Class initialisation
//        _Jump = new JumpingState(this);
//        _Dashing = new DashState(this);
//        anim = GetComponent<Animator>();
//        controller = GetComponent<Controller2D>();
//
//        //Calculate gravity/min/maxJump
//        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
//        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
//        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
//        //print("Gravity:" + gravity + " Velocity:" + velocity);
//
//        //Initial max duration or cooldown
//        dashDuration = dashDurationMax;
//        ignoreGravityForXTime = maxTimeIgnoreGravity;
//
//        
//        
//    }
//
//    #endregion
//
//    #region Start
//
//    // Use this for initialization
//    void Start()
//    {
//        Init();
//    }
//
//    #endregion
//
//    #region Update States
//
//    /// <summary>
//    ///Take care of updating the states and adjust variables/components based on states.
//    /// </summary>
//    private void UpdateStates()
//    {
//        if(IsGrounded())
//        {
//            motorState = MotorState.OnGround;
//            
//            isGroundedFlag = true;
//            
//            isDashingFlag = false;
//            isDashingUp = false;
//            isDashingDown = false;
//            canDashFlag = false;
//            inAirFlag = false;
//            isFallingFlag = false;
//            isJumpingFlag = false;
//           // dInput = false;
//
//            _Jump.graceJumpCount = 0;
//
//           // StopCoroutine(Dashing(0, StopA));  
//        }
//        else if(IsInAir())
//        {
//            isGroundedFlag = false;
//        }
//
//        if(velocity.y < 0)
//        {
//            motorState = MotorState.Falling;
//
//            isFallingFlag = true;
//        }
//        else if(velocity.y > 0)
//        {
//            isFallingFlag = false;
//        }
//
//        //This is to make sure that we can't dash while on the ground or maintain dashing while on the ground
//        if ((Input.anyKey) && IsGrounded())
//        {
//
//            // print("Did occur!!!");
//            canDashFlag = false;
//            isDashingFlag = false;
//            isDashingDown = false;
//            isDashingUp = false;
//            isFallingFlag = false;
//            //dInput = false;
//            motorState = MotorState.OnGround;
//           // StopCoroutine(Dashing(0, 0, 0));
//
//        }
//    }
//
//    #endregion
//
//    #region Updating Movement Variables/Components Needed To Move
//
//    /// <summary>
//    /// Will take care of updating movement related variable that multiple functions needs
//    /// </summary>
//     void UpdateMovementVariables()
//    {
//        //We are using getAxixRaw because we are going to do the smoothing by ourself. 
//        input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
//		 inputTouch = new Vector2(CrossPlatformInputManager.GetAxisRaw("Horizontal"),CrossPlatformInputManager.GetAxisRaw("Vertical"));
//
//        //Just to check if we have wallCollision on the left or right
//        wallDirectionX = (controller.collisionInfo.left) ? -1 : 1;
//
//        //We are doing this to prevent accelerating by pressing forward button during our dash
//        if (!isDashingFlag)
//        {
//			///Check if we are using the keyboardInput or not. So that it will use the appropriate "input" Vector (keyboard or touch input)
//			targetVelocityX = CheckIfKeyboardInput() ?  inputTouch.x * moveSpeed : input.x * moveSpeed;
//        }
//
//        ////This part is to smooth out our movement.
//        //////Our last argument smoothTime is there to determine how long it will take to go from our current velocity to our targetVelocity
//        //////That is something that we want to change depending on the situation for example if we on the ground or in the air
//        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing,
//         (controller.collisionInfo.below) ? accelerationGroundedTime : accelerationAirBoneTime); //Are we on the ground then use the accelerationGroundedTime else we in the air so use the accelerationAirBoneTime
//    }
//    
//    #region TouchStarMoving Function 
//
////    public void TouchStartMoving(float horizontalInput)
////    {
////        hInput =  horizontalInput;
////    }
//
//    #endregion 
//
//    #endregion
//
//    #region Updating Animations
//
//    private void UpdateAnimations()
//    {
//        if (anim)
//        {
//            anim.SetBool("Ground", IsGrounded());
//            anim.SetBool("isDashing", isDashing);
//            anim.SetBool("isDashingUp", isDashingUp);
//            anim.SetBool("isDashingDown", isDashingDown);
//            anim.SetFloat("vSpeed", velocity.y);
//            anim.SetFloat("Speed", Mathf.Abs(velocity.x));  
//            anim.SetBool("isOnWall", IsOnWall());
//        }
//    }
//
//    #endregion
//
//    #region Update FX
//
//    private void UpdateFX()
//    {
//        RunningFX();
//        DashingFX();
//        JumpingFX();
//    }
//
//    #endregion
//
//    #region Timers Updating & Reseting
//
//    /// <summary>
//    /// Will activate a timer based on condition met
//    /// </summary>
//    private void UpdateTimers()
//    {
//        if (isDashingFlag)
//        {
//            dashDuration -= Time.deltaTime;
//            
//            if (ignoreGravityForXTime > 0)
//            {
//                _Jump.ignoreGravity = true;
//                isGravityIgnoredFlag = true;
//            }
//            else
//            {
//                _Jump.ignoreGravity = false;
//                isGravityIgnoredFlag = false;
//            }
//        }
//
//        else if (!isDashingFlag)
//        {
//            isDashingFlag = false;
//        }
//
//        ResetTimersIf();
//    }
//
//    /// <summary>
//    /// Will check if we meet conditions to reset a timer
//    /// </summary>
//    private void ResetTimersIf()
//    {
//        //If we on the ground or a wall we reset our dashDuration and ignoreGravity timers but we are not allowed to dash on the ground
//        if ((IsGrounded() || IsOnWall()))
//        {
//            dashDuration = dashDurationMax;
//            ignoreGravityForXTime = dashDurationMax; // maxTimeIgnoreGravity;
//            canDashFlag = false;
//            didDashFlag = false;
//            isDashResetFlag = false;
//            //dInput = false;
//            dashResetCount = 0;
//        }
//
//
//        //If we are in the air and we are not currently dashing then we can dash
//        if(IsInAir() && !isDashingFlag && dashDuration >= dashDurationMax )//|| didDashFlag && isDashResetFlag)
//        {
//            canDashFlag = true;
//        }
//
//        if (didDashFlag && isDashResetFlag && dashResetCount < 1 && (!IsGrounded() || !IsOnWall()))
//        {
//            dashDuration = dashDurationMax;
//            ignoreGravityForXTime = dashDurationMax; 
//            canDashFlag = true;
//        }
//
//       else if (didDashFlag && isDashResetFlag && dashResetCount >= 1 && dashDuration >= dashDurationMax)
//       {
//            didDashFlag = false;
//            isDashResetFlag = false;
//       }
//
//        if(dashDuration < 0)
//        {
//            dashDuration = 0;
//           
//            canDashFlag = false;
//           
//            didDashFlag = true;
//           
//        }
//    }
//
//    #endregion
//
//    #region Dat New New Update
//
//    /// <summary>
//    /// Update Everything needed for our character
//    /// </summary>
//    void Update()
//    {
//
//        UpdateFX();
//
//        UpdateStates();
//
////#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT || UNITY_EDITOR
//
//       
//
////#else
//
//        
//
////#endif
//
//        #region Testing touch movement
//
//
//        UpdateMovementVariables();
//        
//
//        #endregion
//
//        if (isWallJumpAllowed)
//        {
//            HandleWallSliding();
//
//            HandleWallJumpInteraction();
//        }
//
//        if (isJumpAllowed)
//        {
//            HandleJumping();
//        }
//
//        HandleGravity();
//
//        HandleMoving();
//
//        if (isDashAllowed)
//        {
//            HandleDashing();
//        }
//
//        //To avoid force accumulation
//        if (controller.collisionInfo.above || controller.collisionInfo.below)
//        {
//            velocity.y = 0;
//        }
//
//        UpdateAnimations();
//
//        UpdateTimers();
//    }
//
//    #endregion
//
//    #region Double Tap function
//    //TODO: Implement this function 
//    /// <summary>
//    /// Double tap function that would take dash coroutine as an argument but also keyCode, speedtap and numbTap
//    /// </summary>
//    /// <param name="keycode"></param>
//    /// <param name="speedTap"></param>
//    /// <param name="numbTap"></param>
//    /// <param name="coroutineDash"></param>
//    void DoubleTapDash(KeyCode keycode, float speedTap, float numbTap, DashCoroutine coroutineDash)
//    {
//
//    }
//    #endregion
//
//    #region Handle moving
//
//
//    /// <summary>
//    /// Handle moving
//    /// </summary>
//    public void HandleMoving()
//    {
//        controller.Move(velocity * Time.deltaTime, input);
//      
//        HandleFlipSprite();
//    }
//
//    #endregion
//
//    #region Handle jumping
//
//    /// <summary>
//    /// Take care of jumping.
//    /// Will check if we are on the ground and if we pressed the jump button
//    /// Also check if we release early to jump at minJumpVel
//    /// </summary>
//    public void HandleJumping()
//    {
//        //Saving grace jump (if we did not jump & we are falling we are given the chance to jump)
//        if (isFallingFlag && _Jump.graceJumpCount < 1)
//        {
//            if (_Jump.IsPressed())
//            {
//                //x6 because we are against gravity
//                velocity.y = maxJumpHeight * 6;
//
//                _Jump.graceJumpCount = 1;
//
//                _Jump.jumpType = JumpingState.JumpType.Grace;
//
//            }
//        }
//
//        //If we on the ground we are allowed to jump   
//        if (_Jump.IsPressed() )
//        {
//            if (IsGrounded())
//            {
//                //Jump
//                velocity.y = maxJumpVelocity;
//                _Jump.jumpType = JumpingState.JumpType.Normal;
//                _Jump.graceJumpCount = 1;
//            }
//
//            //States flags
//            isDashingUp = false;
//            isDashingDown = false;
//            isJumpingFlag = true;
//            inAirFlag = true;
//            isGroundedFlag = false;
//
//            //Indicate the current state
//            motorState = MotorState.Jumping;
//        }
//
//        //If we released the jump button we jump to minJumpVel
//		if (_Jump.IsReleased())
//        {
//            if (velocity.y > minJumpVelocity)
//            {
//                velocity.y = minJumpVelocity;
//            }
//        }
//
//        _Jump.jumpType = JumpingState.JumpType.None;
//
//        EndJump();
//    }
//
//    #region TouchStarMoving Function 
//
////    public void TouchJump(bool jumpingInput)
////    {
////        jInput = jumpingInput;
////    }
//
//    #endregion
//
//    #region End Of Some States
//
//    #region End Of Jumping
//
//    /// <summary>
//    /// Gets called right after jumping 
//    /// </summary>
//    void EndJump()
//    {
//        if (motorState == MotorState.Jumping && velocity.y < 0)
//        {
//            motorState = MotorState.Falling;
//            isFallingFlag = true;
//            isJumpingFlag = false;
//        }
//    }
//
//    #endregion
//
//    #region End Of Dashing
//
//    /// <summary>
//    /// Gets called right after dashing 
//    /// </summary>
//    void EndDash()
//    {
//        if (motorState == MotorState.Dashing && velocity.y < 0)
//        {
//            motorState = MotorState.Falling;
//            isFallingFlag = true;
//        }
//    }
//
//    #endregion
//
//    #endregion
//
//    #region Wall Sliding
//
//    /// <summary>
//    /// Take care of wall sliding
//    /// </summary>
//    void HandleWallSliding()
//    {
//        if (IsWallSliding())
//        {
//            motorState = MotorState.WallSliding;
//
//            if (velocity.y < -wallSlideSpeedMax)
//            {
//                velocity.y = -wallSlideSpeedMax;
//            }
//
//            //Counting down the time we can stick to a wall
//            if (wallUnstickTime > 0)
//            {
//                //reset our velocity & velocity smoothing else it gets funky
//                velocityXSmoothing = 0;
//                velocity.x = 0;
//
//                motorState = MotorState.WallSticking;
//
//                //If we are not pressing in the direction of the wall & we are pressing something that is away from our current wall
//				if (input.x != wallDirectionX && input.x != 0 || inputTouch.x != wallDirectionX && inputTouch.x != 0)
//                {
//                    wallUnstickTime -= Time.deltaTime;   
//                }
//
//                //Reset our time to stick
//                else
//                {
//                    wallUnstickTime = wallStickTime;
//                }
//
//            }
//            else
//            {
//                wallUnstickTime = wallStickTime;
//            }
//        }
//    }
//
//    #endregion
//
//    #region WallJumpingInteraction
//
//    /// <summary>
//    /// Handle wall jumping interactions.
//    /// </summary>
//    void HandleWallJumpInteraction()
//    {
//        if (_Jump.IsPressed())
//        {
//            if (IsWallSliding())
//            {
//                //This part could go into IsWallSliding()
//                //If we are trying to move to the direction we are facing
//				if (wallDirectionX == input.x || input.y > 0 || wallDirectionX == inputTouch.x)
//                {
//                    
//                    //Push back on the wall direction
//                    velocity.x = -wallDirectionX * wallJumpClimb.x;
//                    
//                    //Go Up
//                    velocity.y = wallJumpClimb.y;
//
//                    if (wallJumpFX && wallJumpDistortFX)
//                    {
//                        Instantiate(wallJumpFX, wallJumpFXPosition.position, Quaternion.identity);
//
//                        Instantiate(wallJumpDistortFX, wallJumpDistortionFXPosition.position, Quaternion.identity);
//                    }
//
//                 
//                }
//
//                //If we are not trying to move on the wall, we are sliding off 
//				else if (input.x == 0 || inputTouch.x == 0)
//                {
//                    velocity.x = -wallDirectionX * wallJumpOff.x;
//                    velocity.y = wallJumpClimb.y;
//
//                    if (wallJumpFX && wallJumpDistortFX)
//                    {
//                        Instantiate(wallJumpFX, wallJumpFXPosition.position, Quaternion.identity);
//
//                        Instantiate(wallJumpDistortFX, wallJumpDistortionFXPosition.position, Quaternion.identity);
//                    }
//                }
//
//                //If we are trying to jump to the other side
//                else
//                {
//                 
//                    velocity.x = -wallDirectionX * wallLeap.x;
//                    velocity.y = wallLeap.y;
//                    //print("jump to the other side");
//
//                    if (wallJumpFX && wallJumpDistortFX)
//                    {
//                        Instantiate(wallJumpFX, wallJumpFXPosition.position, Quaternion.identity);
//
//                        Instantiate(wallJumpDistortFX, wallJumpDistortionFXPosition.position, Quaternion.identity);
//
//                        //print("wallJumpFX & wallJumpDistortFX for jump to the other side");
//                    }
//                }
//            }
//        }
//    }
//
//    #endregion
//
//    #region Handle Dashing
//
//    /// <summary>
//    /// Take care of turning on/off Pre dash related flags
//    /// </summary>
//    public void PreDashRelatedFlag()
//    {
//        canDashFlag = false;
//        didDashFlag = false;
//    }
//    /// <summary>
//    /// Take care of turning on dash related flags
//    /// </summary>
//    public void TurnOnDashRelatedFlag()
//    {
//        
//        isDashingFlag = true;
//        inAirFlag = true;
//        isJumpingFlag = false;
//        isDashing = true;
//
//        #region touch controls???
//        //touch controls
//        //jInput = false;
//        //dInput = true;
//        #endregion
//
//        _Jump.ignoreGravity = true;
//        isGravityIgnoredFlag = true;
//    }
//
//    /// <summary>
//    /// Take care of turning off dash related flags
//    /// </summary>
//    public void TurnOffDashRelatedFlag()
//    {
//        isDashingFlag = false;
//        _Jump.ignoreGravity = false;
//        isGravityIgnoredFlag = false;
//        isDashing = false;
//        isDashingUp = false;
//        //dInput = false;
//
//        EndDash();
//    }
//
//    
//    /// <summary>
//    /// Take care of the dashing state transition & maintaing x/y constant during the dash. We are not affected by gravity while dashing
//    /// </summary>
//    /// <param name="dura"></param>
//    /// <param name="dirX"></param>
//    /// <param name="dirY"></param>
//    /// <returns>yield return new WaitForSeconds(dashCD);</returns>
//    public IEnumerator DashingStateTimer(float dura, float y, float x, Coroutine flex)
//    {
//        #region Dash State Duration
//       
//        float time = 0;
//
//        while (dura > time)
//        {
//            motorState = MotorState.Dashing;
//
//            time += Time.deltaTime;
//
//            TurnOnDashRelatedFlag();
//
//            yield return null;
//        }
//
//        #endregion
//        
//        TurnOffDashRelatedFlag();
//
//        yield return new WaitForSeconds(dashCD);
//    }
//
//    /// <summary>
//    /// (Original... won't take coroutine as argument)
//    /// Take care of the dashing state transition & maintaing x/y constant during the dash. We are not affected by gravity while dashing
//    /// </summary>
//    /// <param name="dura"></param>
//    /// <param name="dirX"></param>
//    /// <param name="dirY"></param>
//    /// <returns>yield return new WaitForSeconds(dashCD);</returns>
//    public IEnumerator DashingStateTimer(float dura, float y, float x)
//    {
//        #region Dash State Duration
//
//        float time = 0;
//
//        while (dura > time)
//        {
//            motorState = MotorState.Dashing;
//
//            time += Time.deltaTime;
//
//            TurnOnDashRelatedFlag();
//
//            yield return null;
//        }
//
//        #endregion
//
//        TurnOffDashRelatedFlag();
//
//        yield return new WaitForSeconds(dashCD);
//    }
//
//    /// <summary>
//    /// Set a velocity vector and return it
//    /// </summary>
//    /// <param name="vel"></param>
//    /// <param name="param"></param>
//
//    /// <returns></returns>
//    public Vector3 ModifyVelocity(Vector3 vel, float x)
//    {
//        //vel.x = x;
//        vel = new Vector3(transform.position.x + x, transform.position.y, transform.position.z);
//
//        return vel;
//    }
//    /// <summary>
//    /// Check the condition for dashing and adjust the dash accordingly 
//    /// </summary>
//    public void HandleDashing()
//    {
//
//            #region old condition (diagonal right)
//            // if ((IsInAir()) && (input.x > 0.25f && input.y > 1.20f || input.x < 0.25f && input.y > 1.20f) && (Input.GetKey(KeyCode.X) || Input.GetButton("Fire3")) && !IsOnWall() && canDashFlag)
//            #endregion
//
//            //Diagonal right
//            if (_Dashing.IsDashingDiagonalRight())
//            {
//                isDashingUp = false;
//                isDashingDown = false;
//
//                //print("Doing weird diagonal dash to the right");
//                float dashSpeedFunct = Mathf.Clamp((EaseOutQuintD(dashStartSpeed, dashEndSpeed, dashSpeed)), dashSpeedMinClamp, dashSpeedMaxClamp);
//
//                _Dashing.dashType = DashState.DashType.Up;
//
//                //Vector3 dashRVec = new Vector3(transform.position.x * dashSpeed, transform.position.y, transform.position.z);
//                //  StartCoroutine(Dashing(dashDuration, 0, velocity.y = dashSpeedFunct - 6f));
//                //StartCoroutine(transform.MoveFrom(dashRVec, dashDuration, easeType));
//
//                if (isDashResetFlag)
//                {
//                    dashResetCount++;
//                }
//
//                return;
//            }
//
//            #region old condition (diagonal left)
//            //if ((IsInAir()) && (input.x < 0.25f && input.y > 1.20f || input.x > 0.25f && input.y > 1.20f) && (Input.GetKey(KeyCode.X) || Input.GetButton("Fire3")) && !IsOnWall() && canDashFlag)
//            #endregion
//
//            //Diagonal left
//            if (_Dashing.IsDashingDiagonalLeft())
//            {
//                isDashingUp = false;
//                isDashingDown = false;
//
//                //print("Doing weird diagonal dash to the left");
//                float dashSpeedFunct = Mathf.Clamp((EaseOutQuintD(dashStartSpeed, dashEndSpeed, dashSpeed)), dashSpeedMinClamp, dashSpeedMaxClamp);
//
//                _Dashing.dashType = DashState.DashType.Up;
//
//                // StartCoroutine(Dashing(dashDuration, 0, velocity.y = dashSpeedFunct - 6f));
//
//
//                if (isDashResetFlag)
//                {
//                    dashResetCount++;
//                }
//                return;
//            }
//
//            #region old condition (right)
//            //if ((IsInAir()) && _Dashing.IsDashingRight() && !IsOnWall() && canDashFlag && input.y <= 0) 
//            #endregion
//
//            //Going right
//            if (_Dashing.IsDashingRight())
//            {
//                isDashingUp = false;
//                isDashingDown = false;
//
//                //Clamping our max speed/distance we can reach. Else the dashing is more hazardous
//                float dashXLock = Mathf.Clamp(dashStartSpeed, dashSpeedMinClamp, dashSpeedMaxClamp);
//                float dashYLock = Mathf.Clamp(velocity.y, Mathf.Abs(minJumpHeight), Mathf.Abs(maxJumpHeight));
//                _Dashing.dashType = DashState.DashType.Right;
//
//
//                StartCoroutine(DashingStateTimer(dashDuration, velocity.y = dashYLock, velocity.x = dashXLock,
//                StartCoroutine(transform.MoveTo(ModifyVelocity(velocity, dashXLock), dashDuration, easeType))));
//
//
//                //StartCoroutine(transform.MoveTo(ModifyVelocity(velocity, dashXLock), dashDuration, easeType));
//
//                //print("Velocity: " + velocity);
//
//                //print("Dash right");
//
//
//                if (isDashResetFlag)
//                {
//                    dashResetCount++;
//                }
//                return;
//            }
//
//            #region old condition (left)
//            //else if ((IsInAir()) && _Dashing.IsDashingLeft() && !IsOnWall() && canDashFlag && input.y <= 0)
//            #endregion
//
//            else if (_Dashing.IsDashingLeft())
//            //Going left
//            {
//                isDashingUp = false;
//                isDashingDown = false;
//
//                float dashSpeedFunct = Mathf.Clamp((EaseOutQuintD(dashStartSpeed, dashEndSpeed, dashSpeed)), dashSpeedMinClamp, dashSpeedMaxClamp);
//
//                _Dashing.dashType = DashState.DashType.Left;
//
//                // StartCoroutine(Dashing(dashDuration, velocity.x = -dashSpeedFunct,
//                // velocity.y = Mathf.Clamp(velocity.y, Mathf.Abs(minJumpHeight), Mathf.Abs(maxJumpHeight))));
//                //print("Dash left");
//
//
//                if (isDashResetFlag)
//                {
//                    dashResetCount++;
//                }
//                return;
//            }
//
//            #region old condition (up)
//            // else if ((IsInAir()) && _Dashing.IsDashingUp() && !IsOnWall() && canDashFlag)
//            #endregion
//
//            //Going up
//            else if (_Dashing.IsDashingUp())
//            {
//                isDashingUp = true;
//                isDashingDown = false;
//
//
//                float dashSpeedFunct = Mathf.Clamp((EaseOutQuintD(dashStartSpeed, dashEndSpeed, dashSpeed)), dashSpeedMinClamp, dashSpeedMaxClamp);
//
//                _Dashing.dashType = DashState.DashType.Up;
//
//                //StartCoroutine(Dashing(dashDuration, 0, velocity.y = dashSpeedFunct - 6f));
//                //print("Dash up");
//
//
//                if (isDashResetFlag)
//                {
//                    dashResetCount++;
//                }
//                return;
//            }
//
//            #region old condition (down)
//
//            //else if ((IsInAir())  && _Dashing.IsDashingDown() && !IsOnWall() && canDashFlag)
//
//            #endregion
//
//            //Going down
//            else if (_Dashing.IsDashingDown())
//            {
//                isDashingUp = false;
//                isDashingDown = true;
//
//                float dashSpeedFunct = Mathf.Clamp((EaseOutQuintD(dashStartSpeed, dashEndSpeed, dashSpeed)), dashSpeedMinClamp, dashSpeedMaxClamp);
//
//                _Dashing.dashType = DashState.DashType.Down;
//
//                // StartCoroutine(Dashing(0.1f, 0, velocity.y = -dashSpeedFunct * 1.5f));
//                //print("Dash down");
//
//                if (isDashResetFlag)
//                {
//                    dashResetCount++;
//                }
//                return;
//            }
//        }
//    
//
//    #region Touch Dash
//
////    public void TouchDash(bool dashingInput)
////    {
////		dInput = (dashingInput && (hInput >= 1 || hInput <= -1) && (!IsGrounded()))? true : false;
////    }
//
//	#endregion
//
//    #endregion
//
//    #region Handle Gravity
//    /// <summary>
//    /// Handle Gravity/Falling
//    /// </summary>
//    void HandleGravity()
//    {
//        if (!_Jump.ignoreGravity)
//        {
//            velocity.y += gravity * Time.deltaTime;
//        }
//    }
//    #endregion
//
//    #region Flip Sprite Based On Facing Direction
//
//    /// <summary>
//    /// Flip the sprite based on the local scale.
//    /// When facing right local scale x = 1
//    /// When facing left local scale x = -1
//    /// </summary>
//    void HandleFlipSprite()
//    {
//		if (IsFacingRight())
//        {
//            Vector3 newScale = transform.localScale;
//            newScale.x = 1.0f;
//            transform.localScale = newScale;
//            input.x = 1.0f;
//			inputTouch.x = 1.0f;
//        }
//
//		else if (IsFacingLeft())
//        {
//            Vector3 newScale = transform.localScale;
//            newScale.x = -1.0f;
//            transform.localScale = newScale;
//            input.x = -1.0f;
//			inputTouch.x = -1.0f;
//        }
//    }
//
//  
//
//    #endregion
//
//    #region Player Death
//    /// <summary>
//    /// Handle Player Death
//    /// </summary>
//    public void PlayerDeath()
//    {
//        motorState = MotorState.Dead;
//        CameraShakeManager.Instance.BigCameraShake();
//        Instantiate(deathFX, transform.position, Quaternion.identity);
//        Destroy(this.gameObject);
//        print("Player died");
//    }
//    #endregion
//
//    #region Handle FX
//
//    #region RunningFX Function
//
//    /// <summary>
//    /// Take care of how the running fx looks, conditions to appear and emissionRate
//    /// </summary>
//    void RunningFX()
//    {
//        if (runFX)
//        {
//            ///Take care of the run particles emssion when on the ground
//
//            if (IsGrounded() && !isWrapping && (velocity.x > 1 || velocity.x <= -1))
//            {
//                //Instantiate(runFXObj, runFXPosition.position, Quaternion.identity);
//                runFX.emissionRate = runFXEmissionRateMax;
//                
//            }
//            else
//            {
//                runFX.emissionRate = runFXEmissionRateMin;
//            }
//        }
//    }
//
//
//    #endregion
//
//    #region DashingFX Function
//
//    /// <summary>
//    /// Take care of adjusting the dashfx emission based on the state
//    /// </summary>
//    void DashingFX()
//    {
//        if (dashFX)
//        {
//            if (isDashingFlag)
//            {
//                dashFX.emissionRate = dashFXEmissionRateMax;
//            }
//            else
//            {
//                dashFX.emissionRate = dashFXEmissionRateMin;
//            }
//        }
//    }
//
//    #endregion
//
//    #region JumpingFX Function
//
//    /// <summary>
//    /// Take care of instanciate jumpfx when we are jumping
//    /// </summary>
//    void JumpingFX()
//    {
//        if (jumpFX)
//        {
//            if (IsGrounded())
//            {
//                if (_Jump.IsPressed())
//                {
//
//                    Instantiate(jumpFX, jumpFXPosition.position, Quaternion.identity);
//
//                    if (jumpDistortFX)
//                    {
//                          Instantiate(jumpDistortFX, jumpDistortionFXPosition.position, Quaternion.identity);
//                    }
//
//                }
//            }
//        }
//        
//    }
//
//    #endregion
//
//
//    #endregion
//
//    #region Adjust Rotation
//
//    /// <summary>
//    /// This function will adjust the rotation.
//    /// </summary>
//    /// <param name="x"></param>
//    /// <param name="y"></param>
//    void AdjustRotation(float x, float y, float z, float w)
//    {
//        Quaternion r = transform.rotation;
//
//        r = new Quaternion(x, y, z, w);
//
//        transform.rotation = new Quaternion(x, y, z, w);
//    }
//    
//    #endregion
//
//    #region Adjust collider
//
//    /// <summary>
//    /// This function will adjust the collider. Based on our need we can call it.
//    /// </summary>
//    /// <param name="x"></param>
//    /// <param name="y"></param>
//    void AdjustCollider(float x, float y, Bounds bound)
//    {
//        BoxCollider2D box2D = new BoxCollider2D();//gameObject.GetComponent<BoxCollider2D>();
//
//        box2D.size = new Vector2(x, y);
//
//        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(box2D.size.x, box2D.size.y);
//    }
//    
//    #endregion
//
//    #region Check if our cooldown is full
//    /// <summary>
//    /// Use this function to check if a specific cooldown is charged to the max (isFull)
//    /// You can also use this to check if it's not full
//    /// </summary>
//    /// <param name="cd"></param>
//    /// <param name="cdMax"></param>
//    /// <returns></returns>
//    bool CheckIfCDIsFull(float cd, float cdMax)
//    {
//        return (cd >= cdMax);
//    }
//    #endregion
//
//    #region EaseOutQuintD Formula Function
//
//    /// <summary>
//    /// This is just a EaseOut Quint function used for dashing
//    /// It can also be used for others action that involves speed/moving/animation
//    /// </summary>
//    /// <param name="start"></param>
//    /// <param name="end"></param>
//    /// <param name="value"></param>
//    /// <returns></returns>
//    public float EaseOutQuintD(float start, float end, float value)
//    {
//        value--;
//        end -= start;
//        return 5f * end * value * value * value * value;
//    }
//
//    #endregion
//
//    #region OnTriggerEnter2D
//    /// <summary>
//    /// Built-In OnTrigger Enter Function
//    /// </summary>
//    /// <param name="other"></param>
//    void OnCollisionEnter2D(Collision2D other)
//    {
//        //if (other.tag == "Enemy"  )
//        //{
//   
//        //    print("Touched Enemy");
//        //    PlayerDeath();
//        //    GameManager1.instance.CallGameOver();
//            
//        //}
//
//        if (other.gameObject.tag == "DroppedHazard" || other.gameObject.tag == "BouncingHazard")
//        {
//            PlayerDeath();
//        }
//    }
//    #endregion
//
//
//}
//
//
//#endregion