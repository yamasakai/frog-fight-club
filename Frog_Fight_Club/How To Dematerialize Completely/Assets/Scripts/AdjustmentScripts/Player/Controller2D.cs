﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoxCollider2D))]
public class Controller2D : RaycastController 
{
    //public LayerMask collisionMask;
    [HideInInspector]
    public Vector2 playerInput;

    public CollisionInfo collisionInfo;

    
      public override void Start()
      {
        //This will use the instructions from start() raycastcontroller and allow us to keep going we our own code
        base.Start();
        collisionInfo.faceDirection = 1;
      }

    void Update()
    {
     
        
       
    }

    /// <summary>
    /// This will be used to store position of our raycast origin.
    ///  Struct types in mono are allocated on the stack, 
    ///  so if you have a utility class that won’t leave scope, make it a struct. 
    ///  Remember structs are passed by value, so you will have to prefix a parameter with ref to avoid any copy costs.
    /// </summary>
    struct RaycastOrigin
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    };

    public struct CollisionInfo
    {
        public bool above, below, left, right;
       
        public bool climbingSlope;
        public bool descendingSlope;
        public float slopeAngle, slopeAngleOld;
        public Vector3 velocityOld;
        public int faceDirection;
        public bool fallingThroughPlatform;

        public void Reset()
        {
            above = below = false;
            left = right = false;
            climbingSlope = false;
            descendingSlope = false;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
      
    }

    #region UpdateRaycastOrigins
    ///// <summary>
    ///// Assign the raycast position from our struct based on the bound of the collider
    ///// </summary>
    //void UpdateRaycastOrigins()
    //{
    //    Bounds bounds = collider.bounds;
    //    bounds.Expand(skinWidth * -2); //-2 so that it can reach both side of collider

    //    raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
    //    raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
    //    raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
    //    raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
    //}
    #endregion

    #region CalculateRaySpacing
    /// <summary>
	/// Calculate the ray spacing between them (horizontal & vertical ray for our collider)
	/// </summary>
    //void CalculateRaySpacing () 
    //{   
    //    Bounds bounds = collider.bounds;
    //    bounds.Expand(skinWidth * -2);
	
    //    horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
    //    verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);
        
    //    //The spacing is equal to the length of the y/x bound divide by hor/ver -1 so that it is proportionaly spaced
    //    horizontalRaySpacing = bounds.size.x / (horizontalRayCount -1);
    //    verticalRaySpacing = bounds.size.y / (verticalRayCount -1);
    //}
    #endregion

    public void Move(Vector3 velocity, bool standingOnPlatform)
    {
        Move(velocity, Vector2.zero, standingOnPlatform);
    }

    /// <summary>
    /// Can pass whatever velocity that we want to move our character
    /// </summary>
    /// <param name="velocity"></param>
    public void Move(Vector3 velocity , Vector2 input, bool standingOnPlatform = false)
    {
        UpdateRaycastOrigins();
        collisionInfo.Reset();
        collisionInfo.velocityOld = velocity;
        playerInput = input;
        
        if (velocity.x != 0)
        {
            collisionInfo.faceDirection = (int)Mathf.Sign(velocity.x);
       
        }
      
        HorizontalCollision(ref velocity);

        //if our velocity.y is not equal to 0 then use verticalCollision
        //
       // if (velocity.y != 0)
        //{
            
        VerticalCollision(ref velocity);
        //}
         
        transform.Translate(velocity);

        if(standingOnPlatform == true)
        {
            collisionInfo.below = true;
        }
       
    }
   

    /// <summary>
    /// When u pass without the keyword ref, you create a copy of the variable. 
    /// But this time is a ref so if we modify the variable it will change the variable being used as an argument.
    /// </summary>
    /// <param name="velocity"></param>
    void HorizontalCollision(ref Vector3 velocity)
    {
        //Getting the direction of our x/y velocity. If we are going down it become negative if we are going up positive.
        float directionX = collisionInfo.faceDirection; //Mathf.Sign(velocity.x);
        //Forcing our rayLength to be positive by using .Abs
        float rayLength = Mathf.Abs(velocity.x) + skinWidth;

        //If velocity.x is less than skinWidth
        //1SkinWidth to move the ray to the edge of the collider
        //2SkinWidth is to have some distance to detect the walls
        if(Mathf.Abs(velocity.x) < skinWidth)
        {
            rayLength = 2 * skinWidth;
        }
        for (int i = 0; i < horizontalRayCount; i++)
        {
            //We want to see in which direction we are moving. If we are going down we want our ray to start from the bottom left corner.
            //If we are going up from the top left corner.
            //Is the player directionX going down? if yes rayOrigin is equal to bottomLeft else 
            //directionX is going up and rayOrigin is equal to topLeft
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            //This will also cast the ray to the point where we will be on the x axis
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

            //Set our x velocity to amount we have to move from our current position the point where the ray intersect with a obstacle (the ray distance)
            if (hit)
            {
                if(hit.distance == 0)
                {
                    continue; //to skip ahead to the next raycast
                }

                velocity.x = (hit.distance - skinWidth) * directionX;
                rayLength = hit.distance;

                //If we going to the left/right and we hit something then collisionInfo.left/right will be equal to true
                collisionInfo.left = directionX == -1;
                collisionInfo.right = directionX == 1;
            }


        }
    }

    /// <summary>
    /// When u pass without the keyword ref, you create a copy of the variable. 
    /// But this time is a ref so if we modify the variable it will change the variable being used as an argument.
    /// </summary>
    /// <param name="velocity"></param>
    void VerticalCollision(ref Vector3 velocity)
    {
        //Getting the direction of our y velocity. If we are going down it become negative if we are going up positive.
        float directionY = Mathf.Sign(velocity.y);
        //Forcing our rayLength to be positive by using .Abs
        float rayLength = Mathf.Abs(velocity.y) + skinWidth;

       
            for (int i = 0; i < verticalRayCount; i++)
            {
                //We want to see in which direction we are moving. If we are going down we want our ray to start from the bottom left corner.
                //If we are going up from the top left corner.
                //Is the player directionY going down? if yes rayOrigin is equal to bottomLeft else 
                //directionY is going up and rayOrigin is equal to topLeft
                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                
                //This will push the rays to the right to expand based on our spacing and number of rays
                rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);
               

                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);
               

                //Set our y velocity to amount we have to move from our current position the point where the ray intersect with a obstacle (the ray distance)
                if (hit)
                {
                    velocity.y = (hit.distance - skinWidth) * directionY;
                    rayLength = hit.distance;

                    // velocity.y = (hitHelp.distance - skinWidth) * directionY;
                    //rayLength = hitHelp.distance; 

                    collisionInfo.below = directionY == -1;
                    collisionInfo.above = directionY == 1;
                }
            }
    }

        
   
}

