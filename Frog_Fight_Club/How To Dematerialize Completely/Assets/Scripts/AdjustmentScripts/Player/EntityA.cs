﻿using UnityEngine;
using System.Collections;

public class EntityA : MonoBehaviour {
	
	public float health;
	
	public GameObject ragdoll;

	private Vector3 healthBarScale;

	private SpriteRenderer healthBar;

    [HideInInspector]
    //public int enemyKilled = 0;




    //Wao it does not work if I do it in start.
    void Awake()
    {
        if (healthBar)
        {
            //We need to access the sprite renderer properties to know his dimension so we can modify it.
            healthBar = GameObject.Find("Health").GetComponent<SpriteRenderer>();

            //We are storing the initial scale of the health bar.(When the player is full health).
            healthBarScale = healthBar.transform.localScale;
        }
    }

	void Start()
	{

	}

	//For enemies
	public void TakeDamage(float dmg) {

		health -= dmg;
	
		if (health <= 0) {
			enemyDie();	

		}

	}

	//For player when using the health bar
	public void PlayerGetDamaged(float dmg)
	{
		health -= dmg;
        if (healthBar)
        {
            updateHealthBar();
        }
		
		if (health <= 0) {
			Die();
			//Might put a pop out menu here. Because the game over screen shows up a bit too suddently.
			Application.LoadLevel("GameOverScene");
		}


	}

	public void updateHealthBar()
	{
		// Set the health bar's colour to proportion of the way between green and red based on the player's health.
		healthBar.material.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.01f);
		
		// Set the scale of the health bar to be proportional to the player's health.
		healthBar.transform.localScale = new Vector3(healthBarScale.x * health * 0.01f, 1, 1);

	}

	//Player Die function
	public void Die() {
//		Ragdoll r = (Instantiate(ragdoll,transform.position,transform.rotation) as GameObject).GetComponent<Ragdoll>();
//		r.CopyPose(transform);
		//this.gameObject.SetActive(false);
		//Camera.main.GetComponent<CameraShake>().Shaker();
		Destroy(this.gameObject);
	   //	AutoFade.Death(3, 1, Color.black);
//		Destroy (r.gameObject, 1.0f);

	}

	//Enemy Die function
	public void enemyDie()
	{
		//Camera.main.GetComponent<CameraShake>().Shaker();
		Destroy(this.gameObject);
		//enemyKilled++;

	}

	//To destroy clone entity object from class that instantiate it 
	public void destroy(GameObject obj)
	{

		Destroy(obj);

	}
	 
}
