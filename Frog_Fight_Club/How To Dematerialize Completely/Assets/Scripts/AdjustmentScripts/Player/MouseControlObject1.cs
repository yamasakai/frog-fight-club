﻿using UnityEngine;
using System.Collections;

public class MouseControlObject : MonoBehaviour 
{
   
    public bool isCursorVisible = false;
 
    public float depth = 10.00f;

    private Vector3 mousePos;

    public GameObject hand;
    

    void Start()
    {
        Cursor.visible = isCursorVisible;
    }

    void Update()
    {
        if (hand != null)
        {
            mousePos = Input.mousePosition;

            Vector3 wantedPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, depth));

            hand.transform.position = wantedPos;
        }

        if (Input.GetMouseButtonDown(0))
        {
            ClickSelect();

        }

    }

    //This method returns the game object that was clicked using Raycast 2D
    GameObject ClickSelect()
    {
        //Converting Mouse Pos to 2D (vector2) World Pos/ Use ScreenToWorldPoint for 2D and use ScreenPointToRay for 3D
        Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

        RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);

        if (hit)
        {
            //Debug.Log(hit.transform.name);
            return hit.transform.gameObject;
        }
        else return null;
    }

}

