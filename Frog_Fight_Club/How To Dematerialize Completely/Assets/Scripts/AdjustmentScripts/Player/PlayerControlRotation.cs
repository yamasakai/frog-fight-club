﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlRotation : MonoBehaviour
{
    public float rotationX;
    public bool isCopyRot;
    public bool isRotX;
    public float speed = 1.0f; //how fast the object should rotate
    public float rotX;
    public float rotY;

    public int rotationDirection = -1; // -1 for clockwise
                                       //  1 for anti-clockwise

    public int rotationStep = 10;    // should be less than 90



    public Vector3 currentRotation, targetRotation;

    public float rotationDegreesPerSecond = 45f;
    public float rotationDegreesAmount = 90f;
    public float totalRotation = 0;



    public Vector3 mousePos;
   
    public bool isRegularRotUsed;

    public Transform player;
    public Transform froggy;
    public Transform clickPoint;

    public ControlRenderBallisticPath controlRenderBallisticPath;

    #region old variables
    //public GameObject playerChild;
    //public Vector3 amount;
    //public JuiceOnInteraction juicy;

    ////public Targetting targetCall;

    //public Vector3 lookTargetPos;

    //public Quaternion rotateToTarget;
    //public Vector3 newTargetRotationDir;

    //private RaycastHit hit;
    //public float RayCastGrdDist = 0.85f;
    //private Vector3 dir;
    //public KeyCode keyCode;
    //public bool canJump = false;
    //public bool isJumpForward,
    //    isJumpDownward,
    //    isJumpRight,
    //    isJumpLeft;
    //public bool JumpCoolDownActivated = false;

    //public int maxIFrames = 3; //if dodge makes you immune to damage, this is how you would set it
    //public float distanceMoved;
    // Use this for initialization
    #endregion

    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (controlRenderBallisticPath != null)
        //{
        //    if (controlRenderBallisticPath.isCurrentlyPathRender)
        //    {
        //        isRegularRotUsed = false;
        //    }
        //    else
        //    {
        //        isRegularRotUsed = true;
        //    }

        //}

        //RotationControl();

        //if we haven't reached the desired rotation, swing

        if (controlRenderBallisticPath.isCurrentlyPathRender)
        {
            if (isCopyRot)
            {
                
              
                RotateBasedOnLineR();

                CopyRot();
            }
            else
            {
                if (isRotX)
                {
                    RotOjAroundWithMouseX();

                }
                else
                {
                    RotOjAroundWithMouseY();
                }
            }
           

            //RotationControl();
           // rotateObject();
            //if ((totalRotation) < rotationDegreesAmount)
            //    SwingOpen();
        }
        else
        {
            
                ResetXRotation();
            
        }
    }

    void CopyRot()
    {
        Vector3 newRot = new Vector3(player.eulerAngles.x, froggy.eulerAngles.y, froggy.eulerAngles.z);
        transform.eulerAngles = newRot;
    }

    private void RotateBasedOnLineR()
    {
        //By diving by 1.. our rotation number gets lower the bigger the disis to avoid diving by 0tance is..
        //The reason we have bracket (1 + dist) 
        //This is a non linear equation
        //Should clamp here if you don't want weird things
        //moveAmount = (minSwipeDist - swipeDist) 0.01f;
         rotationX = ((1 - controlRenderBallisticPath.distanceClick) * 0.01f) ; //Mathf.Clamp(1 /  controlRenderBallisticPath.distanceClick, 0, 90);
        //rotationX = Mathf.Clamp(1 / controlRenderBallisticPath.distanceClick, 0, 90);

        transform.Rotate(Vector3.right, rotationX * speed);

    }

    private void ResetXRotation()
    {
        Vector3 rotXReset = new Vector3(0, froggy.eulerAngles.y, froggy.eulerAngles.z);
        transform.eulerAngles = rotXReset;
    }

    void RotOjAroundWithMouseX()
    {
        //rotX = Input.GetAxis("Mouse X") * speed * Mathf.Deg2Rad;
        rotY = Input.GetAxis("Mouse Y") * speed;

        //transform.Rotate(new Vector3(-1, froggy.rotation.y, froggy.rotation.z), rotY);

        //This need to face in the y direction to be more accurate
        
        //transform.Rotate(new Vector3(-1, froggy.eulerAngles.y, clickPoint.eulerAngles.z), rotY);

        //Classic
        transform.Rotate(Vector3.left,  rotY);

        //transform.eulerAngles = new Vector3(Mathf.Clamp(player.transform.eulerAngles.x, 0, 90), clickPoint.eulerAngles.y, clickPoint.eulerAngles.z);

        //transform.eulerAngles = new Vector3(-1 * rotY, froggy.eulerAngles.y, transform.eulerAngles.z);
    }

    void RotOjAroundWithMouseY()
    {
        //rotX = Input.GetAxis("Mouse X") * speed * Mathf.Deg2Rad;
        // rotY = Input.GetAxis("Mouse X") * speed;

        //transform.Rotate(Vector3.left, rotY);


        //
        transform.rotation = clickPoint.transform.rotation; //new Quaternion(transform.eulerAngles.x, clickPoint.transform.eulerAngles.y, transform.eulerAngles.z, clickPoint.transform.rotation.w);
        //transform.eulerAngles = new Vector3(player.eulerAngles.x, clickPoint.eulerAngles.y, clickPoint.eulerAngles.z);
        //Different way of copying rotation..might be more accurate
        //transform.eulerAngles = new Vector3();

        //This is makes it rotate correctly at the opposite of the point marker
        //We are copying the x rotation of the parent player
        transform.Rotate(new Vector3(player.transform.eulerAngles.x, 180, clickPoint.transform.eulerAngles.z));
    }

    private void rotateObject()
    {
        currentRotation = gameObject.transform.eulerAngles;
        targetRotation.x = (currentRotation.x + (90 * rotationDirection));
        StartCoroutine(objectRotationAnimation());
    }

    IEnumerator objectRotationAnimation()
    {
        // add rotation step to current rotation.
        currentRotation.x += (rotationStep * rotationDirection);
        gameObject.transform.eulerAngles = currentRotation;

        yield return new WaitForSeconds(0);

        if (((int)currentRotation.x >
(int)targetRotation.x && rotationDirection < 0) ||  // for clockwise
     ((int)currentRotation.x < (int)targetRotation.x && rotationDirection > 0)) // for anti-clockwise
        {
            StartCoroutine(objectRotationAnimation());
        }
        else
        {
            gameObject.transform.eulerAngles = targetRotation;
            //if (isCurrentObjectCollidingWithOtherObject(gameObject))
            //    StartCouroutine(rotateObjectAgain());
            //else
            //    isRotating = false;
        }
    }

   

    public void RotationControl()
    {
        if (!isRegularRotUsed)
        {
            transform.rotation = new Quaternion(transform.eulerAngles.x, clickPoint.transform.rotation.y, transform.eulerAngles.z, clickPoint.transform.rotation.w);


            //This is makes it rotate correctly at the opposite of the point marker
            transform.Rotate(new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z));
        }

        #region old stuff
        //mousePos = Input.mousePosition;
        ////float rotSpeed = 5;

        ////amount = Vector3.zero;

        //#region other way of rotating the character using the mouse
        ////Other way of rotating the character using the mouse
        ////		mousePos = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.transform.position.y - transform.position.y +5f));
        ////
        ////	
        ////		m_targetRotation = Quaternion.LookRotation(mousePos - new Vector3(transform.position.x, transform.position.y,  transform.position.z));
        ////		
        ////
        ////		transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y, m_targetRotation.eulerAngles.y, rotationSpeed * Time.deltaTime );
        //#endregion

        //if (isRegularRotUsed)
        //{
        //    #region Mouse Rotate Player
        //    ////SMOOTH WAY of rotating
        //    //// Generate a plane that intersects the transform's position with an upwards normal.
        //    //Plane playerPlane = new Plane(Vector3.up, playerChild.transform.position);

        //    //// Generate a ray from the cursor position
        //    //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        //    //// Determine the point where the cursor ray intersects the plane.
        //    //// This will be the point that the object must look towards to be looking at the mouse.
        //    //// Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //    ////   then find the point along that ray that meets that distance.  This will be the point
        //    ////   to look at.
        //    //float hitdist = 0.0f;

        //    //RaycastHit hit = new RaycastHit();
        //    //// If the ray is parallel to the plane, Raycast will return false.
        //    //if (playerPlane.Raycast(ray, out hitdist))
        //    //{
        //    //    // Get the point along the ray that hits the calculated distance.
        //    //    Vector3 targetPoint = ray.GetPoint(hitdist);

        //    //    // Determine the target rotation.  This is the rotation if the transform looks at the target point.
        //    //    Quaternion m_targetRotation = Quaternion.LookRotation(targetPoint - playerChild.transform.position);


        //    //    if (targetCall.isLockActivated == false)
        //    //    {
        //    //        // Smoothly rotate towards the target point.
        //    //        //playerChild.transform.rotation = Quaternion.Slerp(playerChild.transform.rotation, m_targetRotation, rotSpeed * Time.fixedDeltaTime); // WITH SPEED
        //    //        //Currently comment out so that we can use the click drag rotation instead...
        //    //        //transform.rotation = Quaternion.Slerp(transform.rotation, m_targetRotation, 1); // WITHOUT SPEED!!!
        //    //    }
        //    //    else if (targetCall.isLockActivated == true)
        //    //    {
        //    //        if (targetCall.selectedTarget != null)
        //    //        {
        //    //            lookTargetPos = targetCall.selectedTarget.transform.position - transform.position;
        //    //            lookTargetPos.y = 0;
        //    //            rotateToTarget = Quaternion.LookRotation(lookTargetPos);

        //    //            transform.rotation = Quaternion.Slerp(transform.rotation, rotateToTarget, Time.fixedDeltaTime * rotSpeed);

        //    //        }
        //    //        //Quaternion targetRotation = Quaternion.LookRotation(targetCall.selectedTarget.position, Vector3.up);
        //    //        //Quaternion newRotation = Quaternion.Lerp(playerChild.transform.rotation, targetRotation, speed * Time.deltaTime);

        //    //        // transform.rotation = newRotation;
        //    //        //playerChild.transform.rotation = Quaternion.Slerp(playerChild.transform.rotation, targetCall.selectedTarget.rotation, rotSpeed * Time.fixedDeltaTime);
        //    //    }

        //    //}

        //    #endregion

        //}
        //else
        //{


        //   // transform.rotation = new Quaternion(transform.eulerAngles.x, clickPoint.transform.rotation.y, transform.eulerAngles.z, clickPoint.transform.rotation.w);

        //    // transform.LookAt(clickPoint.position);
        //    //This is makes it rotate correctly at the opposite of the point marker
        //    //transform.Rotate(new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z));
        //}
        #endregion
    }
}
