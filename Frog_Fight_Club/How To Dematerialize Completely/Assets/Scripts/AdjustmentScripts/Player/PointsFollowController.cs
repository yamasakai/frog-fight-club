﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Flask;

public class PointsFollowController : MonoBehaviour 
{
    public GameObject HandController;
    
    public GameObject[] ArmPoints;

    public GameObject FingerMainFollow;

    public GameObject PointHoldFinger;

   // public GameObject[] FingerPoints;

    public int nbArmPoints;
    
    public float speed;

    public float speedFingerMain;
    
    public float avgFollowDist = 4;

    public 

   
   

	// Use this for initialization
	void Start () 
    {
       
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        BodyFollow();
	}

    void BodyFollow()
    {
        if (FingerMainFollow != null && PointHoldFinger)
        {
            #region
            //float dist1 = Vector3.Distance(FingerMainFollow.transform.position, HandController.transform.position);

            //if (dist1 >= avgFollowDist)
            //{
            //    #region Flask, TestETween
            //    float _omega = 1;
            //    FingerMainFollow.transform.position = ETween.Step(FingerMainFollow.transform.position,
            //        HandController.transform.position, _omega);
            //    #endregion
            //}
            #endregion
           // FingerMainFollow.transform.position = PointHoldFinger.transform.position;

        }

        if (ArmPoints.Length > 0)
        {
            float dist1 = Vector3.Distance(ArmPoints[0].transform.position, HandController.transform.position);

            if (dist1 >= avgFollowDist)
            {
                #region Flask, TestETween
                float _omega = 1;
                ArmPoints[0].transform.position = ETween.Step(ArmPoints[0].transform.position,
                    HandController.transform.position, _omega);
                #endregion
            }
        }

        if (ArmPoints.Length > 1)
        {
            float dist2 = Vector3.Distance(ArmPoints[1].transform.position, ArmPoints[0].transform.position);

            if (dist2 > avgFollowDist)
            {
                #region Flask, TestETween
                float _omega = 1;
                ArmPoints[1].transform.position = ETween.Step(ArmPoints[1].transform.position,
                    HandController.transform.position, _omega);
                #endregion
            }
        }
    }
}
