﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Flask;

public class WormMaster3D : MonoBehaviour 
{
    public GameObject wormHead1;
    public GameObject wormHead2;

    public GameObject[] wormBodyL;
    public GameObject[] wormBodyR;

    public int nbBodyPartL;
    public int nbBodyPartR;

    public float speed;
    
    private float followDist;

    private Vector3 pos1, pos2;

   
   

	// Use this for initialization
	void Start () 
    {
       // wormBodyL = new GameObject[nbBodyPartL];
       // wormBodyR = new GameObject[nbBodyPartR];
       
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        MoveHead();
        BodyFollow();
	}

    void BodyFollow()
    {
        float dist1 = Vector3.Distance(wormBodyL[0].transform.position, wormHead1.transform.position);
        float dist2 = Vector3.Distance(wormBodyL[1].transform.position, wormBodyL[0].transform.position);
        float dist3 = Vector3.Distance(wormBodyR[0].transform.position, wormHead2.transform.position);
        float dist4 = Vector3.Distance(wormBodyR[1].transform.position, wormBodyR[0].transform.position);
     
 
        if (dist1 > 4)
        {
            #region OG lookAt then += transform.forward
            //wormBodyL[0].transform.LookAt(wormHead1.transform.position);
            // wormBodyL[0].transform.position += wormBodyL[0].transform.forward * speed * Time.deltaTime;
            #endregion

            #region Flask, TestDTween
            //Flask, TestDTween
            //DTweenVector3 _position = new DTweenVector3(Vector3.zero, 1);

            //float _omega = 1;

            //_position.omega = _omega;
            //_position.Step(wormHead1.transform.position);
            //wormBodyL[0].transform.position = _position  ;
            #endregion

            #region Flask, TestETween
            float _omega = 1;
            wormBodyL[0].transform.position = ETween.Step(wormBodyL[0].transform.position, 
                wormHead1.transform.position, _omega);
            #endregion
        }

        if (dist2 > 6)
        {
            #region OG lookAt then += transform.forward
            //wormBodyL[1].transform.LookAt(wormBodyL[0].transform.position);
            //wormBodyL[1].transform.position += wormBodyL[1].transform.forward * speed * Time.deltaTime;
            #endregion

            #region Flask, TestETween
            float _omega = 1;
            wormBodyL[1].transform.position = ETween.Step(wormBodyL[1].transform.position,
                wormBodyL[0].transform.position, _omega);
            #endregion
        }


        if (dist3 > 4)
        {
            #region OG lookAt then += transform.forward
            //wormBodyR[0].transform.LookAt(wormHead2.transform.position);
            //wormBodyR[0].transform.position += wormBodyR[0].transform.forward * speed * Time.deltaTime;
            #endregion

            #region Flask, TestETween
            float _omega = 1;
            wormBodyR[0].transform.position = ETween.Step(wormBodyR[0].transform.position,
                wormHead2.transform.position, _omega);
            #endregion
        }

        if (dist4 > 6)
        {
            #region OG lookAt then += transform.forward
            //wormBodyR[1].transform.LookAt(wormBodyR[0].transform.position);
            //wormBodyR[1].transform.position += wormBodyR[1].transform.forward * speed * Time.deltaTime;
            #endregion

            #region Flask, TestETween
            float _omega = 1;
            wormBodyR[1].transform.position = ETween.Step(wormBodyR[1].transform.position,
                wormBodyR[0].transform.position, _omega);
            #endregion
        }
    }

    void CreateCollider()
    {

    }


    void MoveHead()
    {
        if (wormHead1 != null && wormHead1.GetComponent<Rigidbody>() != null && wormHead2 != null && wormHead2.GetComponent<Rigidbody>() != null)
        {
            pos1 = new Vector3(Mathf.Lerp(0, Input.GetAxis("Horizontal") * speed, 0.8f), 0, Mathf.Lerp(0, Input.GetAxis("Vertical") * speed, 0.8f));

            wormHead1.GetComponent<Rigidbody>().velocity = pos1;

            pos2 = new Vector3(Mathf.Lerp(0, Input.GetAxis("Horizontal_Alt") * speed, 0.8f), 0, Mathf.Lerp(0, Input.GetAxis("Vertical_Alt") * speed, 0.8f));

            wormHead2.GetComponent<Rigidbody>().velocity = pos2;
        }
    }


}

//private PlacebleUnit placebleUnit;
//    private Transform currentUnit;
//    private bool hasBeenPlace = false;

//    // Use this for initialization
//    void Start () {
	
//    }
	
//    // Update is called once per frame
//    void Update () {
	
//        if(currentUnit != null && !hasBeenPlace)
//        {
//            Vector2 place = Input.mousePosition;
//            place = new Vector2(place.x, place.y);
//            Vector2 p = camera.ScreenToWorldPoint(place);
//            currentUnit.position = new Vector2(p.x,p.y);

//            if(Input.GetMouseButtonDown(0))
//           {
//                if(isLegalPosition())
//                {
//                hasBeenPlace = true;
//                }
//            }
//        }


//    }

//    public void setItem(GameObject obj)
//    {
//        hasBeenPlace = false;
//        currentUnit = ((GameObject)Instantiate(obj)).transform;

//        placebleUnit = currentUnit.GetComponent<PlacebleUnit>();
//    }

//    bool isLegalPosition()
//    {
//        if(placebleUnit.colliders.Count > 0)
//        {
//            return false;
//        }

//        return true;
//    }
//}

//public bool isSelected = false;
	
//    // Update is called once per frame
//    void Update () 
//    {
	
//        if(renderer.isVisible && Input.GetMouseButton(0))
//        {
//            Vector2 camPos = Camera.main.WorldToScreenPoint(transform.position);
//            camPos.y = Control.InvertMouseY(camPos.y);
//            isSelected = Control.selection.Contains(camPos);
//        }

//        if(isSelected)
//        {
//            renderer.material.color = Color.red;
//        }
//        else
//        {
//            renderer.material.color = Color.white;
//        }
//    }
