﻿using UnityEngine;
using System.Collections;

public class TongueMaster : MonoBehaviour 
{
    //public GameObject target;
    public LineRendererDrag lineRendererDrag;
    public Vector3 endPoint;
    public GameObject TongueCrossHair;
	public GameObject tongueMainPoint;
	public GameObject tongueMainRestPoint;
	public float timeFromAToB = 0.3f;
	public float dist = 15.00f;
	public float returnSpeed = 10f;
    public bool didReachEndOiunt;
    public bool didReachEnemy;
	public bool didShootTongue = false;
	public bool isFixReturn = false;
	public bool limitRange = true;
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {

		StartCoroutine(ShootTongue());

        endPoint = lineRendererDrag.MousePointPos();

        if (!didShootTongue && tongueMainPoint.transform.position != tongueMainRestPoint.transform.position && isFixReturn)
		{
			tongueMainPoint.transform.position = Vector3.Lerp(tongueMainPoint.transform.position,
			                                                  tongueMainRestPoint.transform.position, Time.fixedDeltaTime * returnSpeed);
		}


	}

	IEnumerator ShootTongue()
	{
		if (lineRendererDrag.isRelease)
             //(Input.GetKeyDown (KeyCode.Mouse1)) 
		{
			yield return Helper.EaseLerpAToB (tongueMainPoint.transform, tongueMainRestPoint.transform.position, endPoint, timeFromAToB);
			                                 
			//yield return Helper.EaseLerpAToB (tongueMainPoint.transform, transform.position, tongueMainRestPoint.transform.position, timeFromAToB);

			didShootTongue = true;
		} 
		else 
		{
			didShootTongue = false;
		}

		yield return null;
	}

	///// <summary>
	///// This function use a raycast to return a vector3 where the mouse click clicked in 3D else it just return the orginal 
	///// position of the pointA
	///// </summary>
	///// <returns></returns>
	public Vector3 PositionClicked()
	{
			//Converting Mouse Pos to 2D (vector2) World Pos/ Use ScreenToWorldPoint for 2D and use ScreenPointToRay for 3D
			Ray rayDest = Camera.main.ScreenPointToRay (Input.mousePosition);
			
			
			RaycastHit hit = new RaycastHit ();
		if (limitRange == false) 
		{
			
			if (Physics.Raycast (rayDest, out hit, dist)) {
				//Debug.Log(hit.transform.name);
				
				Debug.DrawRay (tongueMainRestPoint.transform.position, hit.point, Color.red, 5.0f, false);
				//Debug.DrawRay (pointB.transform.position, hit.point, Color.red, 5.0f, false);
				return new Vector3 (hit.point.x, transform.position.y, hit.point.z);
			
			} else
				return tongueMainRestPoint.transform.position;
		}
		else 
		{
			return TongueCrossHair.transform.position;
		}
	}


}