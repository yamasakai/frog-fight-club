//using UnityEngine;
//using System.Collections;
//
//public class Entity : MonoBehaviour {
//
//
//	public float health;
//
//	public GameObject deathFX;
//	public GameObject ragDoll;
//	public SkinnedMeshRenderer skinnedMeshRenderer;
//	public MeshRenderer meshRenderer;
//
//	public bool isDestroyDeathFX;
//	public bool isDestroyRagdoll;
//
//	private bool isInvincible;
//	private bool isIgnoreEnemyLayer;
//
//	void Awake()
//	{
//
//	}
//
//	// Use this for initialization
//	void Start () 
//	{
//
//	}
//	
//	// Update is called once per frame
//	void Update () 
//	{
//	
//
//	}
//
//	//Virtual so I can modify this function from whatever class inherit from entity.
//	public virtual float TakeDamage(float dmg)
//	{
//		health -= dmg;
//
//		//Debug.Log (health);
//
//		StartCoroutine("FlashOnDamage");
//
//		if(health <= 0)
//		{
//			Die();
//		}
//
//		return dmg;
//	}
//
//	/// <summary>
//	/// Die/Kill/Destroy this instance.
//	/// </summary>
//	public virtual void Die()
//	{
//
//		//Debug.Log("Enemy died");
//
//        //HighScoreManager._instance.currentScore.score += 100;
//
//		if(deathFX)
//		{
//		
//		GameObject xplodePrefab = ((GameObject)Instantiate(deathFX, transform.position, transform.rotation));
//		
//		if(isDestroyDeathFX == true)
//		{
//
//		Destroy(xplodePrefab, 3.7f);
//		
//		}
//
//		}
//
//        //if(ragDoll)
//        //{
//        //    //Ragdoll ragDollPrefab = (Instantiate(ragDoll, transform.position, transform.rotation) as GameObject).GetComponent<Ragdoll>();
//        //    ragDollPrefab.CopyPose(transform);
//
//        //    if(isDestroyRagdoll)
//        //    {
//        //        Destroy(ragDollPrefab, 3.7f);
//        //    }
//        //}
//
//		Destroy(this.gameObject);
//
//
//        //if(MobSpawnManager.mobSpawnInst.numbMobAlive != null)
//        //{
//        //    MobSpawnManager.mobSpawnInst.numbMobAlive--;
//        //    //Debug.Log("Minus 1 Mob Alive");
//        //}
//	}
//
//	/// <summary>
//	/// Becomes the invincible.
//	/// </summary>
//	public virtual void becomeInvincible()
//	{
//		isInvincible = true;
//		
//		Physics.IgnoreLayerCollision(8,15, true);
//		Physics.IgnoreLayerCollision(8,16, true);
//		Debug.Log("Invincible");
//	}
//
//
//	/// <summary>
//	/// Becomes the mortal. Switch to his original color again because of weird bug going on.
//	/// </summary>
//	public virtual void becomeMortal()
//	{
//
//		isInvincible = false;
//		Physics.IgnoreLayerCollision(8,15, false);
//		Physics.IgnoreLayerCollision(8,16, false);
//
////		//Making sure that the color goes back to normal because of weird bug.
////		if(skinnedMeshRenderer)
////		{
////			//Taking lenght of our meshFilter array
////			int lengthFilter = skinnedMeshRenderer.renderer.materials.Length;
////
////			//Create original color array to put save our original color
////			Color[] originalCol = new Color[lengthFilter];
////
////		
////			//Put back the original color 
////			for(int x = 0; x < skinnedMeshRenderer.renderer.materials.Length; x++)
////			{
////			skinnedMeshRenderer.renderer.materials[x].SetColor("_BrightColor", originalCol[x]);
////		
////			}
////
////		}
////		else if(meshRenderer)
////		{
////			//Taking lenght of our meshFilter array
////			int lengthFilter = meshRenderer.renderer.materials.Length;
////			
////			//Create original color array to put save our original color
////			Color[] originalCol = new Color[lengthFilter];
////
////			//Put back the original color 
////			for(int x = 0; x < meshRenderer.renderer.materials.Length; x++)
////			{
////				meshRenderer.renderer.materials[x].SetColor("_BrightColor", originalCol[x]);
////			}
////		}
//	}
//
//	/// <summary>
//	/// Flashs the character on damage.
//	/// </summary>
//	/// <returns>The on damage.</returns>
//	IEnumerator FlashOnDamage()
//	{
//
//		if(skinnedMeshRenderer)
//		{
//			//Taking lenght of our meshFilter array
//			int lengthFilter = skinnedMeshRenderer.GetComponent<Renderer>().materials.Length;
//
//			//copying our meshFilter array
//			Material[] oldMatArray = skinnedMeshRenderer.GetComponent<Renderer>().materials;
//
//			//Create original color array to put save our original color
//			Color[] originalCol = new Color[lengthFilter];
//
//			//Iterating into our meshFilter array to save the original color in originalCol array
//			for(int i = 0; i < skinnedMeshRenderer.GetComponent<Renderer>().materials.Length; i++ )
//			{	
//				originalCol[i] = skinnedMeshRenderer.GetComponent<Renderer>().materials[i].GetColor("_BrightColor");
//			}
//
//			//Set the color to red on the oldMatArray
//			for(int i = 0; i < skinnedMeshRenderer.GetComponent<Renderer>().materials.Length; i++ )
//				{	
//					oldMatArray[i].SetColor("_BrightColor", Color.red);
//					//Debug.Log("In the loop setting all mats red");
//				}
//				
//			//Apply the color red into the meshFilter materials
//			skinnedMeshRenderer.GetComponent<Renderer>().materials = oldMatArray;
//				//Debug.Log("Just turned red");
//
//			//Give invincibility for 0.5 sec and also ignore collision of enemy bullet and enemy itself
//			becomeInvincible();
//
//			//Wait for 0.5f seconds
//			yield return new WaitForSeconds(0.5f);
//			yield return new WaitForEndOfFrame();//WaitForSeconds(0.5f);
//
//			//Can get damaged
//			becomeMortal();
//
//
//			//Put back the original color 
//			for(int x = 0; x < skinnedMeshRenderer.GetComponent<Renderer>().materials.Length; x++)
//			{
//				skinnedMeshRenderer.GetComponent<Renderer>().materials[x].SetColor("_BrightColor", originalCol[x]);
//			}
//		}
//		else if(meshRenderer)
//		{
//			//Taking lenght of our meshFilter array
//			int lengthFilter = meshRenderer.GetComponent<Renderer>().materials.Length;
//			
//			//copying our meshFilter array
//			Material[] oldMatArray = meshRenderer.GetComponent<Renderer>().materials;
//			
//			//Create original color array to put save our original color
//			Color[] originalCol = new Color[lengthFilter];
//			
//			//Iterating into our meshFilter array to save the original color in originalCol array
//			for(int i = 0; i < meshRenderer.GetComponent<Renderer>().materials.Length; i++ )
//			{	
//				originalCol[i] = meshRenderer.GetComponent<Renderer>().materials[i].GetColor("_BrightColor");
//			}
//			
//			//Set the color to red on the oldMatArray
//			for(int i = 0; i < meshRenderer.GetComponent<Renderer>().materials.Length; i++ )
//			{	
//				oldMatArray[i].SetColor("_BrightColor", Color.red);
//				//Debug.Log("In the loop setting all mats red");
//			}
//			
//			//Apply the color red into the meshFilter materials
//			meshRenderer.GetComponent<Renderer>().materials = oldMatArray;
//			//Debug.Log("Just turned red");
//			
//			//Give invincibility for 0.5 sec and also ignore collision of enemy bullet and enemy itself
//			becomeInvincible();
//			
//			//Wait for 0.5f seconds
//			yield return new WaitForSeconds(0.5f);
//			yield return new WaitForEndOfFrame();//WaitForSeconds(0.5f);
//			
//			//Can get damaged
//			becomeMortal();
//			
//			
//			//Put back the original color 
//			for(int x = 0; x < meshRenderer.GetComponent<Renderer>().materials.Length; x++)
//			{
//				meshRenderer.GetComponent<Renderer>().materials[x].SetColor("_BrightColor", originalCol[x]);
//			}
//
//		}
//
//
//	}
//	
//	//Virtual so I can modify this function from whatever class inherit from entity.
//	public virtual float PlayerTakeDamage(float dmg)
//	{
//		if(!isInvincible)
//		{
//
//		health -= dmg;
//		//renderer.material.color = Color.white;
//		//Debug.Log (health);
//
//		StartCoroutine("FlashOnDamage");
//
//		if(health <= 0)
//		{
//			PlayerDie();
//                //AutoFade.SimpleFade(0.1f, 0.1f, Color.red, false);
//		}
//		
//		}
//		
//		return dmg;
//	}
//	
//	//Might make a die function just for enemy.
//	public virtual float PlayerDie()
//	{
//		//Debug.Log("Player died");
//		Time.timeScale = 0.5f;
//
//		Destroy(gameObject);
//
//		if(deathFX)
//		{
//
//		    GameObject deathFXPrefab = (GameObject)Instantiate(deathFX, transform.position, Quaternion.identity);
//
//			if(isDestroyDeathFX == true)
//			{
//				Destroy(deathFXPrefab, 3.7f);
//			}
//		}
//
//        //if(ragDoll)
//        //{
//        //    //Ragdoll ragDollPrefab = (Instantiate(ragDoll, transform.position, transform.rotation) as GameObject).GetComponent<Ragdoll>();
//        //    ragDollPrefab.CopyPose(transform);
//			
//        //    if(isDestroyRagdoll)
//        //    {
//        //        Destroy(ragDollPrefab, 3.7f);
//        //    }
//        //}
//
//		//Application.LoadLevel("GameOver");
//
//		return 0;
//		
//	}
//	
//
//	//This function will be used to pick the closest target (The player/Sheep)
//	public virtual  GameObject pickClosestTarget(GameObject[] closeTargets, string targetName)
//	{
//		float nearestDistance = Mathf.Infinity;
//		
//		GameObject nearestObj = null;
//		
//		closeTargets = GameObject.FindGameObjectsWithTag(targetName);
//		
//		foreach(GameObject obj in closeTargets)
//		{
//			
//			Vector3 objDist = obj.transform.position;
//			
//			float distSqr = (objDist - transform.position).sqrMagnitude;
//			
//			if(distSqr < nearestDistance)
//			{
//				nearestObj = obj;
//				
//				nearestDistance = distSqr;
//				
//			}
//			
//		}
//		
//		return nearestObj;
//
//	}
//
//
//		
//}
