﻿using UnityEngine;
using System.Collections;

public class DeadZone : MonoBehaviour 
{
    public GameManager1 manager;

	// Use this for initialization
	void Start () 
    {

        if (GameObject.FindGameObjectWithTag("MainCamera"))
        {
            manager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager1>();
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            print("Player In The Dead Zone");
         
            //other.GetComponent<PlayerTwoD>().PlayerDeath();

            manager.CallGameOver();
        }
    }
}
