﻿using UnityEngine;
using System.Collections;

public class SpawnRandomArea : MonoBehaviour 
{
    public GameObject[] objectsArray;
    public float maxNumberObjs;
    
    [Range (0, 1)]
    public float minPotentialDist = 0.1f;
    [Range(0, 10)]
    public float maxPotentialDist = 3.5f;
    
    [HideInInspector]
    public float currentNumberObjs;


    void Awake()
    {
        currentNumberObjs = maxNumberObjs;
        ObjectSpawn();
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void ObjectSpawn()
    {
        //Will pick random planet within the array

        //Will instantiate maxNumberOfPlanets
        for (int i = 0; i <= maxNumberObjs; i++)
        {
            GameObject obj;
            obj = objectsArray[Random.Range(0, objectsArray.Length)];

            float potentialDist = Random.Range(minPotentialDist, maxPotentialDist);

            Vector2 rndPosWithin;
            rndPosWithin = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            rndPosWithin = transform.TransformPoint(rndPosWithin * potentialDist);

            obj = ((GameObject)Instantiate(obj, rndPosWithin, obj.transform.rotation)).gameObject;
        }
    }
}
