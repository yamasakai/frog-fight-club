﻿using UnityEngine;
using System.Collections;

public class ArtificialChildParentTransformRelationship : MonoBehaviour 
{
	public Transform parentTransform;

	Vector3 startParentPosition;
	Quaternion startParentRotationQ;
	Vector3 startParentScale;

	Vector3 startChildPosition;
	Quaternion startChildRotationQ;
	Vector3 startChildScale;

	Matrix4x4 parentMatrix;


	// Use this for initialization
	void Start () 
	{
		startParentPosition = parentTransform.position;
		startParentRotationQ = parentTransform.rotation;
		startParentScale = parentTransform.lossyScale;

		startChildPosition = transform.position;
		startChildRotationQ = transform.rotation;
		startChildScale = transform.lossyScale;

		// Keeps child position from being modified at the start by the parent's initial transform
		startChildPosition = DivideVectors(Quaternion.Inverse(parentTransform.rotation) * (startChildPosition - startParentPosition), startParentScale);
	}
	
	// Update is called once per frame
	void Update () 
	{
		parentMatrix = Matrix4x4.TRS(new Vector3(parentTransform.position.x, 1, parentTransform.position.z), parentTransform.rotation, parentTransform.lossyScale);

		transform.position = parentMatrix.MultiplyPoint3x4(startChildPosition);

		transform.rotation = (parentTransform.rotation * Quaternion.Inverse(startParentRotationQ)) * startChildRotationQ;

	}

	Vector3 DivideVectors(Vector3 num, Vector3 den)
	{

		return new Vector3(num.x / den.x, num.y / den.y, num.z / den.z);

	}


}
