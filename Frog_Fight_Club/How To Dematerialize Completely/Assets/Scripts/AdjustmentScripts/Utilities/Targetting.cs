﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Targetting : MonoBehaviour
{
    public List<GameObject> targets;

    // do not manual select the targets, this will be filled by the scripted with object that have the defines tag
    public GameObject selectedTarget;
	public GameObject previousTarget;
	public GameObject closestTarget;
	public GameObject lockMarker;
    public string targetTag = "Enemy";
    public int index;
    public bool isLockActivated = false;



    //Stuff that keeps messing up Part 1
    private Transform myTransform;

    //Use this for initialization
    void Start()
    {

        print("Start Pre AddAllEnemies. Target Count: " + targets.Count);

        targets = new List<GameObject>();
        selectedTarget = null;
        //Stuff that keeps messing up Part 2
        myTransform = transform;

        AddAllEnemies();


        print("Start Past AddAllEnemies. Target Count: " + targets.Count);
    }

    public void AddAllEnemies()
    {
        GameObject[] go = GameObject.FindGameObjectsWithTag(targetTag);

        foreach (GameObject enemy in go)
        {
            AddTarget(enemy);
        }
    }

	public void AddTarget(GameObject enemy)
    {
        targets.Add(enemy);
    }

    //Stuff that keeps messing up Part 3
    private void SortTargetsByDistance()
    {
		targets.Sort(delegate(GameObject t1, GameObject t2)
        {
            return (Vector3.Distance(t1.transform.position, myTransform.position).CompareTo)
					(Vector3.Distance(t2.transform.position, myTransform.position));
        });
    }

    private void TargetEnemy()
    {
        SortTargetsByDistance();

        if (closestTarget != targets[0])
        {
            index = 0;
            closestTarget = targets[0];
        }

        if (selectedTarget == null)
        {
            //Stuff that keeps messing me up Part 4

            selectedTarget = targets[index];
            ShowSelectedTarget(selectedTarget);
        }

        //// EDIT: Add this code; now we can jump 2 all the found enemy's by pressing TAB
        else
        {
            //index = targets.IndexOf(selectedTarget);

            #region Why compare against targets.count - 1?
            // because it start at 0 which is actualy already 1 when compared to target.count.
            // Index is going from 0 to 5. 
            //Example: the biggest number that you will have to compare
            //is 5 < 5 ? no so it will go to the else to restart the index at 0
            //Why we are comparing targets.count -1?
            //Because as the index get bigger we cannot go past the actual
            //real number of targets that we have..else the index goes out of bound.
            //
            #endregion
            //if (index < targets.Count - 1)

            previousTarget = selectedTarget;
            ShowDeSelectedTarget(previousTarget);
            TargetEnemy();
        }
        
        closestTarget = targets[0];
    }


	public MeshRenderer CurrentSelectedTargetMeshRenderer()
	{
		if (selectedTarget != null)
			return selectedTarget.GetComponent<MeshRenderer> ();
		else
			return null;
	}


    //// EDIT: Add this
    private void ShowSelectedTarget(GameObject target)
    {
        if (target != null)
        {
			//Color originCol = target.GetComponent<Renderer> ().material.color;
            //target.GetComponent<Renderer>().material.color = Color.red;

			//lockMarker = target.GetComponent<Renderer> ();
			//lockMarker.SetActive (true);
			//lockMarker.enabled = true;
			if(	target.GetComponent<MeshRenderer> ().receiveShadows == true)
			target.GetComponent<MeshRenderer> ().receiveShadows = false;

			lockMarker = Helper.getChildGameObject (target, "EnemyLockedMarker");
			lockMarker.GetComponent<MeshRenderer> ().enabled = true;
			//CurrentSelectedTargetMeshRenderer ();
		}

        // this is used if your going to used the a "player-attack-script"
        // PlayerAttack attack = (PlayerAttack)GetComponents("PlayerAttack");
        // attack.target = selectedTarget.gameObject:
    }



    //// EDIT: Add this
	private void ShowDeSelectedTarget(GameObject target)
    {
        if (target != null)
        {
            //target.GetComponent<Renderer>().material.color = Color.red;
           
			//selectedTarget = null;
			if(	target.GetComponent<MeshRenderer> ().receiveShadows == false)
			target.GetComponent<MeshRenderer> ().receiveShadows = true;
			//CurrentSelectedTargetMeshRenderer (target.GetComponent<MeshRenderer> ());
			lockMarker = Helper.getChildGameObject (target, "EnemyLockedMarker");
			lockMarker.GetComponent<MeshRenderer> ().enabled = false;

			selectedTarget = null;


        }
    }


    //Update is called once per frame
    void Update() 
	{
		//if(Input.GetKeyDown(KeyCode.Tab))
		//{
        //Middle mouse button
       // if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Alpha2))//if(Input.GetMouseButtonDown(2))

		if (Input.GetMouseButtonDown(2)) //(Input.GetKeyDown(KeyCode.Tab))
        {
            isLockActivated = !isLockActivated;
            if (isLockActivated)
            {
                //print("Lock ON");
            }
            else if (!isLockActivated)
            {
               // print("Lock OFF");
                
                ShowDeSelectedTarget(selectedTarget);
            }
        }

        //Lock the closest enemy
        if (Input.GetMouseButtonDown(2) && isLockActivated)
        {
           
            TargetEnemy ();  
        }

        //Lock the next enemy in the array
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && isLockActivated)
        {
            index++;

            if (index > targets.Count - 1)
            {
                index = 0;
            }

            TargetEnemy();
        }

        //Lock the previous enemy in the array
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && isLockActivated)
        {
            index--;

            if (index < 0)
            {
                index = targets.Count - 1;
            }

            TargetEnemy();
        }
	}
}