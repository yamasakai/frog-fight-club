﻿using UnityEngine;
using System.Collections;

public class InteractionViaButton : MonoBehaviour 
{
    public GameObject activateFX;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    [Signal]
    void TurnOn()
    {
        Debug.Log("Turn On...");
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.GetComponent<Collider2D>().enabled = true;
        if (activateFX != null)
        {
            Instantiate(activateFX, transform.position, Quaternion.identity);
        }
        StartCoroutine(transform.ScaleTo(new Vector3(1, 1, 1), 1.5f, EaseTypeAuto.ElasticOut));
    }

    [Signal]
    void TurnOff()
    {
        Debug.Log("Turn Off...");
    }
}
