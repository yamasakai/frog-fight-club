﻿using UnityEngine;
using System.Collections;

public class RenderBallisticPath : MonoBehaviour
{
    public bool isRenderPath;
    public bool isFollowPlayerRot;
    public Transform player;
	public Transform startPoint;
	public GameObject explosionDisplay;

    
	public float initialVelocity = 10.0f;
	public float timeResolution = 0.02f;
	public float maxTime = 10.0f; //Show full 10sec of ballistic projectile

	public LayerMask layerMask = -1;



	// Use this for initialization
	void Start ()
	{

		lineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	/// <summary>
	/// figure out and trace the path the object will travel
	/// </summary>
	void Update ()
	{
        if (isRenderPath)
        {
            Vector3 velocityVector = transform.forward * initialVelocity;

            //Number of point the line will have
            lineRenderer.positionCount = ((int)(maxTime / timeResolution));

            int index = 0;


            Vector3 currentPosition = player.transform.position;
                currentPosition = startPoint.position;

            //Move t from 0 to maxtime in steps timeResolution
            //
            for (float t = 0.0f; t < maxTime; t += timeResolution)
            {
                lineRenderer.SetPosition(index, currentPosition);

                RaycastHit hit;

                //Need this to detect if we hit something. This is a raycast raymarch each point will raycast from their pos.
                //if performance is affected add more resolution to skip points.
                if (Physics.Raycast(currentPosition, velocityVector, out hit, velocityVector.magnitude * timeResolution, layerMask))
                {
                    lineRenderer.positionCount = (index + 2);

                    lineRenderer.SetPosition(index + 1, hit.point);//this will put the line renderer at the final point

                    if (explosionDisplay != null)
                    {
                        if (explosionDisplayInstance != null)
                        {
                            explosionDisplayInstance.SetActive(true);
                            explosionDisplayInstance.transform.position = hit.point;
                        }
                        else
                        {
                            explosionDisplayInstance = Instantiate(explosionDisplay, hit.point, Quaternion.identity) as GameObject;
                            explosionDisplayInstance.SetActive(true);
                        }
                    }
                    //If we hit then get out of the loop, we don't need to see the other points
                    break;
                }
                else
                {
                    if (explosionDisplayInstance != null)
                    {
                        explosionDisplayInstance.SetActive(false);
                    }
                }
                //CurrentPosition increase based on vel * time. *Reminder displacement = vel * t
                currentPosition += velocityVector * timeResolution;
                //Making sure that our vel is affected by gravity
                velocityVector += Physics.gravity * timeResolution;
                index++;
            }

        }

        if(isFollowPlayerRot && player != null)
        {
            this.transform.rotation = new Quaternion(this.transform.rotation.x, player.transform.rotation.y, this.transform.rotation.z, this.transform.rotation.w);
        }

	}

	private GameObject explosionDisplayInstance;
	
	private LineRenderer lineRenderer;
}
