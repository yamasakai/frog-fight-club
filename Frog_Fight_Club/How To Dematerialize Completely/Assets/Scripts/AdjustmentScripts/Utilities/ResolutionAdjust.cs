﻿using UnityEngine;
using System.Collections;

public class ResolutionAdjust : MonoBehaviour 
{
    public float floatable = 100.0f; //This can be PixelsPerUnit, or you can change it during runtime to alter the camera.

    void Awake()
    {
       // GetComponent<Camera>().orthographicSize = (Screen.height / 100f / 2.0f); // 100f is the PixelPerUnit that you have set on your sprite. Default is 100.
    }

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
    void Update ()
    {
       GetComponent<Camera>().orthographicSize = Screen.height *  GetComponent<Camera>().rect.height  / floatable / 2.0f; //- 0.1f;
    }


}
