﻿using UnityEngine;
using System.Collections;

public class RecycleObj : MonoBehaviour 
{
    public bool isRecycleOnCol;
    public float duration;
    

    void OnEnable()
    {
        if (!isRecycleOnCol)
        {
            StartCoroutine(Recycle());
        }
    }

    IEnumerator Recycle()
    {
        float elapsed = 0;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            yield return 0;
        }

        //Recycle this pooled explosion instance
        gameObject.Recycle();
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject)
        {
            if (isRecycleOnCol)
            {
                this.gameObject.Recycle();
            }
        }
    }
}
