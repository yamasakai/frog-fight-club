﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour 
{
    public GameObject onIndicator;
    public Signal onPress;
    int ActivateCount = 0;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	

	}

    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && ActivateCount < 1)
        {
            onPress.Invoke();
            ActivateCount++;

            if(onIndicator != null)
            {
                onIndicator.GetComponent<SpriteRenderer>().enabled = true;
                this.transform.GetComponent<SpriteRenderer>().enabled = false;
                StartCoroutine(onIndicator.transform.ScaleTo(new Vector3(1, 1, 1), 1.5f, EaseTypeAuto.ElasticOut));
            }
        }
    }


}
