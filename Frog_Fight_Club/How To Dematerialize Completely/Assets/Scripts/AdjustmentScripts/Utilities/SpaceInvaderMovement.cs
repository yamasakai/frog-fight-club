﻿using UnityEngine;
using System.Collections;

public class SpaceInvaderMovement : MonoBehaviour 
{
    Vector2 moveDirection;
    public float speed = 3;
    public float leftDist, rightDist;

    void Update()
    {
        SpaceInvaderMovementLogic();
    }

    #region SpaceInvaderMovement
    //Goes left and right between 1 and 10 on the x axis.
    //After arriving to one of the edge it goes down one unit in the y axis.
    public void SpaceInvaderMovementLogic()
    {
        float smallPushX = 0.5f; //downer = 0.5f;
        Vector3 newPos = transform.position;

        newPos.y += smallPushX; //-= downer


        //IF enemy x position is less than player negative position move in a positive direction (right)
        if (transform.position.x <= -leftDist)
        {

            moveDirection.x = 1;
            transform.position = newPos;
        }

        //(left)
        else if (transform.position.x >= rightDist)
        {

            moveDirection.x = -1;
            transform.position = newPos;

        }

        transform.Translate(new Vector3(moveDirection.x * speed * Time.deltaTime, 0, 0));


    }
    #endregion
}
