﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthUI : MonoBehaviour {

	public Image currentHealthbar;
	public Text ratioText;

	private float hp = 150;
	private float maxHP = 150;


	public bool isHPPercDisplay = true;

	// Use this for initialization
	void Start () 
	{
		UpdateHealthBar ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	private void UpdateHealthBar()
	{
		//Will give a value between 0 and 1. Because hp cannot be above maxHp
		float ration = hp / maxHP;

		currentHealthbar .rectTransform.localScale = new Vector3 (ration, 1, 1);
		//0 argument force tostring to display only one decimal

		if (isHPPercDisplay) 
		{
			ratioText.text = (ration * 100).ToString ("0") + '%';
		}
	
	}

	private void TakeDamage(float damage)
	{
		hp -= damage;
		if (hp < 0)
		{
			hp = 0;
			Debug.Log("Dead");
		}

		UpdateHealthBar ();
	}

	private void HealAmount(float heal)
	{
		hp += heal;
		if (hp > maxHP)
		{
			hp = maxHP;

		}

		UpdateHealthBar ();
	}
}
