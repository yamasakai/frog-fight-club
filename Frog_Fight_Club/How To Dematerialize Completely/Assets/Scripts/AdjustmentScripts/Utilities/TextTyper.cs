﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Advices/Hints/Potential change
/// How to move independently each individual letters that are in a string format on the text component in unity?
/// You don't!
/// -There's some methods that involve rendering the texture, then splitting it up into different letters and moving the rendered letters.
/// -You could also create something that initiates a text component for each letter. Use object pooling if you're doing that.
/// </summary>
/// Mush:
////Over the last few days, I've been working on the custom text renderer as you can see in those gifz 
//^^^ You can mix and match any effect as the effect class is just a buncha floats such as the text wave parameters,
//color wave parameters, shake amount, etc., so everything can be used and combined. 
//You can make text wave, shake, spin, and flash any color all together and it's easy to use. yeeeeeeeeeeeeeeeeeeeeeee
public class TextTyper : MonoBehaviour
{

    public float letterPause = 0.2f;
    public AudioClip typeSound1;
    public AudioClip typeSound2;

    string message;
    Text textComp;

    // Use this for initialization
    void Start()
    {
        if (GetComponent<Text>() != null)
        {
            textComp = GetComponent<Text>();
            message = textComp.text;
            textComp.text = "";
            StartCoroutine(TypeText());
        }
    }

    IEnumerator TypeText()
    {
        foreach (char letter in message.ToCharArray())
        {
            textComp.text += letter;
            if (typeSound1 && typeSound2)
                //SoundManager.instance.RandomizeSfx(typeSound1, typeSound2);
            yield return 0;
            yield return new WaitForSeconds(letterPause);
        }
    }
}