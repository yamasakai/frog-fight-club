﻿using UnityEngine;
using System.Collections;

public class SphereGizmos : MonoBehaviour 
{
	public float gizmosSize = 0.75f;
	public Color gizmosColor = Color.yellow;

	void OnDrawGizmos()
	{
		Gizmos.color = gizmosColor;
		Gizmos.DrawWireSphere (transform.position, gizmosSize);
	}
}