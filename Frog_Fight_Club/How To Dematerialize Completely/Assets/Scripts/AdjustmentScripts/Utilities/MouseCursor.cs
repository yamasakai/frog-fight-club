﻿using UnityEngine;
using System.Collections;
//using UnityStandardAssets.CrossPlatformInput;

public class MouseCursor : MonoBehaviour 
{
    public bool shouldCreateHandMadeCursor = false;
    public bool isCursorVisible = false;
    public bool useHandMadeCursor = false;
    public float depth = 10.00f;

    private Vector3 mousePos;
    
    [SerializeField]
    private Texture aimTexture;

    [SerializeField]
    private GameObject handMadeCursor;
    private GameObject handClone;

    void Start()
    {
        Cursor.visible = isCursorVisible;

        if (shouldCreateHandMadeCursor)
        {
            handClone = Instantiate(handMadeCursor, mousePos, Quaternion.identity) as GameObject;
        }
    }

    void Update()
    {
        if (handMadeCursor != null && useHandMadeCursor)
        {
            mousePos = Input.mousePosition;

           Vector3 wantedPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, depth));

           handClone.transform.position = wantedPos;

          // handMadeCursor.transform.position = wantedPos;
        }

        if (Input.GetMouseButtonDown(0))
        {
            ClickSelect();
            
        }
    
    }

    //This method returns the game object that was clicked using Raycast 2D
    GameObject ClickSelect()
     {
        //Converting Mouse Pos to 2D (vector2) World Pos/ Use ScreenToWorldPoint for 2D and use ScreenPointToRay for 3D
        Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, 
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        
        RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);
         
         if (hit)
         {
              Debug.Log(hit.transform.name);
             return hit.transform.gameObject;
         }
         else return null;
     }

    

    void OnGUI()
    {
        if (aimTexture != null)
        {
            //Vector3 mousePos = new Vector3(0,0,0);
            mousePos = Input.mousePosition;

            Rect pos = new Rect(mousePos.x - 25f, 
                Screen.height - mousePos.y - 25f, aimTexture.width / 3, aimTexture.height / 3);

          //  GUI.Label(pos, aimTexture);

          //  GameObject handClone = Instantiate(handMadeCursor, mousePos, Quaternion.identity) as GameObject;
            
           // handClone.transform.position = mousePos;
        }

    }
}
