﻿using UnityEngine;
using System.Collections;


public class WaitForRealSeconds : CustomYieldInstruction
 {
     private readonly float _endTime;
 
     public override bool keepWaiting
     {
         get { return _endTime > Time.realtimeSinceStartup; }
     }
 
     public WaitForRealSeconds(float seconds)
     {
         _endTime = Time.realtimeSinceStartup + seconds;
     }
 }

