﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRenderBallisticPath : MonoBehaviour 
{
    public float distanceClick;
    public float initialVelocity;
    public float extraRotValue;
    public float extraRotSpeed;
    public float timeScaleValue;
    public float currentTimeScaleValue = 0.15f;
    public float targetTimeScaleValueMax = 1.0f;
    public float targetTimeScaleValueMin = 0.02f;
    public float smoothTime = 0.3F;
    public float refTimeScaleValue = 0.0F;
    public RenderBallisticPath renderPath;
    public bool runCoRoutineUh = false;
    public bool isRender;
	public bool isPlayerRotControlled;
    public bool isCurrentlyPathRender;
	public GameObject player;
	public GameObject renderPathObj;
	public GameObject objUser;
	public GameObject objAtClickedPos;
	public LineRenderer lineDrag;

	public Transform UIPointClick;

	public Canvas myCanvas;

    public float force = 100.0f;
    public ForceMode forceMode;
    public float lineMaxLenght = 10.0f;
	public float mouseClickObjHeight = 0.0f;
    //public float depth = 10.0f;
    private Quaternion playerStartRot;

	// Use this for initialization
	void Start () 
	{
		Init ();
	}
    private void Update()
    {
        //extraRotValue += Time.deltaTime;
    }

    // Update is called once per frame
    void FixedUpdate () 
	{

		ClickLineRange ();
		//if(Input.GetMouseButton(1))
		//ChangePlayerRotation ();
		//UIMouseFollow ();
		//CopyUserRotation ();
	}

	void Init()
	{
        initialVelocity = renderPath.initialVelocity;
        playerStartRot = player.transform.rotation;

		if(objAtClickedPos != null)
		objAtClickedPos.SetActive (isRender);

		if(renderPathObj != null)
			renderPathObj.SetActive (isRender);

		if (UIPointClick != null)
			UIPointClick.gameObject.SetActive (isRender);

		if (lineDrag != null)
			lineDrag.enabled = isRender;
	}

	void ChangePlayerRotation()
	{

        //Quaternion newRotation = Quaternion.AngleAxis(90, new Vector3(1, transform.eulerAngles.y, transform.eulerAngles.z));
        //transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform., distanceClick);

        // transform.RotateAround(transform.transform.position, Vector3.left, distanceClick);

        //// player.transform.Rotate( (Input.mousePosition, Space.Self);
        ////transform.Rotate(( Vector3.right * distanceClick ));
        ////transform.Rotate(Vector3.right * Time.deltaTime);
        ////transform.rotation = Quaternion.Lerp(new Quaternion(transform.rotation.x, 0, 0, transform.rotation.w ), new Quaternion( distanceClick, 0, 0, transform.rotation.w), extraRotSpeed * Time.deltaTime);
        ////localEulerAngles.y = 0;
        ////transform.localEulerAngles.z;

        ////player.transform.rotation = new Quaternion(Mathf.Lerp(player.transform.rotation.x, 
        ////     distanceClick * extraRotValue, Time.deltaTime * extraRotSpeed), 
        ////    player.transform.rotation.y, player.transform.rotation.z, player.transform.rotation.w);//.Lerp(player.transform.rotation, to.rotation, Time.time * speed);

        ////distanceClick += Input.GetAxis("Vertical") * extraRotSpeed;
        ////player.transform.eulerAngles = new Vector3(distanceClick, player.transform.rotation.y, 0);

    }

    void ResetPlayerDefaultSettings()
    {
      if(!player.GetComponent<PlayerControl>().isGrounded)
        player.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
       
        //player.transform.rotation = new Quaternion(0, -90, 0, player.transform.rotation.w);
        player.transform.rotation = playerStartRot;

       // player.GetComponent<PlayerControl>().enabled = true;
    }

    void OnClickPlayerSettings()
    {
        player.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
       
    }

	void ClickLineRange()
	{
		Vector3 clickedMousePos = MousePointPos();

		if (Input.GetMouseButtonDown(1)) 
		{
           
			if(objAtClickedPos != null)
			{
				//Physical obj
				objAtClickedPos.SetActive(true);
				
				objAtClickedPos.transform.position = new Vector3(MousePointPos().x, mouseClickObjHeight, MousePointPos().z);
              
                //player.transform.rotation = new Quaternion(-1 * MousePointPos().z, player.transform.rotation.y, player.transform.rotation.z, player.transform.rotation.w);
			}

            if(player != null)
            {
                //player.rigidbody
            }

		} 
		else if(Input.GetMouseButtonUp(1))
		{

            if (player != null && renderPath != null)
            {
                player.GetComponent<Rigidbody>().AddForce(transform.forward * renderPath.initialVelocity, forceMode);
            }

            isCurrentlyPathRender = false;

//			if(UIPointClick != null)
//				UIPointClick.gameObject.SetActive(false);

            if (lineDrag != null)
			{
				lineDrag.enabled = false;

			}

			if(objAtClickedPos != null)
			{
				
				objAtClickedPos.SetActive(false);
			}

            //ResetPlayerDefaultSettings();

            //slow mooo back to normal
            Time.timeScale = 1;
           // timeScaleValue = Mathf.SmoothDamp(currentTimeScaleValue, targetTimeScaleValueMax, ref refTimeScaleValue, smoothTime);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            //  Time.timeScale = timeScaleValue;
            //renderPathObj.SetActive (false);
        }


		if (Input.GetMouseButton (1)) 
		{
            //OnClickPlayerSettings();
            
            isCurrentlyPathRender = true;
            distanceClick = Vector3.Distance(objAtClickedPos.transform.position, MousePointPos());
            //renderPath.initialVelocity *= distanceClick;

            //player.GetComponent<PlayerControl>().enabled = false;
            ChangePlayerRotation();
            if (lineDrag != null)
			{
				float dist = Vector3.Distance(objAtClickedPos.transform.position, MousePointPos());
                

                if (dist > lineMaxLenght)
					return;
				
				else
				{
					///set positions of lines
				
				lineDrag.enabled = true;
			
				lineDrag.SetPosition(0, objAtClickedPos.transform.position);
//
//
				lineDrag.SetPosition(1, MousePointPos());

				}
			
			}

            //Slow moooo
            Time.timeScale = 0.15f;
           // timeScaleValue = Mathf.SmoothDamp(currentTimeScaleValue, targetTimeScaleValueMin, ref refTimeScaleValue, smoothTime);
            //Time.timeScale = timeScaleValue;
            Time.fixedDeltaTime = targetTimeScaleValueMin * Time.timeScale;
        }

        //renderPath.initialVelocity = initialVelocity;
	}

	void UIMouseFollow()
	{
		if (myCanvas != null && UIPointClick != null) 
		{
			Vector2 pos;


			RectTransformUtility.ScreenPointToLocalPointInRectangle (myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);


			UIPointClick.position = myCanvas.transform.TransformPoint (pos);
		}
	}

	Vector3 MousePointPos()
	{
		// Generate a plane that intersects the transform's position with an upwards normal.
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		
		// Generate a ray from the cursor position
		Ray rayMousePos = Camera.main.ScreenPointToRay(Input.mousePosition);


		float hitdist = 0.0f;

		// If the ray is parallel to the plane, Raycast will return false.
		if (plane.Raycast (rayMousePos, out hitdist))
		{
			// Get the point along the ray that hits the calculated distance.
			targetPoint = rayMousePos.GetPoint (hitdist);

		}

		return targetPoint;
	}
	
	void AdjustPath()
	{

	}

	void CopyUserRotation()
	{
		renderPathObj.transform.rotation = objUser.transform.rotation;
	}

	private Vector3 targetPoint;

	private Vector3 mouseWorldPos;
	private Vector3 mousePos2D;
	private Vector3 clickedPos;
}
