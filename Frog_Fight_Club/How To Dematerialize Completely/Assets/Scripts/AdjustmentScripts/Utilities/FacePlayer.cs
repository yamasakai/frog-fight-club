﻿using UnityEngine;
using System.Collections;

public class FacePlayer : MonoBehaviour 
{
	private Transform myTransform;

	public bool inUpdate;
	
    public Vector3 offsetRot;

	void Awake ()
	{
		myTransform = this.transform;

        FaceCam();
	}

	void Update () 
	{
        // Maintain facing ?
        if ( inUpdate )
        {
            FaceCam();
        }
	}

    void FaceCam ()
    {
        myTransform.LookAt(myTransform.position + (Camera.main.transform.rotation * Quaternion.Euler(90f,0f,0f) * Vector3.forward));
            
        //Vector3 lookPoint = (Camera.main.transform.position - myTransform.position);
        //Quaternion lookRot = Quaternion.LookRotation(lookPoint);
        //lookRot.x = 0f;    
        //lookRot.z = 0f;

        //myTransform.rotation = lookRot * Quaternion.EulerAngles(offsetRot);
    }
}
