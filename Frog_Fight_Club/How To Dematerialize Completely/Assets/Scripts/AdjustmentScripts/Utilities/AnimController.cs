﻿using UnityEngine;
using System.Collections;

public class AnimController : MonoBehaviour 
{
	public Animator anim, animTar;
    public GameObject player, target;

	// Use this for initialization
	void Start () 
    {

        anim = GetComponent<Animator>();
		animTar = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (GetComponent<Targetting>() != null)
        {
            PlayerControl playerCont = player.GetComponent<PlayerControl>();

            Targetting targetInst = target.GetComponent<Targetting>();

            if (playerCont.jumpWalk)
            {
                anim.SetBool("Jump", true);
            }
            else
            {
                anim.SetBool("Jump", false);
            }

            if (targetInst.isLockActivated)
            {
                anim.SetBool("isLockActivated", true);
            }
            else
            {
                anim.SetBool("isLockActivated", false);
            }
        }
        
	}
}
