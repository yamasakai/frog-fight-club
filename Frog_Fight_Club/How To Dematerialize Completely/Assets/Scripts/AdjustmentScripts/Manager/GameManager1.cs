﻿using UnityEngine;
using System.Collections;

public class GameManager1 : MonoBehaviour 
{
    //private static GameManager1 _instance;
    
    public int frameRate = 60;
	public int levelCount = 10;
	public int currentLVL = 0;

    [Range (0, 5)]
    public float gameOverDelay = 1.2f;
    [Range(0, 5)]
    public float gameStartDelay = 1.2f;
    [Range(0, 5)]
    public float levelCompletedDelay = 1.2f;

    private ScreenTransitionImageEffect transitionFX;
    private GoingToNextLevel nextLevel;
    private LevelIndicator levelIndicator;
    private Camera2DFollower cam;
    private Transform playerSpawnPosition;

    #region GameManager Public Static Instance
    //public static GameManager1 instance
    //{

    //    get 
    //    { 
    //        if(_instance == null)
    //        {

    //            _instance = GameObject.FindObjectOfType<GameManager1>();

    //            DontDestroyOnLoad(_instance.gameObject);
    //        }

    //        return _instance; 
    //    }
    //}
    #endregion 

    void Awake()
    {
        #region Persistant Singleton & Targeting frameRate 
        //#if UNITY_EDITOR
        //        QualitySettings.vSyncCount = 1;  // VSync must be disabled
        //        Application.targetFrameRate = 60;
        //#endif

        //        QualitySettings.vSyncCount = 1;  // VSync must be disabled
        //        Application.targetFrameRate = 60;

        //if(_instance == null)
        //{

        //    _instance = this;
        //    DontDestroyOnLoad(_instance.gameObject);
        //}
        //else
        //{
        //    if(this != _instance)
        //    {
        //        Destroy(this.gameObject);
        //    }
        //}

        ////Just for testing stuff
        ////StartCoroutine(StartLevel());
        #endregion

    }

	// Use this for initialization
	void Start () 
    {
        CheckComponents();

        if (levelIndicator != null)
        {
            currentLVL = levelIndicator.whatIsCurrentLVL;
        }
        //StartCoroutine(ChangeFramerate());
    }
	
	// Update is called once per frame
	void Update () 
    {
        StartLevelScreenTransition();

        if (nextLevel)
        {
            //If level is complete then call the endLevelScreenTransition
            if (nextLevel.isLevelComplete)
            {
                EndLevelScreenTransition();
            }
        }
        //Making sure that we don't go over 1 and we don't go below 0
        if(transitionFX.maskValue > 1)
        {
            transitionFX.maskValue = 1;
        }
        else if (transitionFX.maskValue < 0)
        {
            transitionFX.maskValue = 0;
        }
    }

    /// <summary>
    /// Look for essential components like scene transition script and portal to next level script
    /// </summary>
    void CheckComponents()
    {
        ///We are checking if the transition script is attached or not. If it's not then we are looking for it and we attach it
        if (GameObject.FindGameObjectWithTag("MainCamera"))
        {
            //transitionFX = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScreenTransitionImageEffect>();
            transitionFX = GetComponent<ScreenTransitionImageEffect>();
            print("Initialized transitionFX (TransitionFX script)");
        }

        if (GameObject.FindGameObjectWithTag("Portal"))
        {
            nextLevel = GameObject.FindGameObjectWithTag("Portal").GetComponent<GoingToNextLevel>();
            print("Initialized nextLevel (Portal script)");
        }

        if(GameObject.FindGameObjectWithTag("MainCamera"))
        {
            cam = GetComponent<Camera2DFollower>();
            print("Initialized cam (Camera2DFollower script)");
        }

        if(GameObject.FindGameObjectWithTag("LevelIndicator"))
        {
            levelIndicator = GameObject.FindGameObjectWithTag("LevelIndicator").GetComponent<LevelIndicator>();
            print("Initialized levelIndicator (LevelIndicator)");
        }

    }


    /// <summary>
    //   Here we are looking if the TransitionMask value is higher than 0 so we can lower it to 0
    //   This is best to put at the start of a level and the end for good transition fx
    /// </summary>
    public void StartLevelScreenTransition()
    {
        if (transitionFX != null)
        {
            if (transitionFX.maskValue > 0)
            {
                transitionFX.maskValue -= 1.0f * Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// Opposite of StartLevelScreenTransition function
    /// </summary>
    public void EndLevelScreenTransition()
    {
        if (transitionFX != null)
        {
            if (transitionFX.maskValue < 1)
            {
                transitionFX.maskValue += 2.0f * Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// We are checking if the currentLevel is less than the maximum amount of level that we have 
    /// so that we know it is safe to proceed to the next level so we add +1 to the current level and load the next level.
    /// Else we are done all the levels
    /// </summary>
    public void CallNextLevel()
    {
        StartCoroutine(NextLevel());
    }

    public void CallGameOver()
    {
        StartCoroutine(GameOver());
    }

    private IEnumerator ChangeFramerate()
    {
        yield return new WaitForSeconds(1);
        Application.targetFrameRate = frameRate;
    }


    private IEnumerator NextLevel()
    {
        if (currentLVL < levelCount)
        {
            yield return new WaitForSeconds(levelCompletedDelay);
            currentLVL++;
            Application.LoadLevel(currentLVL);
        }

        else
        {

            Debug.Log("Finish all levelS....GG WP");

        }

        yield return null;
    }

    private IEnumerator StartLevel()
    {
		yield return new WaitForSeconds(gameStartDelay);
        
        print(" gameStartDelay wait completed");

    }

    private IEnumerator GameOver()
    {
        yield return new WaitForSeconds(gameOverDelay);

        Application.LoadLevel(currentLVL);
    }



    

    
}
