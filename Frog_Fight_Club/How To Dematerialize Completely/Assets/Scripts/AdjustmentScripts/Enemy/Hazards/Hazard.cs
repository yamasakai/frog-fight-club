﻿using UnityEngine;
using System.Collections;

public class Hazard : MonoBehaviour, HazardBehaviour
{
    public bool isCreateMiniMe;

    public bool isSpawnPool;

    public GameObject miniMeObj;

    [Space(12)]
    public float startForceMin;

    public float startForceMax;

    [Space(12)]
    public float rXMin;
    public float rYMin;
    public float rXMax; 
    public float rYMax;

    [Space(12)]
    public int miniMeNumbMin;

    public int miniMeNumbMax;
    

    void OnEnable()
    {

    }

    void OnDisable()
    {
        if (isCreateMiniMe)
        {
            CreateMiniMeOnKilled(miniMeObj, rXMin,
         rYMin, rXMax, rYMax, miniMeNumbMin, miniMeNumbMax);

        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

                          

    /// <summary>
    /// This function create a mini me clone of the original hazard
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="pos"></param>
    /// <param name="randXMin"></param>
    /// <param name="randYMin"></param>
    /// <param name="randXMax"></param>
    /// <param name="randYMax"></param>
    /// <param name="minNumb"></param>
    /// <param name="maxNumb"></param>
    /// <param name="numb"></param>
    public void CreateMiniMeOnKilled(GameObject obj, float randXMin,
       float randYMin, float randXMax, float randYMax, int minNumb, int maxNumb)
    {
        if (obj != null)
        {
            float randX, randY;
            randX = Random.Range(randXMin, randXMax);
            randY = Random.Range(randYMin, randYMax);

           int numb = Random.Range(minNumb, maxNumb);

            Vector2 pos;
            pos = new Vector2(randX, randY);

            for (int x = 0; x < numb; x++)
            {
                if (isSpawnPool)
                {
                    obj.Spawn(transform.position);
                }
                else
                {
                    Instantiate(obj, transform.position, Quaternion.identity);
                }

                float startForce = Random.Range(startForceMin, startForceMax);
                obj.GetComponent<Rigidbody2D>().AddForce(pos * startForce);
            }

        }
    }

}

