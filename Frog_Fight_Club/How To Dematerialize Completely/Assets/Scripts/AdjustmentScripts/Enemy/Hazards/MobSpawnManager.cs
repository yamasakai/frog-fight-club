using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* This class use a singleton presistent pattern. To make sure it is easy to use accross all the classes. 
 * 
 * 
 */
public class MobSpawnManager : MonoBehaviour 
{

    private static MobSpawnManager _mobSpawnInst;

    //private List<GameObject> listMobA;

    public List<GameObject> normalMobs;

    public List<GameObject> strongMobs;

    //public GameObject[] mobOnlyThisWave;

    //public List<GameObject> currentWave;

    //public GameObject mobA;

    public List<Transform> spawnPoints;

    public int waveLevel = 0;

    public int numbMob;

    public int numbStrongMob;

    private int allStrongMob;

    public int defaultNumbMob = 5;

    public int additionalMobPerWave = 2;

    public int numbMobAlive = 0;

    public float waveTime = 0;

    public float waveTimeIntermission = 8;

    public float timeIntervalSpawnMin = 3;

    public float timeIntervalSpawnMax = 10;

    private float timeIntervalSpawnMinBase = 3;

    private float timeIntervalSpawnMaxBase = 10;

    public bool isSpawning;

    public bool isSpecialWave;

    public bool canUseSpawnPos;

    public bool spawnOnlyThisWave;

    //If we are allow to make our pool bigger
    //public bool growPool;

    public GUIText guiText;

    //private MazeCell currentCell;

    //private MazeDirection currentDirection;

    //private Maze mazeInstance;

    private Transform pos;

    private Vector3 m_myPos;

    public GameObject obj;


    public Transform _Pos
    {
        get { return pos; }
        set { pos = value; }

    }
    void Awake()
    {
        //This is to make sure that there is always only one instance of the mobSpawnManager in the game even after unity load a new scene
        //		if(_mobSpawnInst == null)
        //		{
        //If I am the first instance, make me the Singleton
        _mobSpawnInst = this;

        //DontDestroyOnLoad(this);
        //  }

        //		else
        //		{
        //			if(_mobSpawnInst != null)
        //			{
        //				Destroy(this.gameObject);
        //			}
        //		}
    }

    // Use this for initialization
    void Start()
    {

        //		if(mazeInstance)
        //		{
        //			
        //			mazeInstance = GameObject.Find("Maze").GetComponent<Maze>();
        //			
        //		}

        //		//Normal pooling
        ////		listMobA = new List<GameObject>();
        ////
        ////		createXEnemy(numbMob);
        ////
        ////		activateEnemy();
        //		//normalMobs = new List<GameObject>();
        //		//strongMobs = new List<GameObject>();
        //		}

        StartCoroutine(WaveSystem());

    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] ListNumbMobAlive = GameObject.FindGameObjectsWithTag("Enemy");
        numbMobAlive = ListNumbMobAlive.Length;
        allStrongMob = strongMobs.Count;
        updateWave();
        updateWaveLVL();
    }

    public static MobSpawnManager mobSpawnInst
    {
        get
        {
            if (_mobSpawnInst == null)
            {
                _mobSpawnInst = GameObject.FindObjectOfType<MobSpawnManager>();
            }

            return _mobSpawnInst;
        }

    }

    /*
     * This is a very basic way to do object pooling.
     */
    //Pool function.Prepare all the enemy/Mob to be used.
    //	public void createXEnemy(int numbEnemy)
    //	{
    //		for(int i = 0; i < numbEnemy; i++)
    //		{
    //			GameObject obj = ((GameObject)Instantiate(mobA)).gameObject;
    //			obj.SetActive(false);
    //			listMobA.Add(obj);
    //		}
    //	}

    //	//Activate the enemy if the enemy is not active
    //	public void activateEnemy()
    //	{
    //
    //		GameObject obj = getEnemyPooled();
    //
    //		if(obj == null) return;
    //
    //		obj.transform.position = transform.position;
    //		obj.transform.rotation = transform.rotation;
    //		obj.SetActive(true);
    //
    //	}

    //Will return an object that is not active so available in our list.
    //	public GameObject getEnemyPooled()
    //	{
    //		for(int i = 0; i < listMobA.Count; i++)
    //		{
    //			if(!listMobA[i].activeInHierarchy)
    //			{
    //			return listMobA[i];
    //			}
    //		}
    //		//If we can make our list bigger
    //		if(growPool)
    //		{
    //			GameObject obj = ((GameObject)Instantiate(mobA)).gameObject;
    //			listMobA.Add(obj);
    //			return obj;
    //		}
    //
    //		//Not allowed to make the pool bigger and did not find any available object
    //		return null;
    //	}


    void updateWave()
    {
        StartCoroutine("WaveSystem");

    }

    void updateWaveLVL()
    {
        //	guiText.text = "Wave: " + waveLevel;
    }

    /// <summary>
    /// Sets the location. Might make this function as an interface....
    /// </summary>
    /// <param name="cell">Cell.</param>
    //public void SetLocation(MazeCell cell, Transform positionInCell)
    //{
    //    //positionInCell = new Transform();
    //    if (cell != null && positionInCell != null)
    //    {
    //        m_myPos.y = 10f;

    //        positionInCell.position = cell.transform.position + m_myPos;

    //    }
    //}

    //Will take care of the wave system. 
    IEnumerator WaveSystem()
    {
        //If there is no enemy on the field start the wave. so Wave 1. Then wait for intermission time and start spawning enemies
        if (numbMobAlive == 0)
        {


            if (waveTimeIntermission <= waveTime)
            {


                waveTime = 0;

                waveLevel++;

                //Incrementing difficulty as we progress through waves by having more mobs
                //If we made it pass wave 1 now start incrementing the number of mob at every wave.
                numbMob = (defaultNumbMob + additionalMobPerWave * (waveLevel - 1));



                //Spawning stronger mob every x waves
                if (waveLevel % 3 == 0)
                {
                    if (numbStrongMob < allStrongMob)
                    {

                        //Debug.Log("I am in the modulos ifcondition");
                        GameObject strongObj;

                        for (int xx = 0; xx < 1; xx++)
                        {
                            strongObj = strongMobs[numbStrongMob];

                            normalMobs.Add(strongObj);
                            //Debug.Log("Adding one strongMob to normal mob list");

                        }

                        numbStrongMob++;
                        //Debug.Log("A new strong mob has appeared");

                    }

                }


                //Regular Spawning
                //This is where I instanciate random monster at random spawn point
                for (int i = 0; i < numbMob; i++)
                {
                    //Will choose random mobs in the array.
                    obj = normalMobs[Random.Range(0, normalMobs.Count)];
                    //Will choose random spawn point
                    pos = spawnPoints[Random.Range(0, spawnPoints.Count)];

                    //					if(mazeInstance)
                    //					{
                    //						SetLocation(mazeInstance.GetCell(mazeInstance.RandomCoordinates), pos);
                    //					}

                    //Create a randomly picked mob in the array and spawn it at a random spawn point
                    obj = ((GameObject)Instantiate(obj, pos.position, pos.rotation)).gameObject;


                    //Debug.Log("Number of Mob Alive: " + numbMobAlive);
                    //How long to wait until another enemy is instanciated.
                    yield return new WaitForSeconds(Random.Range(timeIntervalSpawnMin, timeIntervalSpawnMax));

                }



            }
            else
            {
                waveTime += Time.deltaTime;
            }
        }
    }

}





