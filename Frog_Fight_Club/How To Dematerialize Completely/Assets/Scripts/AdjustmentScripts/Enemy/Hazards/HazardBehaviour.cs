﻿using UnityEngine;
using System.Collections;

public interface HazardBehaviour  
{

    void CreateMiniMeOnKilled(GameObject obj, float randXMin,
       float randYMin, float randXMax, float randYMax, int minNumb, int maxNumb);

}
