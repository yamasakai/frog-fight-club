﻿using UnityEngine;
using System.Collections;

public class ShootingDelayed : MonoBehaviour
{
    // public float speed;

   // public float bulletSpeed;

    [HideInInspector]
    public float shootingRate;
    [Space(5)]
    [Header("Shooting Rate")]
    public float minShootingRate;
    public float maxShootingRate;

    [HideInInspector]
    public float firstShootTime;
    [Space(5)]
    [Header("First Time Shooting")]
    public float minFirstShootTime;
    public float maxFirstShootTime;

    [Space(10)]
    [Header("Bullet Type")]
    public GameObject[] bullets;

    [HideInInspector]
    public GameObject randBullet;

    [Space(5)]
    [Header("Transfoms")]
    public Transform shootPoint;
    [Space(5)]
    public Transform target;

    [Space(10)]
    [Header("Juice")]
    public JuiceOnInteraction juice;
    public float juiceTimer = 0.15f;

    private GameObject player;

    float attackTimer = 0;

    private Transform myTransform;




    #region Current Trash
    // Use this for initialization
    void Start()
    {

        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        //InvokeRepeating("Shoot", Random.Range(3.666f,firstShootTime), Random.Range(startShootingDelay,shootingDelay));
        shootingRate = Random.Range(minShootingRate, maxShootingRate);
        firstShootTime = Random.Range(minFirstShootTime,maxFirstShootTime);
        InvokeRepeating("Shoot", firstShootTime, shootingRate);
    }

    // Update is called once per frame
    void Update()
    {
        //AirEnemyLogic();
    }

    #region SpaceInvaderMovement
    //Goes left and right between 1 and 10 on the x axis.
    //After arriving to one of the edge it goes down one unit in the y axis.
    //public void AirEnemyLogic()
    //{
    //    float downer = 0.5f;
    //    Vector3 newPos = transform.position;

    //    //newPos.y -= downer;


    //    //IF enemy x position is less than player negative position move in a positive direction (right)
    //    if (transform.position.x <= -15)
    //    {

    //        moveDirection.x = 1;
    //        transform.position = newPos;
    //    }

    //    //(left)
    //    else if (transform.position.x >= 15)
    //    {

    //        moveDirection.x = -1;
    //        transform.position = newPos;

    //    }

    //    transform.Translate(new Vector3(moveDirection.x * speed * Time.deltaTime, 0, 0));


    //}
    #endregion

    IEnumerator ScaleOnOff()
    {
        juice.OnImpact();
        yield return StartCoroutine(Auto.Wait(juiceTimer));
        juice.OffImpact();
    }

    void Shoot()
    {

        if (player != null)
        {

            //Debug.Log("Shooting");

            #region Look at shit
            //shootPoint.transform.LookAt(player.transform.position);
            #endregion

            randBullet = bullets[Random.Range(0, bullets.Length)];
            randBullet.Spawn(shootPoint.transform.position);
            StartCoroutine(ScaleOnOff());

            #region trash 
            //GameObject projectile = Instantiate(bullet, shootPoint.transform.position , shootPoint.transform.rotation) as GameObject;
           // bullet.GetComponent<Rigidbody2D>().AddForce(bullet.transform.forward * bulletSpeed);
            //GetComponent<AudioSource>().clip = sfxBeam;
            //GetComponent<AudioSource>().volume = 0.5f;
            //GetComponent<AudioSource>().Play();

            //Destroy(projectile, 3f);
            #endregion

        }
    }
}

    
#endregion

#region bullshit
    //void Shoot()
    //{
    //    // rotate the projectile to aim the target:
    //    shootPoint.LookAt(target);

    //    bullet.Spawn(shootPoint.transform.position);
    //    // distance moved since last frame:
    //    float amtToMove = ProjectileSpeed * Time.deltaTime;
    //    // translate projectile in its forward direction:
    //    bullet.transform.Translate(target.position - shootPoint.transform.position * amtToMove);

    //    Debug.Log("Shooting");

    //}

    //IEnumerator DelayShoot()
    //{
    //    canShoot = false;
    //    yield return new WaitForSeconds(waitAmt);

    //    canShoot = true;
    //}
#endregion