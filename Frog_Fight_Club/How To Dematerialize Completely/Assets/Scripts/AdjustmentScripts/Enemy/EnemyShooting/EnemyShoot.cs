﻿using UnityEngine;
using System.Collections;

public class EnemyShoot : MonoBehaviour {

	private Transform target;

	public float speed = 0;
	public GameObject impactFX;
	//#TODO SFX...

	void Awake()
	{
		//target = GameObject.FindGameObjectWithTag("Player").transform;
	
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

//		Vector2 dir = target.transform.position - transform.position;
//		float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
//		transform.rotation = Quaternion.AngleAxis(angle, Vector2.right);
//
//		rigidbody2D.AddForce(transform.up * speed );
//
//		Destroy(this.gameObject, 5.0f);

	}

	void OnTriggerEnter2D(Collider2D col)
	{
	
		if(col.tag == "Player" )
		{

			//Debug.Log ("Shi Ne");
			col.GetComponent<EntityA>().PlayerGetDamaged(10f);
			Destroy(this.gameObject);
			GameObject fx = ((GameObject)Instantiate(impactFX, transform.position, Quaternion.identity)).gameObject;
			Destroy(fx, 0.3f);

		}


		
	}
	
}
