﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour 
{
    public Transform target;
    public float ProjectileSpeed = 20;

    private Transform myTransform;
    private Vector3 toTarget;

    void Awake()
    {
        myTransform = transform;
    }

    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            GameObject go = GameObject.FindGameObjectWithTag("Player");
            target = go.transform;
            // rotate the projectile to aim the target:
            //myTransform.LookAt(target);
             toTarget = target.transform.position - transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // distance moved since last frame:
        float amtToMove = ProjectileSpeed * Time.deltaTime;
        // translate projectile in its forward direction:
        myTransform.Translate(toTarget * amtToMove);
    }


}

