﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropSpotManager : MonoBehaviour 
{
    //
    public bool isSpawnPool;
    public bool activateDrops;
    public bool isHazardChild;
    public bool createHazardSpots;
    public bool isRandomHazardObjPicked;
    
    //
    public List <GameObject> hazardsObj;
    public List<Transform> dropSpots;

    //
    public int dropNb;
    public int spotNb;
    public float timeIntervalSpawnMin;
    public float timeIntervalSpawnMax;
    public float distanceBetweenDropSpots;

	// Use this for initialization
	void Start () 
    {
        InvokeRepeating("DropHazard", PickRandomFloat(timeIntervalSpawnMin, timeIntervalSpawnMax), PickRandomFloat(timeIntervalSpawnMin, timeIntervalSpawnMax));
	}
	
	// Update is called once per frame
	void Update () 
    {
        //StartCoroutine(DropHazard());
	}

    /// <summary>
    /// Will return a random number based on the index passed as argument
    /// </summary>
    /// <param name="minRange"></param>
    /// <param name="maxRange"></param>
    /// <returns>return a random number between minRange & maxRange</returns>
    int PickRandomInt(int minRange, int maxRange)
    {
        int rand = Random.Range(minRange, maxRange);

        return rand;
    }

    float PickRandomFloat(float minRange, float maxRange)
    {
        float rand = Random.Range(minRange, maxRange);

        return rand;
    }

    List <Transform> CreateSpots(int nb, Vector2 startPos, float distanceBetween)
    {
        Transform spot = null;
        List<Transform> spotList = new List<Transform>();

        for (int x = 0; x < nb; x++)
        {
            spot = (Transform)Instantiate(spot, new Vector2(startPos.x + distanceBetween, startPos.y), Quaternion.identity);
            spotList.Add(spot);
        }
       
        return spotList;
    }

    Transform ChooseSpots()
    {
        Transform spot;
               
        spot = dropSpots[PickRandomInt(0, dropSpots.Count)];
       
        return spot;
    }

    void DropHazard()
    {
        GameObject obj;
   
        obj = hazardsObj[PickRandomInt(0, hazardsObj.Count)];
            
        //obj = (GameObject)Instantiate(obj, ChooseSpots().position, ChooseSpots().rotation);

            
        if (isSpawnPool)    
        {
                
            obj.Spawn(transform.position);
            
        }
            
        else    
        {    
            obj = (GameObject)Instantiate(obj, ChooseSpots().position, ChooseSpots().rotation);   
        }

            if (isHazardChild)
            {
                this.transform.DetachChildren();
            }    
    }

}
