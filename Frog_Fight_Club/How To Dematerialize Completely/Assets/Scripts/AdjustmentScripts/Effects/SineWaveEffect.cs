﻿using UnityEngine;
using System.Collections;

public class SineWaveEffect : MonoBehaviour 
{
    public float speed;
    public float maxSpeed;
    public float phase;
    public Vector3 offset;

    private float timer;
    private float sineWave;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {

        sineWave = Mathf.Sin(timer) + phase;
       
        timer += Time.deltaTime * speed;
        if (timer > Mathf.PI * 2)
        {
            timer -= Mathf.PI * 2;
        }
        
        
        transform.position = new Vector3(transform.position.x + offset.x, maxSpeed * sineWave + offset.y, transform.position.z + offset.z);
	}
}
