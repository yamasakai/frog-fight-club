﻿using UnityEngine;
using System.Collections;

public class FlowerScript : MonoBehaviour 
{
	[HideInInspector] public bool dead;

	//private AudioSource myAudioSource;

	private Transform myTransform, myTop;
	private LineRenderer myLineRenderer;
	private Renderer myTopRenderer;
	
	private Vector3 topHideScale, topMinScale, topMaxScale, topCurScale;
	private float topScaleSpd;

	private int touchCounter, touchDuration;

	private float widthBase, widthTop, widthFactorMin, widthFactorMax, widthFactorCur, widthFactorSpd;

	private Vector3 topNormalPos, topTargetPos, topCurPos;
	private float topPosSpd;

	public bool small;
    
    //
    public GameObject playerTarget;

	// Circular shit
	private float angle = 0f;
	private float speed = (2f * Mathf.PI) / .5f; //2 * PI in degress is 360, so you get 5 seconds to complete a circle
	private float radius = .5f; 

	private Vector3 circOffset;
	private float circDir;

	// Sin shit
	public float amplitude = .5f;
	public float omega = 2f;
	
	private float index, sinOffset;

	private float sinFactorMin, sinFactorMax, sinFactorCur, sinFactorAcc;

	private float height;

	// Colors
	//public Color[] colors;
	public Color flowerBaseCol;	
	private Color flowerTipCol;
	public Color flowerBaseVoidCol;

	private float lineTargetW, lineCurW, lineSpdW;
	private int showCounter, showDuration;

	public bool inVoid;

	public LayerMask solidLayerMask;

	void Awake ()
	{
		Vector3 fromPoint = transform.position + new Vector3(0f,10f,0f);
		Vector3 endPoint = fromPoint + new Vector3(0f,-20f,0f);
		Vector3 toDir = (endPoint - fromPoint);
		
		RaycastHit hit;
		if ( Physics.Linecast(fromPoint,endPoint,out hit,solidLayerMask) )
		{
			string hitTag = hit.transform.tag;
			
			if ( hitTag == "ground" )
				inVoid = false;
			
			if ( hitTag == "void" )
				inVoid = true;
		}
		else
		{
			inVoid = false;
		}
	}

	void Start () 
	{
        //myAudioSource = gameObject.AddComponent<AudioSource>();
        //myAudioSource.spatialBlend = 1f;

		myTransform = this.transform;
		myTop = myTransform.Find("top").transform;
		myLineRenderer = GetComponent<LineRenderer>();
		myTopRenderer = myTop.GetComponent<Renderer>();

		//Color startCol =  colors[Random.Range(0,colors.Length)];
		//float colOff = 0f;
		//myTopRenderer.material.color = new Color(startCol.r + colOff,startCol.g + colOff,startCol.b + colOff,1f);

		touchDuration = 20;
		touchCounter = touchDuration;

		// Position & scaling
		float topScale = 0f;

		switch ( small )
		{
			case false:
				height = Random.Range(.5f,1.75f);
				topScale = Random.Range(.3f,.45f);
				lineTargetW = .15f;
			break;
			case true:
				height = Random.Range(.4f,.75f);
				topScale = Random.Range(.2f,.3f);
				lineTargetW = .06f;
			break;
		}

		lineCurW = lineTargetW;
		lineSpdW = .75f;

		widthBase = lineCurW;
		widthTop = lineCurW;

		topHideScale = Vector3.zero;
		topMinScale = new Vector3(topScale,topScale,topScale) * .7f;
		topMaxScale = topMinScale * .7f;
		topCurScale = topMinScale;
		topScaleSpd = .25f;

		myTop.localPosition = new Vector3(0f,height,0f);
		topNormalPos = myTop.localPosition;
		topTargetPos = topNormalPos;
		topCurPos = topNormalPos;
		topPosSpd = .1f;

		widthFactorMax = 1f;
		widthFactorMin = 0f;
		widthFactorCur = widthFactorMax;
		widthFactorSpd = .05f;

		if ( inVoid )
		{
			flowerBaseCol = flowerBaseVoidCol;

			myTransform.Find("top").gameObject.SetActive(false);
		}

		// Color
		float colTipOff = Random.Range(.15f,.2f);
		flowerTipCol = new Color(flowerBaseCol.r + colTipOff,flowerBaseCol.g + colTipOff,flowerBaseCol.b + colTipOff,flowerBaseCol.a);

		myLineRenderer.SetColors(flowerBaseCol,flowerTipCol);
		myLineRenderer.SetWidth(widthBase,widthTop);

		// Sin stuff
		sinFactorMin = 0f;
		sinFactorMax = 1f;
		sinFactorCur = sinFactorMax;
		sinFactorAcc = .1f;
	}

	void Update () 
	{
		if ( !dead )
		{
			if ( inVoid )
			{
				Vector3 fromPoint = transform.position + new Vector3(0f,10f,0f);
				Vector3 endPoint = fromPoint + new Vector3(0f,-20f,0f);
				Vector3 toDir = (endPoint - fromPoint);
				
				RaycastHit hit;
				if ( Physics.Linecast(fromPoint,endPoint,out hit,solidLayerMask) )
				{
					string hitTag = hit.transform.tag;
					
					if ( hitTag == "ground" )
						dead = true;
				}
			}

			if ( (Vector3.Distance(transform.position,playerTarget.transform.position) <= 40f))//|| (GameManager.gameStartCounter < GameManager.gameStartDuration) )
			{
				myTop.localPosition = topCurPos + circOffset;
				myTop.localScale = topCurScale + new Vector3(Mathf.Abs(sinOffset),Mathf.Abs(sinOffset * .5f),Mathf.Abs(sinOffset)) * sinFactorCur;

				if ( touchCounter < touchDuration )
				{
					touchCounter ++;

					topCurScale += (topMaxScale - topCurScale) * (topScaleSpd * 1.1f);
					topCurPos += (topTargetPos - topCurPos) * (topPosSpd * 2f);
					sinFactorCur += (sinFactorMax - sinFactorCur) * (sinFactorAcc * 1f);
				}
				else
				{
					topCurScale += (topMinScale - topCurScale) * topScaleSpd;
					topCurPos += (topNormalPos - topCurPos) * topPosSpd;
					sinFactorCur += (sinFactorMin - sinFactorCur) * (sinFactorAcc * .3f);
				}

				// LineRenderer
				UpdateLineRenderer();

				// Circular motion shit
				angle += (speed * Time.deltaTime) * circDir; 
				circOffset = new Vector3(Mathf.Cos(angle) * radius * sinFactorCur,0f,Mathf.Sin(angle) * radius * sinFactorCur);

				// Sin shit
				index += Time.deltaTime;
				sinOffset = amplitude * Mathf.Cos(omega * index);
			}
		}
		else
		{
			topCurScale += (Vector3.zero - topCurScale) * topScaleSpd;

			widthFactorCur += (widthFactorMin - widthFactorCur) * widthFactorSpd;
			myLineRenderer.SetWidth(widthBase * widthFactorCur,widthTop * widthFactorCur);
				
			myTop.localPosition = topCurPos + circOffset;
			myTop.localScale = topCurScale + new Vector3(Mathf.Abs(sinOffset),Mathf.Abs(sinOffset * .5f),Mathf.Abs(sinOffset)) * sinFactorCur;

			if ( widthFactorCur <= .01f )
				Destroy(gameObject);
		}

		// Disable audio if not playing something
        //if ( myAudioSource.enabled )
        //{
        //    if ( !myAudioSource.isPlaying )
        //        myAudioSource.enabled = false;
        //}
	}

	void GetTouched ()
	{
		touchCounter = 0;

		// Circular motion stuff
		circDir = ( Random.value > .5f ) ? 1f : -1f;

        //// Audio
        //if ( !myAudioSource.isPlaying )
        //    AudioManager.PlayPop(myAudioSource,.2f,.3f,.7f,1f);
	}

	void UpdateLineRenderer ()
	{
		myLineRenderer.SetPosition(1,myTop.localPosition);

		if ( !dead )
			myLineRenderer.SetWidth(lineCurW,lineCurW);
	}

	void OnTriggerEnter ( Collider hit )
	{
		float yOffset = 0f;
		float spdFactor = 0f;

		yOffset = height + Random.Range(-.4f,0f);

		if ( !hit.transform.CompareTag("water") && !hit.transform.CompareTag("seed") )
		{
			if ( hit.transform.parent != null && hit.transform.parent.CompareTag("Actor") )
			{
				PlayerControl hitScript = hit.transform.parent.GetComponent<PlayerControl>();

				spdFactor = 1f;

				float dZ = .01f;
				if ( Mathf.Abs(hitScript.motion.x) > dZ || Mathf.Abs(hitScript.motion.z) > dZ )
				{
					topTargetPos = new Vector3(hitScript.motion.x,yOffset,hitScript.motion.z) * spdFactor;

					GetTouched();
				}
			}
		}
	}
}
