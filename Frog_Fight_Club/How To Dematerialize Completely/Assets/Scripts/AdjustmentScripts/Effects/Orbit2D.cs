﻿using UnityEngine;
using System.Collections;

public class Orbit2D : MonoBehaviour 
{
    public Transform targetOrbit;
    public float speed;
    public float OrbitDegrees;
  
    public Vector3 desiredPosition;
    public float radius = 2.0f;
    public float radiusSpeed = 0.5f;
    public float rotationSpeed = 80.0f;


	// Use this for initialization
	void Start () 
    {

        if (targetOrbit != null)
        {
            transform.position = (transform.position - targetOrbit.position).normalized * radius + targetOrbit.position;
          
        }
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (targetOrbit != null)
        {
            OrbitAround(targetOrbit);
        }
        else 
        {
            this.gameObject.GetComponent<ToPortalPoint>().enabled = true;
            this.gameObject.GetComponent<Orbit2D>().enabled = false;
        }
	}

    

    public void OrbitAround(Transform target)
    {
        if (target != null)
        {
            //this will find the direction angle of the target
            // Transform.InverseTransformDirection() will make the angle relative to the game object rather than a world direction:
            //Vector3 dir = target.transform.position - transform.position;
            //dir = target.transform.InverseTransformDirection(dir);
            //float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

            
            
            Vector3 point = new Vector3(target.position.x, target.position.y, 0);
            transform.RotateAround(target.position, Vector3.forward, speed * Time.deltaTime);

            desiredPosition = (transform.position - targetOrbit.position).normalized * radius + targetOrbit.position;
            transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * radiusSpeed);
        
        }
       

    }
}
