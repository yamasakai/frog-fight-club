﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour 
{

    [HideInInspector]
	public float duration;
    [HideInInspector]
	public float magnitude;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    IEnumerator Shake(float d, float m) 
    {
        duration = d;
        magnitude = m;
		
		float elapsed = 0.0f;
		
		Vector3 originalCamPos = Camera.main.transform.position;

		
		while (elapsed < duration) 
        {	
			elapsed += Time.deltaTime;          
			
			float percentComplete = elapsed / duration;         
			float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);
			
			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= magnitude * damper;
			y *= magnitude * damper;
			
			transform.position = new Vector3(x, transform.position.y, transform.position.z);
			
			yield return null;
		}

	}

    public void Shaker(float d, float m)
    {
        StartCoroutine(Shake(d, m));
    }

    //public void Shaker()
    //{
    //    StartCoroutine("Shake");
    //}
}
