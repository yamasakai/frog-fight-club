﻿using UnityEngine;
using System.Collections;
using Flask;

public class ReportImpact : MonoBehaviour 
{
    public bool isTouched = false;
    [HideInInspector]
    public bool isNotColliding = true;
    public GameObject targetImpacted;
    public JuiceOnInteraction juice;

    public float generalTimerOn = 0;
   //public float generalTimerOff = 0;
   // public bool isTimerReach;
   public GameObject onWallImpactFX;
   public GameObject destroyFX;
   public GameObject peerImpactFX;
    

    
	// Use this for initialization
	void Start () 
    {
    
	}
	
	// Update is called once per frame
	void Update () 
    {
        StartCoroutine(OnOffImpactTimer());
	}

    IEnumerator OnOffImpactTimer()
    {
            
        if (isTouched == true)
        {
            #region old scale ON contact using auto.cs
            //Scale high on impact
            //yield return StartCoroutine(targetImpacted.transform.ScaleTo    
            //    (juice.scaleToVecTargetOn, juice.scaleToDurationOn,
            //        juice.scaleToEaseTypeOn));
            #endregion

            //When touching something scale up
            //iTweenExtensions.ScaleTo(targetImpacted, new Vector3(1.5f, 1.5f, 1), generalTimerOn, 0, EaseType.spring);
            juice.OnImpact();

                #region old timer
                ////for (float delay = generalTimer; delay >= 0; delay -= Time.deltaTime)
                ////   yield return 0;
                #endregion

            //Wait x amount of time and put the object back to his initial scale
            ////Wait based on generalTimerOn As long as it bigger than 0 
            //Also after the timer is elapsed it will reset the isTouched bool to false so we can reset to our 
            //object to his initial scale 
            if (generalTimerOn > 0)    
                yield return StartCoroutine(Auto.Wait(generalTimerOn));    
            isTouched = false;

            #region old scale OFF contact using auto.cs
            //yield return StartCoroutine(targetImpacted.transform.ScaleTo      
            //    (juice.scaleToVecTargetOff, juice.scaleToDurationOff,      
            //    juice.scaleToEaseTypeOff));
            #endregion

            //When not touching something scale down to our initial scale
            //iTweenExtensions.ScaleTo(targetImpacted, new Vector3(1f, 1f, 1), generalTimerOn, 0, EaseType.spring);
            juice.OffImpact();     
            
        }
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject && isTouched == false)
        {
            isTouched = true;
        }

        

        switch (c.gameObject.tag)
        {
		case "PlayerFist":
                
                if (onWallImpactFX != null)
                {
                    onWallImpactFX.Spawn(transform.position);
                }

                if (this.gameObject.name == "CircleBouncingOff(Collider)")
                {
                    CameraShakeManager.Instance.BigCameraShake();
                }
                else if (this.gameObject.name == "CircleBouncingOffSmall(Collider)")
                {
                    CameraShakeManager.Instance.MediumCameraShake();
                }
                else 
                {
                    CameraShakeManager.Instance.SmallCameraShake();
                }

                //CameraShakeManager.Instance.MediumCameraShake();
                break;

            case "PlayerBullet":

                if (destroyFX != null)
                {
                    destroyFX.Spawn(transform.position);
                }
                
                this.gameObject.SetActive(false);
            
               
                break;

            case "BouncingHazard":

                if (peerImpactFX != null)
                {
                    peerImpactFX.Spawn(transform.position);
                }

                CameraShakeManager.Instance.MediumCameraShake();

                break;

            default:

                break;
        }
      
    }
}
