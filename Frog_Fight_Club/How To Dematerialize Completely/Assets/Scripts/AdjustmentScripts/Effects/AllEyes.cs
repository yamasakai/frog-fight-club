﻿using UnityEngine;
using System.Collections;

public class AllEyes : MonoBehaviour 
{
    public Transform target;

    public Transform eye;

    public Transform pupil;

    public float eyeRadius;

    public float pupilRadius;

    public float smoothing;

	// Use this for initialization
	void Start () 
    {
        //if (FindObjectOfType<PlayerTwoD>())
        //{
        //    target = FindObjectOfType<PlayerTwoD>().transform;
        //}
	}
	
	// Update is called once per frame
    void Update()
    {
        if (!target)
        {
            //if (GameObject.FindGameObjectWithTag("Player"))
            //{
            //    target = GameObject.FindGameObjectWithTag("Player").transform;

            //    return;
            //}
        }
            
        LookAtTarget();
    }

    /// <summary>
    /// The Pupil will look at the target and follow him with the radius of the eye
    /// </summary>
    void LookAtTarget()
    {
        if (target != null)
        {
            Vector3 distanceToTargetFromEye = (target.transform.position - eye.transform.position).normalized;

            //This will help keep the pupil within the eye radius.
            distanceToTargetFromEye = Vector3.ClampMagnitude(distanceToTargetFromEye, eyeRadius - pupilRadius);

            //This is just so that the pupil can move along the eye position while looking at the target.
            Vector3 finalPupilPosition = eye.transform.position + distanceToTargetFromEye;

            pupil.transform.position = Vector3.Lerp(pupil.transform.position, finalPupilPosition, smoothing * Time.deltaTime);
        }

    }

    
}
