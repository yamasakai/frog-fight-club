﻿using UnityEngine;
using System.Collections;

public class RotationEffect : MonoBehaviour
{
	public float RotationSpeed = 1F;
	public float MaxRotation = 15F;
	public float phase = 0F;
	
	private float timer = 0F;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		float waveslice = 0F;
		phase = Mathf.Clamp(phase, -1F, 1F);
		waveslice = Mathf.Sin(timer) + phase;
		timer = timer + (RotationSpeed * Time.deltaTime);
		if (timer > Mathf.PI * 2)
		{
			timer -= Mathf.PI*2;
		}

		transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, MaxRotation * waveslice));

		//Debug.Log(waveslice);
	}
}
