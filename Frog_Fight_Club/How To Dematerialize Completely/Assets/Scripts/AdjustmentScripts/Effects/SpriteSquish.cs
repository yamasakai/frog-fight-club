﻿//using UnityEngine;
//using System.Collections;
//
//
//{
//  
//  // If you recall this tracks how much of the blend between _from and _to we're going to use.     
//  // If we update it slowly every frame the scale will slowly move from (_from vector) to (_to vector).    
//  private float _counter = 0f;     
//  [SerializeField] private Vector2 _from,_to; 
//  
//  // this is the speed at which the transition will take place.    
//  [SerializeField] private float _time = 8f;     
//  
//  public void OnTriggerEnter2D(Collider2D other) 
//  {         
//    // Here we invoke the lerp method. Now the LerpLocalScale method will run on every delta time update.        
//    InvokeRepeating ("LerpLocalScale", 0, Time.deltaTime);    
//  }
//  
//  void LerpLocalScale() 
//  {         
//    // if our lerp isn't finished. keep lerping         
//    if (_counter < 1) {            
//      // we add to the counter the delta time and then offset that by the speed with which we want to tween.             
//      _counter += Time.deltaTime * _time;             
//    
//      // This is it. Here the lerp tweens the local scale             
//      transform.localScale = Vector2.Lerp (_from, _to, _counter);        
//    } else {             
//      // here the lerp is finished we reset the counter and the scale and then cancel updating this method.            
//      _counter = 0f;            
//      transform.localScale = new Vector2 (1, 1);             
//      CancelInvoke ("LerpLocalScale");        
//    }
//  }
//}
//
