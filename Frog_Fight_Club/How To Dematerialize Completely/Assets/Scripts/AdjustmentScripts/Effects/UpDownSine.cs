﻿using UnityEngine;
using System.Collections;

public class UpDownSine : MonoBehaviour 
{
	private Transform target;
	public float speed;    
	public float amplitudeY;
	public float frequency = 3;
	public float offSetSin;
	Vector3 StartPos;

	// Use this for initialization
	void Start () 
	{
		StartPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 offset = new Vector3(transform.position.x, StartPos.y + Mathf.Sin( speed * (frequency * Time.time)) * amplitudeY, transform.position.z);

		transform.position = offset;
	}


}
