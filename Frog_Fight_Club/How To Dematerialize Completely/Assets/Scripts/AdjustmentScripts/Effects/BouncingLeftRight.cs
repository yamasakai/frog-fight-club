﻿using UnityEngine;
using System.Collections;

public class BouncingLeftRight : MonoBehaviour
{

    //[Range (-300, 300)]
    //public float amplitudeX = 10.0f;
    
    [Range(0, 15)]
    public  float amplitudeY = 5.0f;
    
    [Range(-15, 15)]
    public float omegaX = 1.0f;
    
    [Range(0, 15)]
    public float omegaY = 5.0f;
     
    //[Range(-300, 300)]
    //public float offset;
    
     float originalX;

   
    float index;

    void Start()
    {
        this.originalX = this.transform.position.x;
    }

    public void Update()
    {
        index += Time.deltaTime;
        //float x = amplitudeX * Mathf.Cos(omegaX * index);
        float y = Mathf.Abs(amplitudeY * Mathf.Sin(omegaY * index));

        //If you muliply index * omegaX(strenght on the x) inside of the bracket your movement will be more restricted. 
        //But if you multiply by omegaX after(outside the bracket) you have more freedom 
        //on where to go and can easily limit the strength and range of where the object will land
        transform.localPosition = new Vector3(originalX + Mathf.Cos(index) * omegaX, y, 0);
      
    }

#region simpleBouncingLeftRIght from -x|0|x
     //index += Time.deltaTime;
     //float x = amplitudeX*Mathf.Cos (omegaX*index);
     //float y = Mathf.Abs (amplitudeY*Mathf.Sin (omegaY*index));
    //transform.localPosition= new Vector3(x,y,0);
#endregion

}