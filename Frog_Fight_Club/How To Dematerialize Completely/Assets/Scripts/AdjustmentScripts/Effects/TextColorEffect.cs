﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextColorEffect : MonoBehaviour
{
	public Color colorA;
	public Color colorB;
	public Text text;
	public float ColorCycleSpeed = 1F;

	private float timer = 0F;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		float waveslice = 0F;
		waveslice = Mathf.Sin(timer);
		timer = timer + (ColorCycleSpeed * Time.deltaTime);
		if (timer > Mathf.PI * 2)
		{
			timer -= Mathf.PI * 2;
		}

		// normalize to between 0 and 1
		waveslice = (waveslice / 2) + 0.5F;

		text.color = Color.Lerp(colorA, colorB, waveslice);
	}
}
