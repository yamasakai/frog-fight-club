﻿using UnityEngine;
using System.Collections;

public class DrawingManager : MonoBehaviour 
{
    public GameObject drawObj;
    private GameObject drawPref;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            CreateDrawingObj();
        }
	}

    void CreateDrawingObj()
    {
        if (drawObj != null)
        {
            drawPref = (Instantiate(drawObj, Vector3.zero, Quaternion.identity) as GameObject);
        }
    }
}
