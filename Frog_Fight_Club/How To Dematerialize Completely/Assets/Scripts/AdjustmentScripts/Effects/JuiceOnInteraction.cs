﻿using UnityEngine;
using System.Collections;

public class JuiceOnInteraction : MonoBehaviour 
{

    //[Space(3)]
    //public bool isScaleTo;

    [Space(3)]
    [Header ("ON")]
    public EaseType scaleToEaseTypeOn = EaseType.spring;
    public Vector3 scaleToVecTargetOn = Vector3.one;
    [Tooltip ("Best to be similar to generalTimer from ReportImpact class")]
    public float scaleToDurationOn = 1.5f;
    public float scaleToDelayOn = 0;
    [Space(5)]
    [Header("OFF")]
    public EaseType scaleToEaseTypeOff = EaseType.spring;
    public Vector3 scaleToVecTargetOff = Vector3.one;
    [Tooltip("Best to be similar to generalTimer from ReportImpact class")]
    public float scaleToDurationOff = 1.5f;
    public float scaleToDelayOff = 0;

    #region ol scale shit
    //[Space(10)]
    //[Header("ScaleFrom")]
    
    //[Header("ON")]
    //public EaseType scaleFromEaseTypeOn = EaseType.ElasticOut;
    //public Vector3 scaleFromVecTargetOn = Vector3.one;
    //public float scaleFromDurationOn = 1.5f;
    //[Space(5)]
    //[Header("OFF")]
    //public EaseType scaleFromEaseTypeOff = EaseType.ElasticOut;
    //public Vector3 scaleFromVecTargetOff = Vector3.one;
    //public float scaleFromDurationOff = 1.5f;

    //[Space(10)]
    //public GameObject onImpactFX;
    #endregion


    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region instan impactFX
    //public void FXOnImpact()
    //{
    //    if (onImpactFX != null)
    //    {
    //        //Spaw the fx using our pool
    //        //onImpactFX.Spawn(transform.position, Quaternion.identity);
    //        //Instantiate(onImpactFX, transform.position, Quaternion.identity);
    //    }
    //}
    #endregion

    public void OnImpact()
    {
        //Debug.Log("Turn On...");

        iTweenExtensions.ScaleTo(this.gameObject, scaleToVecTargetOn, scaleToDurationOn, scaleToDelayOn, scaleToEaseTypeOn);
        
    }


    public void OffImpact()
    {
        //Debug.Log("Turn Off...");

        iTweenExtensions.ScaleTo(this.gameObject, scaleToVecTargetOff, scaleToDurationOff, scaleToDelayOff, scaleToEaseTypeOff);
        
    }
}
