﻿using UnityEngine;
using System.Collections;

public class TowerObject : MonoBehaviour 
{

	public GameObject obj;
	//public Transform spawn;
	// Use this for initialization
	void Start () {
	
		int colums = 30; 
		int rows = 20;
		float radius = -25;

		for(int x = 0; x < colums; x++)
		{
		for(int i = 0; i < rows; i++)
		{
				GameObject temp = (GameObject)Instantiate(obj, new Vector3(0f, i, radius)  , Quaternion.identity); //new Vector3(0f, i, radius)
			temp.transform.parent = transform;
		
		}
		transform.Rotate(0f, 360/colums, 0f);
	}

	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
