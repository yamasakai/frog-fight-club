﻿using UnityEngine;
using System.Collections;

public class ScalePingPong : MonoBehaviour {

    [Range(0, 15)]
    public float scaleX, scaleY, scaleZ;

    [Range(0, 15)]
    float originalScaleX, originalScaleY, originalScaleZ;

    [Range(0, 15)]
    public float amplitudeY = 5.0f;

    [Range(-15, 15)]
    public float omegaX = 1.0f;

    [Range(0, 15)]
    public float omegaY = 5.0f;

    float index;

	// Use this for initialization
	void Start () 
    {
        originalScaleX = transform.localScale.x;
        originalScaleY = transform.localScale.y;
        originalScaleZ = transform.localScale.z;

	}
	
	// Update is called once per frame
	void Update () 
    {
        index += Time.deltaTime;
        float y = Mathf.Abs(amplitudeY * Mathf.Sin(omegaY * index));

        transform.localScale = new Vector3(originalScaleX + Mathf.Cos(index) * omegaX, y , 1 );
	
	}
}
