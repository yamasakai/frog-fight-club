﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DynamicScale : MonoBehaviour
{
	Vector3 lastPosition;

	void Start()
	{
		lastPosition = transform.position;
	}

	/// <summary>
	/// Lates the update.
	/// volume of a cube; V=h×w×L
	/// you know h=w though and assume volume is 1, so
	/// 1=w×w×L
	///	1=w²×L
	///	1/L=w²
	///	√(1/L)=w
	/// </summary>
	/// <returns>The update.</returns>
	void LateUpdate()
	{
		Vector3 delta = transform.position - lastPosition;
		transform.rotation = Quaternion.LookRotation(delta + Vector3.forward * 0.001f);
		float l = 1f + delta.magnitude;
		float wh = Mathf.Sqrt(1f / l);
		transform.localScale = new Vector3(wh, wh, l);

		lastPosition = transform.position;
	}

}