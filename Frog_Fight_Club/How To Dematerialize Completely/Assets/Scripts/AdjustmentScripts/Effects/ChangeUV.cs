﻿using UnityEngine;
using System.Collections;

public class ChangeUV : MonoBehaviour {

	Renderer uv;
	public float speed = 10;
	// Use this for initialization
	void Start () {
		uv = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		uv.GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (Mathf.Cos (Time.time) * speed, Mathf.Sin (-Time.time));
	}
}
