﻿using UnityEngine;
using System.Collections;

public class BouncySinCos : MonoBehaviour 
{
	public float Width, Height;
	public float Speed;
	public float BaseSize;
	public float BaseHeight;
	float height;
	float Offset = .5f;

	[Header ("Rotation")]
	public bool CanTurn;
	public float Angle;
	public float RotationSpeed;
	float RotOffset;

	// Use this for initialization
	private void Start()
	{
		Offset = Random.Range(1f, 10f);
		height = transform.position.y;

		RotOffset = Random.Range(0f,2f);
	}
	// Update is called once per frame
	void Update()
	{

		float Xsize = Mathf.Sin(Time.realtimeSinceStartup * Speed+ Offset) * Width + BaseSize;
		float Ysize = Mathf.Cos(Time.realtimeSinceStartup * Speed + .5f+ Offset) * Height + BaseSize;

		float yHeight = Mathf.Sin(Time.realtimeSinceStartup * Speed + Offset * 1.2f) * BaseHeight + height;

		transform.localScale = new Vector3(Xsize, Ysize, 1);
		Vector3 Pos = transform.position;
		Pos.y = yHeight;
		transform.position = Pos;

		if (CanTurn)
		{
			float ZAngle = Mathf.Sin(RotationSpeed * (Time.time + RotOffset)) * Angle;
			transform.localEulerAngles = new Vector3(0, 0, ZAngle);
		}

	}
}
