﻿using UnityEngine;
using System.Collections;

public class FreezeFrameOnHit : MonoBehaviour 
{
    [Range (0, 1)]
    public float timeFreeze = .004f;
    

    
    void OnDisable()
    {
        //just in case
        //WakeUp();
    }

    public void FreezingFrame()
    {
        Time.timeScale = .001f;

        #region old stuff 
        //float pauseEndTime = Time.realtimeSinceStartup + delay;

        //while (Time.realtimeSinceStartup < pauseEndTime)
        //{
        //    yield return null;
        //}
        
        ////yield return new WaitForSeconds(delay * Time.timeScale);

        ////Time.timeScale = 1;
        #endregion
    }

    public void WakeUp()
    {
        Time.timeScale = 1.0f;

        //Recycle this pooled bullet instance            
        //gameObject.Recycle();
    }

    private IEnumerator DelayCoroutine()
    {
        FreezingFrame();

        float waitTime = Time.realtimeSinceStartup + timeFreeze;
        yield return new WaitWhile(() => Time.realtimeSinceStartup < waitTime);

        WakeUp();
    }


    void OnCollisionEnter(Collision col)
    {
        StartCoroutine(DelayCoroutine());
    }

}
