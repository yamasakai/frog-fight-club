﻿using UnityEngine;
using System.Collections;

public class ToPortalPoint : MonoBehaviour 
{
    private Transform target;
    public float speed;    
    public float amplitudeY;


   

	// Use this for initialization
	void Start () 
    {

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Portal").transform;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (target != null)
        {
            ToDestination(target);


            if (transform.position == target.position)
            {
                print("Destination reached");
            }
        }
	}

    public void ToDestination(Transform dest)
    {
        
        Vector3 offset = new Vector3(0,  Mathf.Sin(amplitudeY * Time.time), 0);

        transform.position = Vector3.Lerp(transform.position, dest.position + offset, Time.deltaTime * speed);
    }
}
