﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class Drawing : MonoBehaviour 
{
    public LineRenderer lineRender;
    //public Transform paintOrigin;
    private Vector3 worldPos;
    private int numberOfPoints = 0;
    private bool didReleaseMouse = false;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {

        if (!didReleaseMouse)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                numberOfPoints++;
                //iNCRrement the number of point to the lineRenderer based on our click
                lineRender.SetVertexCount(numberOfPoints);

                Vector3 mousePos = new Vector3(0, 0, 0);
                mousePos = Input.mousePosition;
                //This is the z where the player & camera is. 
                mousePos.z = 20f;
                //Position based on the main camera
                worldPos = Camera.main.ScreenToWorldPoint(mousePos);

                Ray ray = new Ray(worldPos, worldPos);
                Debug.DrawRay(worldPos, worldPos);

                //RaycastHit hit;

                lineRender.SetPosition(numberOfPoints - 1, worldPos);


            }
            else if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                didReleaseMouse = true;
                GetComponent<Drawing>().enabled = false;
            }
            
            if (Input.GetKey(KeyCode.Backspace))
            {
                numberOfPoints = 0;
                lineRender.SetVertexCount(0);

            }
        }
	}


}
