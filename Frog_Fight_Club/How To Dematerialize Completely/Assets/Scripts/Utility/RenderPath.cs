﻿using UnityEngine;
using System.Collections;

public class RenderPath : MonoBehaviour 
{
    public float initialVelocity;
    public float timeResolution = 0.02f;
    public float maxTime = 10.0f;

    private LineRenderer lineRenderer;

	// Use this for initialization
	void Start () 
    {

         lineRenderer = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 velocityVector = transform.forward * initialVelocity;
        lineRenderer.SetVertexCount((int)( maxTime/timeResolution));
        int index = 0;
        Vector3 currentPos = transform.position;
        
        for (float x = 0.0f; x < maxTime; x += timeResolution)
        {

            lineRenderer.SetPosition(index, currentPos);
            currentPos += velocityVector * timeResolution;
            velocityVector += Physics.gravity * timeResolution;

            index++;
        }
	}


}
