﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public  class Helper : MonoBehaviour 
{


	/// <summary>
	/// Gets the child game object.
	/// </summary>
	/// <returns>The child game object.</returns>
	/// <param name="fromGameObject">From game object.</param>
	/// <param name="withName">With name.</param>
	static public GameObject getChildGameObject(GameObject fromGameObject, string withName)
	{
		
		Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(true); //<-the true will include inactive objects // bool includeInactive = false
		foreach (Transform t in ts) if (t.gameObject.name == withName) return t.gameObject;
		return null;
	}	

	/// <summary>
	/// Gets the child game object.
	/// </summary>
	/// <returns>The child game object.</returns>
	/// <param name="fromGameObject">From game object.</param>
	/// <param name="withName">With name.</param>
	static public void turnOffAllChildren(GameObject fromGameObject)
	{
		//if(fromGameObject.childCount > 0)
		//{
		
		//Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(true); //<-the true will include inactive objects // bool includeInactive = false
		//foreach (Transform t in ts)  t.gameObject.SetActive(false);
	    
	 //   }
		
	}	

	/// <summary>
	/// Gets the child colors. Make it so that you have the option to run this without those tags 
	/// </summary>
	/// <returns>The child colors.</returns>
	/// <param name="fromGameObject">From game object.</param>
	static public List <Color> getChildColors(GameObject fromGameObject)
	{
		//Make an array of child transform
		Transform[] rend = fromGameObject.transform.GetComponentsInChildren<Transform> ();//new Renderer[fromGameObject.transform.childCount];
		List<Color> cols = new List <Color>();

		//iterate through the list of child transform
		foreach(Transform t in rend)//for (int i = 0; i < rend.Length; i++) 
		{
			if(t.GetComponent<Renderer>() != null && t.gameObject.tag == "EnemyBody" || t.gameObject.tag == "Enemy")
			cols.Add (t.GetComponent<Renderer>().material.color);
		}


		return cols;

	}


	/// <summary>
	/// Gets the child colors. Make it so that you have the option to run this without those tags 
	/// </summary>
	/// <returns>The child colors.</returns>
	/// <param name="fromGameObject">From game object.</param>
	static public List <Renderer> getChildRenderer(GameObject fromGameObject)
	{
		//Make an array of child transform
		Transform[] tran = fromGameObject.transform.GetComponentsInChildren<Transform> ();//new Renderer[fromGameObject.transform.childCount];
		List<Renderer> rends = new List <Renderer>();
		
		//iterate through the list of child transform
		foreach(Transform t in tran)//for (int i = 0; i < rend.Length; i++) 
		{
			if(t.GetComponent<Renderer>() != null && t.gameObject.tag == "EnemyBody" || t.gameObject.tag == "Enemy")
				rends.Add (t.GetComponent<Renderer>());
		}
		
		
		return rends;
		
	}


	/// <summary>
	/// Changes the object cols to a single colors. Like flash on damage etc..
	/// </summary>
	/// <returns>The object cols.</returns>
	/// <param name="colList">Col list.</param>
	static public void changeListObjsColorsFlash(List<Renderer> colList, Color colorChange)
	{

		foreach(Renderer rend in colList)
		{
			
			rend.material.SetColor("_Color", colorChange);

			
		}
	}

	/// <summary>
	/// Changes the children to orignal cols.
	/// </summary>
	/// <returns>The children to orignal cols.</returns>
	static public void changeChildrenToOrignalCols(Renderer[] currentRendererList, Color[] originalColorList)
	{
		for( int i = 0; i < currentRendererList.Length; i++)
		{
			
			currentRendererList[i].GetComponent<Renderer>().material.SetColor("_Color", originalColorList[i]);
			
		}
	}

    /// <summary>
    /// MoveToPosition with a set time with no easing
    /// </summary>
    /// <param name="transform"></param>
    /// <param name="position"></param>
    /// <param name="timeToMove"></param>
    /// <returns></returns>
    public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
    {

        var currentPos = transform.position;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            transform.position = Vector3.Lerp(currentPos, position, t);
            yield return null;
        }
    }


    /// <summary>
    /// This function will lerp the transform of this game object to an end position at a certain time
    /// </summary>
    /// <param name="thisTransform"></param>
    /// <param name="startPos"></param>
    /// <param name="endPos"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    static public IEnumerator EaseLerpAToB(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
	{

		yield return null;
		float i = 0.0f;
		float rate = 1.0f / time;
		while (i < 1.0f)
		{
			i += Time.deltaTime * rate;
			//The way the smoothStep is set up allow us to have a ease in and ease out for a smoother lerp
			thisTransform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, i)));
			
			yield return null;
		}
	}
	
	
	
	
}
