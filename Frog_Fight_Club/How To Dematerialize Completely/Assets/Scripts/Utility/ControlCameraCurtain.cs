﻿using UnityEngine;
using System.Collections;

public class ControlCameraCurtain : MonoBehaviour 
{
	public GameObject curtain;
	public Targetting targetScript;
	//public Animator animTop, animBot;

	// Use this for initialization
	void Start () 
	{
		//anim = GetComponent<Animator>();

		curtain = GameObject.FindWithTag ("Curtain");


	}
	
	// Update is called once per frame
	void Update () 
	{

        if (targetScript != null)
        {
            if (targetScript.selectedTarget != null)
            {
                curtain.GetComponent<Animator>().SetBool("CurtainActive", true);
            }
            else
            {
                curtain.GetComponent<Animator>().SetBool("CurtainActive", false);
            }

        }



	}
}
