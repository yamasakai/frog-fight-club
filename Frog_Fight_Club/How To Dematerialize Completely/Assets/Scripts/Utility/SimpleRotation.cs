﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotation : MonoBehaviour 
{
	[Range(0, 9000)]
	public float rotSpeedX;
	[Range(0, 9000)]
	public float rotSpeedY;
	[Range(0, 9000)]
	public float rotSpeedZ;



	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		RotateInfinitely ();
	}

	void RotateInfinitely()
	{
		stepY = rotSpeedY * Time.fixedDeltaTime;
		stepX = rotSpeedX * Time.fixedDeltaTime;
		stepZ = rotSpeedZ * Time.fixedDeltaTime;

		transform.Rotate (stepX,stepY,stepZ); //rotates 50 degrees(step) per second around y axis
	}

	private float stepX;
	private float stepY;
	private float stepZ;
}
