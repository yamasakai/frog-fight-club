﻿using UnityEngine;
using System.Collections;

public class TestingFibonacci : MonoBehaviour 
{
    public int f = 0;
    public int g = 1;

	// Use this for initialization
	void Start () 
    {
	
	}

    /// <summary>
    /// The Fibonacci numbers are generated by setting F0=0, F1=1, and then using the recursive formula

        //Fn = Fn-1 + Fn-2

        //to get the rest. Thus the sequence begins: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ... 
        //This sequence of Fibonacci numbers arises all over mathematics and also in nature.
        //However, if I wanted the 100th term of this sequence, 
        //it would take lots of intermediate calculations with the recursive formula to get a result. Is there an easier way?

        //Yes, there is an exact formula for the n-th term! It is: //an = [ Phin - (phi)n ]/Sqrt[5].

        //where Phi=(1+Sqrt[5])/2 is the so-called golden mean, and phi=(1-Sqrt[5])/2 is an associated golden number, 
        //also equal to (-1/Phi). This formula is attributed to Binet in 1843, though known by Euler before him.
    /// </summary>
    public void FibonacciTest()
    {
        //f start at 0
        //g start at 1
        
        int x = 0;
        //for (int i = 0; i < 10; i++)
        while(x < 10)
        {
            x = x + 1;
            Debug.Log("f: " + f);

            f = f + g;
            g = f - g;
        }
    }

	// Update is called once per frame
	void Update () 
    {
        FibonacciTest();
        
	}
}
