﻿using UnityEngine;
using System.Collections;

public class CustomImageEffect : MonoBehaviour 
{
	public Material EffectMaterial;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

		

		void OnRenderImage(RenderTexture src, RenderTexture dst)
		{
			if (EffectMaterial != null)
				Graphics.Blit(src, dst, EffectMaterial);
		}


}
