﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveLineFromAtoB : MonoBehaviour 
{
	public LineRenderer objLineRend;
	public Transform objDestLinePoint;
	Vector3[] objLineRendPoints;
	public float objLineRendLenght = 1.2f;
	public float lineSmoothness = 3f;

	// Use this for initialization
	void Start () 
	{

		objLineRendPoints = new Vector3[3];
		objLineRend.useWorldSpace = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		SetUpLine();
	}

	void SetUpLine()
	{
		objLineRendPoints[0] = objLineRend.transform.position;
		
		
		objLineRendPoints[2] = objDestLinePoint.transform.position;
		
		
		float TotalLegLenght = (objLineRendPoints[0] - objLineRendPoints[2]).magnitude;
		float kneediffrence = Mathf.Clamp((-1 * TotalLegLenght) + objLineRendLenght, 0, 2.5f) * -1;  //bending of the knee in corralation of the leglenght
		
		
		Vector3 v = objLineRendPoints[0] - objLineRendPoints[2];
		Vector3 InBetween = (objLineRendPoints[0] + objLineRendPoints[2]) / 2f;
		Vector3 normal1 = Vector3.Normalize(Vector3.Cross(v, Vector3.forward)) * kneediffrence + InBetween; // calc normal for knee  base position
		
		
		objLineRendPoints[1] = normal1;
		Vector3[] SmoothPoints = Functions.MakeSmoothCurve(objLineRendPoints, 3);
		objLineRend.positionCount = SmoothPoints.Length;
		objLineRend.SetPositions(SmoothPoints);
	}
}
