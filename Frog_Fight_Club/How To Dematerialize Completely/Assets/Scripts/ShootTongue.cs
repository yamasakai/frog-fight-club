﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTongue : MonoBehaviour
{
    public DetectTagOnTrigger detectTagOnTrigger;
    public GameObject tongueBall;
    public Transform startPoint;
    public Vector3 endPoint;
    public Transform player;
    public bool didTongueTouchEnemy = false;

    public bool canShootTongue;
    public bool didTongueLanded;
    public LineRendererDrag lineRendererDrag;
    public float secondsForOneLength = 2f;

    public EaseType moveEase;
   
    public float moveDuration;
   
    public float delay;
  
 

    // Use this for initialization
    void Start ()
    {
		
	}

    // Update is called once per frame
    void Update()
    {
       

        StartCoroutine(ShootingTongue(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), moveDuration, delay, moveEase));

        detectTagOnTrigger.didTouchTag = didTongueTouchEnemy;

        if(detectTagOnTrigger.didTouchTag)
        {
            tongueBall.transform.position = detectTagOnTrigger.enemyTransform.position;
        }

        #region ooo
        //endPoint = lineRendererDrag.MousePointPos();

        //  RunShootTongue(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), moveDuration, delay, moveEase);
        #endregion

        #region ooold crap
        //if (lineRendererDrag.isRelease)
        //{

        //    tongueBall.transform.position = Vector3.Lerp(startPoint.position, new Vector3(endPoint.x, startPoint.position.y, endPoint.z), 2f);//Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.time / secondsForOneLength, 1f)));
        //}
        // if (!lineRendererDrag.isRelease)
        //{
        //    //tongueBall.transform.position = startPoint.position;

        //    tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, 2f);
        //}
        //else //if(lineRendererDrag.isRelease)
        //{
        //    //tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, 2f);

        //    tongueBall.transform.position = startPoint.position;
        //}
        #endregion

    }

    public void ReturnTongueToStartPos(Vector3 finalDestination, float durationMovement, float delay, EaseType easingType)
    {

        iTweenExtensions.MoveTo(tongueBall, finalDestination, durationMovement, delay, easingType);
    }

    public IEnumerator ShootingTongue(Vector3 finalDestination, float durationMovement, float delay, EaseType easingType)
    {
      

        if (lineRendererDrag.isRelease)
        {
            endPoint = lineRendererDrag.MousePointPos();

            iTweenExtensions.MoveTo(tongueBall, finalDestination, durationMovement, delay, easingType);

            //new Vector3(endPoint.x, startPoint.position.y, endPoint.z), moveDuration, 0, moveEase);

            #region ooo
            //StartCoroutine(tongueBall.transform.MoveTo(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), moveDuration, moveEase));

            //tongueBall.transform.position = Vector3.Lerp(tongueBall.transform.position, new Vector3(endPoint.x, startPoint.position.y, endPoint.z), Time.deltaTime * .5f);//Mathf.SmoothStep(0f, 1f, Mathf.PingPong(Time.time / secondsForOneLength, 1f)));

            //yield return new WaitForSeconds(.1f);

            //tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, Time.deltaTime * .5f);

            //yield return new WaitForSeconds(1.3f);

            // tongueBall.transform.position = startPoint.position;
            #endregion

            yield return null;

        }

        #region oooo
        //if (!lineRendererDrag.isRelease)
        //{
        //    tongueBall.transform.position = startPoint.position;

        //    tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, 2f);
        //}
        //else //if(lineRendererDrag.isRelease)
        //{
        //    tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, 2f);

        //    //tongueBall.transform.position = startPoint.position;
        //}
        //else if (!lineRendererDrag.isRelease)
        //{
        //    //tongueBall.transform.position = startPoint.position;

        //    tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, 2f);
        //}
        //else //if(lineRendererDrag.isRelease)
        //{
        //    //tongueBall.transform.position = Vector3.Lerp(new Vector3(endPoint.x, startPoint.position.y, endPoint.z), startPoint.position, 2f);

        //    tongueBall.transform.position = startPoint.position;
        //}
        #endregion

        yield return null;
    }

}
