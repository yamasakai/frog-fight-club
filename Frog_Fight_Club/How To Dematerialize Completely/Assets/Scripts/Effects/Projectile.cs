﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	public GameObject target;
	public float height;
	public float MoveSpeed = 5.0f;
	public float waveTimer = 0.0f;
	public float frequency = 20.0f;  // Speed of sine movement
	public float magnitude = 0.5f;   // Size of sine movement
	public float timingOffSet = 0.0f;
	public float adjustSin = 3.0f;
	public float RotationSpeed = 0.0f;

	private Vector3 axis;

	private Vector3 pos;

	Vector3 lookTargetPos;
	Quaternion rotateToTarget;
	private float offset = 0;
	Vector3 toDest = Vector3.zero;
	float zPos = 0;
	public float omegaX = 1.0f;
	public float amplitudeX = 10.0f;
	float index;
	private Vector3 orthogonal;

	void Start () 
	{
		pos = transform.localPosition;
		//DestroyObject(gameObject, 1.0f);
		axis = transform.right;  // May or may not be the axis you want

	}

	void Update () 
	{
		lookTargetPos = (target.transform.localPosition - transform.localPosition).normalized;;
		lookTargetPos.y = 0;
//		rotateToTarget = Quaternion.LookRotation(lookTargetPos);
//		transform.localRotation = Quaternion.Slerp(transform.localRotation, rotateToTarget, 1);

		//Transforming world coord to local
		//Vector3 localForward = transform.worldToLocalMatrix.MultiplyVector(transform.forward);
		//Vector3 localUp = transform.worldToLocalMatrix.MultiplyVector(transform.up);

		//transform.Rotate( new Vector3(0, target.transform.localPosition - transform.localPosition, 0), Space.Self );

		orthogonal = new Vector3 (-lookTargetPos.z, 0, lookTargetPos.x);

		Quaternion rot = Quaternion.LookRotation(lookTargetPos, transform.up);      
		transform.rotation = Quaternion.RotateTowards(
			transform.localRotation, 
			rot, 
			RotationSpeed * Time.deltaTime);
		//		transform.rotation = Quaternion.LookRotation(lookTarget);
		index += Time.deltaTime;
		float x = amplitudeX*Mathf.Cos (omegaX*index);

		zPos -= Time.deltaTime / waveTimer;
		offset = Mathf.Sin(Time.time * MoveSpeed + timingOffSet) *  adjustSin;//Mathf.Sin (Time.time * frequency) * magnitude;
		pos += transform.forward * Time.deltaTime * MoveSpeed;//(target.transform.position - transform.position) * Time.deltaTime * MoveSpeed;
		toDest = new Vector3 (offset , height, zPos);




		 //* Time.deltaTime * MoveSpeed;//transform.forward * Time.deltaTime * MoveSpeed;
		transform.localPosition = pos + toDest + orthogonal ;//pos + axis * Mathf.Sin (Time.time * frequency) * magnitude;
	}

//	float offset = 0;
//	public GameObject[]                shipSpawnPoints;
//
//	Vector3 startPosition;
//
//	void Awake()
//	{    
//		shipSpawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
//		startPosition = shipSpawnPoints[Random.Range(3,7)].transform.position;
//	}
//
//	void Update() 
//	{
//		transform.rotation = Quaternion.LookRotation(GetComponent<Rigidbody>().velocity);
//		transform.GetComponent<Rigidbody>().AddForce(xVect, yVect, zVect);
//
//		if(isFanShip)
//		{
//			zPos -= Time.deltaTime / waveTimer;
//			 offset = Mathf.Sin(Time.time * directionSpeed + timingOffset) * .3f;
//			transform.position = startPosition + new Vector3(offset,0, zPos);
//		}
//	}
}
