﻿using UnityEngine;
using System.Collections;

public class SinWaveGroup : MonoBehaviour 
{
	public int numberOfDots = 3;
	public GameObject[] waveDots;
	public GameObject waveDotPrefab;
	//scales the wave vertically
	public float amplitude;
	//scales the wave horizontally 
	public float waveLength;
	//shifts the wave
	public float phase;



	void Start()
	{

		waveDots = new GameObject[numberOfDots];

		for (int i = 1; i < numberOfDots; i++) 
		{
			waveDots [i] = Instantiate (waveDotPrefab, new Vector3 (0, 0, i), Quaternion.identity) as GameObject;
		}
	}

	void Update() 
	{
		for (int i = 1; i < numberOfDots; i++) 
		{
			float normalizedProgress = (float)i / (numberOfDots - 1);
			float x =  Time.deltaTime * waveLength + phase;
			Vector3 position = (Vector3.forward * i) + Vector3.up * Mathf.Sin(x) * amplitude ;
			waveDots [i].transform.position = position;//new Vector3(0, position.y, 0);
		}
	}
}
