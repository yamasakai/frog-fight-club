﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyNormal : ByTheTale.StateMachine.MachineBehaviour 
{
	public GameObject target;
	public Rigidbody rigidBody { get; protected set; }
	public CapsuleCollider capsuleCollider { get; protected set; }

	public float TimeToIdle { get { return timeToIdle; } }
	public float TimeToWander { get { return timeToWander; } }
	public float TimeToAttack { get { return timeToAttack; } set { TimeToAttack = TimeToAttack; } }
	public float MovementSpeed { get { return movementSpeed; }  set{ MovementSpeed = movementSpeed; } }
	public float MovementSpeedDamp { get { return movementSpeedDamp; } }
	public float AttackRange { get { return attackRange; } }
	public float PreAttackDelay { get { return preAttackDelay; } }
	public float AttackSpeed { get { return attackSpeed; }}
	public float AttackDist { get { return attackDist; } }


	[HideInInspector]public bool stopMovement;
//	public bool isSineChase;
//	public float amplitude;
//	public float frequency;
//	public float offSetSinPhase = 0.0f;
//	public float startTime;
//	public Vector3 orthogonal;
//	public Vector3 directionSin;
	[HideInInspector] public  Vector3 fixDir;
	[HideInInspector] public  Vector3 direction;
	[HideInInspector] public  Vector3 currentTargetPoint;
	[HideInInspector] public  Vector3 oldTargetPoint;


	public override void AddStates()
	{
		AddState<ChaseTargetState>();
		AddState<PreAttackState>();
		AddState<SimpleChargeAttackState> ();
		
		SetInitialState<ChaseTargetState>();
	}
	

	public void Awake()
	{
		rigidBody = GetComponent<Rigidbody>();
		capsuleCollider = GetComponent<CapsuleCollider>();

	}

	
	public override void LateUpdate()
	{
		//base.LateUpdate();
	}

	[SerializeField] protected float timeToIdle = 1.337F;
	[SerializeField] protected float timeToWander = 3.37F;
	[SerializeField] protected float timeToAttack = 1.0F;
	[Range(0, 100)][SerializeField] protected float attackRange = 10.0F;
	[Range(0, 100)][SerializeField] protected float preAttackDelay = 2.0F;
	[Range(0, 100)][SerializeField] protected float attackSpeed = 1.5F;
	[Range(0, 100)][SerializeField] protected float attackDist = 5.5F;
	[Range(0, 100)] protected float movementSpeedDamp = 0.5F;
	[Range(0, 100)][SerializeField] protected float movementSpeed = 7.33F;


	
}
