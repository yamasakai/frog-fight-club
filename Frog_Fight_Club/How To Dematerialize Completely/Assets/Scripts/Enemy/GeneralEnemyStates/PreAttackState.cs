﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreAttackState : ByTheTale.StateMachine.State 
{
	public EnemyFlyNormal enemyFlyNormal { get { return (EnemyFlyNormal)machine; } }
	public float timeInState { get; protected set; }
	
	public override void Enter()
	{
		base.Enter();
		
		timeInState = 0;
	}

	public override void Execute()
	{
		
		base.Execute();
		
		PreAttackDelay ();
		
		
	}

	public void PreAttackDelay()
	{
		//Count the time before going into the attack state
		timeInState += Time.deltaTime;

		//Do whatever you like before attacking, like stoping in his track before doing a charge attack
		enemyFlyNormal.stopMovement = true;

//		if (enemyFlyNormal.isSineChase) 
//		{
//			enemyFlyNormal.rigidBody.velocity = Vector3.zero;
//		}

		//Show visual indication that enemy will attack
		if(enemyFlyNormal.PreAttackDelay <= timeInState) 
		{
		
			//Attack
			machine.ChangeState <SimpleChargeAttackState>();

		}


	}

	public void PreAttackColorFlash()
	{


	}

}
