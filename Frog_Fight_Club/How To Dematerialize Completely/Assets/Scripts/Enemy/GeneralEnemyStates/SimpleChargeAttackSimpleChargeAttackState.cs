using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleChargeAttackState : ByTheTale.StateMachine.State 
{
	public EnemyFlyNormal enemyFlyNormal { get { return (EnemyFlyNormal)machine; } }
	public float timeInState { get; protected set; }
	public bool didAttack {get; protected set; }

	public override void Enter()
	{
		base.Enter();
		
		timeInState = 0;

		didAttack = false;
	}
	
	public override void Execute()
	{
		
		base.Execute();
		

		timeInState += Time.deltaTime;



//		if (timeInState >= enemyFlyNormal.TimeToAttack) 
//		{

			AttackCharge ();
	//	} 

		if ( Vector3.Distance(enemyFlyNormal.transform.position, enemyFlyNormal.currentTargetPoint) <= 1)
		{

			//enemyFlyNormal.oldTargetPoint = enemyFlyNormal.currentTargetPoint;
			//enemyFlyNormal.currentTargetPoint = enemyFlyNormal.oldTargetPoint;
			machine.ChangeState<ChaseTargetState> ();
		}
	}

	public void AttackCharge()
	{

		machine.GetState<ChaseTargetState> ().LookAtTarget ();

		// Get a direction vector from us to the target
		// Normalize it so that it's a unit direction vector
		Vector3 targetDir = (enemyFlyNormal.target.transform.position - enemyFlyNormal.transform.position);
		
	
		// Move enemy in that direction
//		enemyFlyNormal.transform.position = Vector3.MoveTowards(enemyFlyNormal.transform.position,
//		                                                        enemyFlyNormal.currentTargetPoint, 
//		                                                        enemyFlyNormal.AttackSpeed * Time.deltaTime);
		iTweenExtensions.MoveTo (enemyFlyNormal.gameObject,
		                         enemyFlyNormal.currentTargetPoint, 
		                        enemyFlyNormal.TimeToAttack,
		                        0,
		                        EaseType.easeOutBack);


	}

}
