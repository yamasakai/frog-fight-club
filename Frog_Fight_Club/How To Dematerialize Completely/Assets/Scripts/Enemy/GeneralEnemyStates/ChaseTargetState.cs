﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseTargetState : ByTheTale.StateMachine.State 
{
	public EnemyFlyNormal enemyFlyNormal { get { return (EnemyFlyNormal)machine; } }




	public override void Enter()
	{
		base.Enter();
		
		enemyFlyNormal.stopMovement = false;
	}
	
	public override void Execute()
	{

		base.Execute();

		if (enemyFlyNormal.stopMovement == false) 
		{
			ChaseTarget ();

		}
	}

	public void InitTarget()
	{

		if (!enemyFlyNormal.target) 
		{
		
			enemyFlyNormal.target = GameObject.FindGameObjectWithTag("Player");
		}
	}

	public void LookAtTarget()
	{

		InitTarget ();

		if (enemyFlyNormal.target) 
		{

			enemyFlyNormal.fixDir = new Vector3 (enemyFlyNormal.target.transform.position.x, 
		                                         this.enemyFlyNormal.transform.position.y, 
			                                     enemyFlyNormal.target.transform.position.z);

			enemyFlyNormal.transform.LookAt (enemyFlyNormal.fixDir);
		}

	
	}

	public void ChaseTarget()
	{

		LookAtTarget ();

//		if (enemyFlyNormal.isSineChase)
//		{
//			float t = Time.time - enemyFlyNormal.startTime;
//			enemyFlyNormal.directionSin = (enemyFlyNormal.target.transform.position - enemyFlyNormal.transform.position).normalized;
//			enemyFlyNormal.direction.y = 0;
//			enemyFlyNormal.orthogonal = new Vector3 (-enemyFlyNormal.directionSin.z, 0, enemyFlyNormal.directionSin.x); //inversing dir 
//
//			
//			enemyFlyNormal.rigidBody.velocity = enemyFlyNormal.directionSin * enemyFlyNormal.MovementSpeed + 
//				enemyFlyNormal.orthogonal * enemyFlyNormal.amplitude * Mathf.Sin (enemyFlyNormal.frequency * t + enemyFlyNormal.offSetSinPhase);
//		} 
//		else 
//		{
			enemyFlyNormal.transform.position = Vector3.Lerp (enemyFlyNormal.transform.position, 
			                                                  enemyFlyNormal.target.transform.position, 
			                                                  enemyFlyNormal.MovementSpeed * Time.deltaTime);
//		}

		//When within attack range change to pre-attack state
		if (Vector3.Distance (enemyFlyNormal.transform.position, enemyFlyNormal.target.transform.position) <= enemyFlyNormal.AttackRange) 
		{
			enemyFlyNormal.currentTargetPoint = enemyFlyNormal.target.transform.position;
			machine.ChangeState<PreAttackState>();
		}



	}




}
