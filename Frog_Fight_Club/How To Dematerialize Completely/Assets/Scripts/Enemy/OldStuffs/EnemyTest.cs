﻿using UnityEngine;
using System.Collections;

public class EnemyTest : MonoBehaviour 
{
	private StateMachine stateMachine = new StateMachine();
	[SerializeField]
	private LayerMask targetLayer;
	[SerializeField]
	private float viewRange;
	[SerializeField]
	private string targetTag;
	[SerializeField]
	private float movementSpeed;

	private GameObject actor;

	// Use this for initialization
	void Start () 
	{
	
		this.stateMachine.ChangeState (new SearchForX (this.targetLayer, 
		                                               this.gameObject, this.viewRange, this.targetTag));
	}
	
	// Update is called once per frame
	void Update () 
	{
	
		this.stateMachine.ExecuteStateUpdate();
	}
}
