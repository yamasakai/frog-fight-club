﻿using UnityEngine;
using System.Collections;

public class IfFistReached : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider otherObject)
	{
		if (otherObject.gameObject.tag == "PlayerFist"  )
		{
			if(this.gameObject.tag == "Crosshair")
			{
			otherObject.gameObject.transform.GetComponent<PlayerPunching>().didChargedFistReachedDest = true;
		
		//	print("Reached Crosshair");
			}

			else if ( this.gameObject.tag == "FistPoint")
			{
				otherObject.gameObject.transform.GetComponent<PlayerPunching>().didChargedFistReachedInit = true;
			//	print("Reached Fist Rest Point");
			}
		}
	}
	
}
