﻿using UnityEngine;
using System.Collections;

public class FollowObj : MonoBehaviour 
{
    //
    public Transform target;

    //
    //public EaseType easeType;
    
    //
    public float speed;
    public float amplitudeY;
    //public float waitXBeforeChase;
   // public float XToCatch;
    
    //
    public bool chase;
    public bool chasePlayer;

	// Use this for initialization
	void Start () 
    {
        Init();
        StartCoroutine("FollowTarget");
	}

    void Init()
    {
        if (chasePlayer)
        {
            if (target == null)
            {
                if (GameObject.FindGameObjectWithTag("Player").transform)
                {
                    target = GameObject.FindGameObjectWithTag("Player").transform;
                }
            }
        }
    }

    void FollowTarget()
    {
        if (target != null)
        {
            Vector3 offset = new Vector3(0, Mathf.Sin(amplitudeY * Time.time), 0);

            transform.position = Vector3.Lerp(transform.position, target.position + offset, Time.deltaTime * speed);
        }

    }

	// Update is called once per frame
	void Update () 
    {
        FollowTarget();
	}
}
