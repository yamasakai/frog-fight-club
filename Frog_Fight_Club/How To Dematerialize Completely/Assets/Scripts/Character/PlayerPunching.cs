using UnityEngine;
using System.Collections;

public class PlayerPunching : MonoBehaviour 
{
    public float depth = 10.00f;
    public float dist = 15.00f;
    public float timeRate = 1.00f;
   // public float punchDelay = 0.0f;
    public float returnSpeed = 10.0f;
	public float chargeSpeed = 5.0f;
	public float chargeTime = 0;
	public float chargeTimeMax = 2.0f;
	public float chargeLimit = 5f;

	float t = 0;
	float timeCounter;

	public float heightCirc = 2;
	public float widthCirc = 2;
	public float speedCirc = 5;

    public KeyCode keyCode;
    public bool isFixReturn = false;
	public bool canPunch = true;
    public bool didPunch = false;
    public bool punchSwitchLR = false;
	public bool limitRange = true;
	public bool m_chargeOccured;
	public bool m_isCharging;
	public bool didChargedFistReachedDest;
	public bool didChargedFistReachedInit;
	public bool m_isChargeRelease;
	public bool isChargePunchDone;

	public GameObject pointA; 
	public GameObject pointArmA; 
	public GameObject firstAChargeRestPoint; 
	public GameObject armAChargeRestPoint; 
	public GameObject armAChargeDonePoint;

	public GameObject pointB; 
	public GameObject pointArmB; 
	public GameObject firstBChargeRestPoint; 
	public GameObject armBChargeRestPoint; 
	public GameObject armBChargeDonePoint; 

	public GameObject FirstA;
	public GameObject ArmA;

	public GameObject FirstB;
	public GameObject ArmB;
	public GameObject crossHair;


   // public EaseType punchEaseType;

    private Vector3 mousePos;

	public Targetting targetCall;

    // Use this for initialization
    void Start()
    {
        //StartCoroutine(GOGO());
        //print(punchSwitchLR);
		print ("punchSwitchLR  : " + punchSwitchLR);
    }

    void Update()
    {
        //Check if our fist return to the right place after moving

		StartCoroutine (ChargingPunch ());
        StartCoroutine(FistOfTheNorthStar());
		StartCoroutine (FirstRotation ());

		StartCoroutine (DoneChargingPunchReleasing ());

        if (!didPunch && FirstA.transform.position != pointA.transform.position && isFixReturn && !Is_Charging() && isChargePunchDone)
        {
            FirstA.transform.position = Vector3.Lerp(FirstA.transform.position,
               pointA.transform.position, Time.deltaTime * returnSpeed);

				ArmA.transform.position = Vector3.Lerp(ArmA.transform.position,
			                                         pointArmA.transform.position, Time.deltaTime * returnSpeed);
			//FirstA.GetComponent<SphereCollider>().enabled = false;
        }

		if( isChargePunchDone || !isChargePunchDone)
		{
			ArmA.transform.position = Vector3.Lerp(ArmA.transform.position,
			                                       pointArmA.transform.position, Time.deltaTime * returnSpeed);

			FirstA.transform.position = Vector3.Lerp(FirstA.transform.position,
			                                         pointA.transform.position, Time.deltaTime * returnSpeed);

			FirstB.transform.position = Vector3.Lerp(FirstB.transform.position,
			                                         pointB.transform.position, Time.deltaTime * returnSpeed);
		}

		if (!didPunch && FirstB.transform.position != pointB.transform.position && isFixReturn && !Is_Charging() && m_isChargeRelease && isChargePunchDone)
        {
            FirstB.transform.position = Vector3.Lerp(FirstB.transform.position,
			                                         pointB.transform.position, Time.deltaTime * returnSpeed);

//			ArmB.transform.position = Vector3.Lerp(ArmB.transform.position,
//			                                       pointArmB.transform.position, Time.deltaTime * returnSpeed);

			//FirstB.GetComponent<SphereCollider>().enabled = false;
        }

    }



    /// <summary>
    /// This function will lerp the transform of this game object to an end position at a certain time
    /// </summary>
    /// <param name="thisTransform"></param>
    /// <param name="startPos"></param>
    /// <param name="endPos"></param>
    /// <param name="time"></param>
    /// <returns></returns>
     IEnumerator FromAToB(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        //iTweenExtensions.MoveTo(thisTransform.gameObject, endPos, time, 1, EaseType.spring);

        //gameObject.MoveTo(endPos, time, punchDelay, punchEaseType);

        yield return null;
        float i = 0.0f;
        float rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            //The way the smoothStep is set up allow us to have a ease in and ease out for a smoother lerp
            thisTransform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, i)));

            yield return null;
        }
    }

	IEnumerator FirstRotation ()
	{

		timeCounter += Time.fixedDeltaTime * speedCirc;
			
		float x, y, z;

		x = Mathf.Cos (timeCounter) * widthCirc;
		y =  Mathf.Sin (timeCounter) * heightCirc;
		z = 0;

		//transform.position = Quaternion.AngleAxis(timeCounter, Vector3.forward) * new Vector3(radius, 0f);

		if (targetCall.selectedTarget != null && !m_isCharging) 
		{
			FirstA.transform.position = new Vector3 (x, y, z) + pointA.transform.position;
			FirstB.transform.position = new Vector3 (-x, -y, z) + pointB.transform.position;
		}

		yield return null;
	}

	IEnumerator ChargingPunch()
	{
	
		if (Input.GetKey(keyCode) && targetCall.selectedTarget != null) 
		{
			m_chargeOccured = false;
			m_isChargeRelease = false;


			isChargePunchDone = false;

			//StartCoroutine( FromAToB(FirstB.transform, pointB.transform.position, firstBChargeRestPoint.transform.position, timeRate));
		//	StartCoroutine( FromAToB(ArmB.transform, armBChargeRestPoint.transform.position, pointArmB.transform.position, timeRate));

			//Put this part as an unlockable ability
//			if (m_isCharging) 
//			{
				//Here we can increase the damage of the bullet based on time and other stuff 
				chargeTime += Time.deltaTime * chargeSpeed;



				if(chargeTime >= .5f)
				{
				m_chargeOccured = true;
				FirstA.transform.position = Vector3.Lerp(FirstA.transform.position,
				                                         firstAChargeRestPoint.transform.position, chargeTime);
				ArmA.transform.position = Vector3.Lerp(ArmA.transform.position,
				                                       armAChargeRestPoint.transform.position, chargeTime);

//				FirstB.transform.position = Vector3.Lerp(FirstB.transform.position,
//				                                         firstBChargeRestPoint.transform.position, chargeTime);
//				ArmB.transform.position = Vector3.Lerp(ArmB.transform.position,
//				                                       armBChargeRestPoint.transform.position, chargeTime);

				//yield return FromAToB(FirstA.transform, pointA.transform.position, firstAChargeRestPoint.transform.position, chargeTime);
				//StartCoroutine( FromAToB(ArmA.transform, pointArmA.transform.position, armAChargeRestPoint.transform.position, chargeTime));


//				ArmA.transform.position = Vector3.Lerp(ArmA.transform.position,
//				                                       armAChargeRestPoint.transform.position, Time.deltaTime * returnSpeed);
				}

				//Could indicate to defaultBullet in bullet manager to pick up the speed here.
				if (chargeTime >= chargeLimit) 
				{

				chargeTime = chargeLimit;

					print("MaxChargeTimeReached");
					


				
				}
			}
		//}

		if(Input.GetKeyUp(keyCode) && targetCall.selectedTarget != null)
		{
				
			chargeTime = 0;
			m_isCharging = false;

			m_isChargeRelease = true;
			FirstA.GetComponent<SphereCollider>().enabled = true;


			//StartCoroutine( FromAToB(FirstA.transform, firstAChargeRestPoint.transform.position, crossHair.transform.position, timeRate));
			//StartCoroutine( FromAToB(ArmA.transform, armAChargeRestPoint.transform.position, armAChargeDonePoint.transform.position, timeRate));
			
			//StartCoroutine( FromAToB(FirstB.transform, firstBChargeRestPoint.transform.position, crossHair.transform.position, timeRate));
			//StartCoroutine( FromAToB(ArmB.transform, pointArmB.transform.position, armBChargeDonePoint.transform.position, timeRate));
		}

		yield return null;
	
	}

	IEnumerator  DoneChargingPunchReleasing()
	{
		if (m_chargeOccured) 
		{
			if (m_isChargeRelease && !didChargedFistReachedDest) 
			{
//			FirstA.transform.position = Vector3.MoveTowards (FirstA.transform.position,
//		                                         crossHair.transform.position, Time.deltaTime * chargeSpeed);
//			ArmA.transform.position = Vector3.MoveTowards (ArmA.transform.position,
//			                                               armAChargeDonePoint.transform.position, Time.deltaTime * chargeSpeed);

				yield return FromAToB (FirstA.transform, FirstA.transform.position, crossHair.transform.position, Time.deltaTime * chargeSpeed);
				yield return FromAToB (ArmA.transform, ArmA.transform.position, armAChargeDonePoint.transform.position, Time.deltaTime * chargeSpeed);

//				yield return FromAToB (FirstB.transform, FirstB.transform.position, crossHair.transform.position, Time.deltaTime * chargeSpeed);
//				yield return FromAToB (ArmB.transform, ArmB.transform.position, armBChargeDonePoint.transform.position, Time.deltaTime * chargeSpeed);
			}

			if (didChargedFistReachedDest && !didChargedFistReachedInit) 
			{	
				//m_isChargeRelease = true;	
				//print("reached");
				yield return FromAToB (FirstA.transform, FirstA.transform.position, pointA.transform.position, Time.deltaTime * returnSpeed);
				yield return FromAToB (ArmA.transform, ArmA.transform.position, pointArmA.transform.position, Time.deltaTime * returnSpeed);

//				yield return FromAToB (FirstB.transform, FirstB.transform.position,  pointB .transform.position, Time.deltaTime * chargeSpeed);
//				yield return FromAToB (ArmB.transform, ArmB.transform.position,  pointArmB.transform.position, Time.deltaTime * chargeSpeed);
			}
		
			if (didChargedFistReachedInit) 
			{
				m_isChargeRelease = false;
				didChargedFistReachedDest = false;
				didChargedFistReachedInit = false;
				isChargePunchDone = true;
			}
		}
		
		yield return null;
	}

	public bool Is_Charging()
	{
		if (Input.GetKey (keyCode) && chargeTime >= .5f)
		{ //&& chargeTime >= 0.15f) {
			m_isCharging = true;
		} 
		else if(Input.GetKeyUp(keyCode) && chargeTime <= .5f) //&& chargeTime <= 0.15f)
		{
			m_isCharging = false;
		}

		return m_isCharging;
	}

	public bool CanPunch()
	{

		if(!Is_Charging())
		{
			//reseting next PossibleShot
			//nextPossibleShootTime = 0;
			
			//True u Can shoot
			canPunch = true;
		}
		
		else
		{
			canPunch = false;
		}
		
		return canPunch;
	}
				
				///// <summary>
    ///// This is the punching function.
    ///// Fist goes from point A to B and 
    ///// </summary>
    ///// <returns></returns>
    IEnumerator FistOfTheNorthStar()
    {
        //bool throwedLeftPunch = false;
        //bool throwedRightPunch = false;



		if (Input.GetKeyDown(keyCode) && !Is_Charging()) //&& CanPunch())
		{
           // yield return FromAToB(transform.gameObject, PositionClicked(), PositionClicked(), timeRate);
           // yield return FromAToB(transform.gameObject, pointA.transform.position, pointA.transform.position, timeRate);

            punchSwitchLR = !punchSwitchLR;


			FirstA.GetComponent<SphereCollider>().enabled = true;
			FirstB.GetComponent<SphereCollider>().enabled = true;

            if (punchSwitchLR)
            {


						yield return FromAToB (FirstA.transform, pointA.transform.position, new Vector3 (PositionClicked ().x
						+ Random.Range (-0.5f, 0.5f), PositionClicked ().y, PositionClicked ().z
						+ Random.Range (-0.5f, -0.5f)), timeRate);
						yield return FromAToB (FirstA.transform, PositionClicked (), pointA.transform.position, timeRate);

			
				FirstA.GetComponent<SphereCollider>().enabled = !punchSwitchLR;
				FirstB.GetComponent<SphereCollider>().enabled = false;
            }
            else 
            {

						yield return FromAToB (FirstB.transform, pointB.transform.position, new Vector3 (PositionClicked ().x
						+ Random.Range (-0.5f, 0.5f), PositionClicked ().y, PositionClicked ().z
						+ Random.Range (-0.5f, -0.5f)), timeRate);
						yield return FromAToB (FirstB.transform, PositionClicked (), pointB.transform.position, timeRate);

				FirstA.GetComponent<SphereCollider>().enabled = false;
				FirstB.GetComponent<SphereCollider>().enabled = punchSwitchLR;
            }

           
            didPunch = true;
        }
		else
        {
//			FirstA.GetComponent<SphereCollider>().enabled = false;
//			FirstB.GetComponent<SphereCollider>().enabled = false;
            didPunch = false;

        }

        yield return null;
    }

    ///// <summary>
    ///// This function use a raycast to return a vector3 where the mouse click clicked in 3D else it just return the orginal 
    ///// position of the pointA
    ///// </summary>
    ///// <returns></returns>
    Vector3 PositionClicked()
    {

		if (limitRange == false)
		{
			//Converting Mouse Pos to 2D (vector2) World Pos/ Use ScreenToWorldPoint for 2D and use ScreenPointToRay for 3D
			Ray rayDest = Camera.main.ScreenPointToRay (Input.mousePosition);


			RaycastHit hit = new RaycastHit ();

			if (Physics.Raycast (rayDest, out hit, dist)) 
			{
				//Debug.Log(hit.transform.name);

				Debug.DrawRay (pointA.transform.position, hit.point, Color.red, 5.0f, false);
				Debug.DrawRay (pointB.transform.position, hit.point, Color.red, 5.0f, false);
				return new Vector3 (hit.point.x, transform.position.y, hit.point.z);
			} else
				return pointA.transform.position;
		} 
		else 
		{
			return crossHair.transform.position;
		}
    }

    #region OG From A to B
    //public GameObject pointB;

    //IEnumerator GOGO()
    //{
    //    var pointA = transform.position;
    //    while (true)
    //    {
    //        yield return StartCoroutine(MoveObject(transform, pointA, pointB.transform.position, timeRate));
    //        yield return StartCoroutine(MoveObject(transform, pointB.transform.position, pointA, timeRate));
    //    }
    //}

    //IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    //{
    //    var i = 0.0f;
    //    var rate = 1.0f / time;
    //    while (i < 1.0f)
    //    {
    //        i += Time.deltaTime * rate;
    //        thisTransform.position = Vector3.Lerp(startPos, endPos, i);
    //        yield return null;
    //    }
    //}
    #endregion
}
