﻿using UnityEngine;
using System.Collections;

public class PlayerMove: Entity
{


    public float speed;
    public float movementDirection;
    public GUIText healthTxt;

    public float fireRateTimer = 0;
    public float fireRate = 0.5f;
    public float force;
    public float bulletForce;
    public float jumpPower;
    public float groundCheckRadius;
    public Transform groundCheck;
    public LayerMask layerMask;
    public GameObject bullet;
    public Transform shootingPoint;
    public AudioClip beamFX;
    public AudioClip jumpFX;

    //Private stuff
    //private Vector3 movementDirection;
    private CharacterController controller;
    private string axisName = "Horizontal";
    private Transform myTransform;
    private Animator anim;
    //need to check
    public bool isGrounded;
    public bool automaticJumping;



    //MOTHER OF ALL THE STARTS
    void Awake()
    {


    }


    // Use this for initialization
    void Start()
    {
       // anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        fireRateTimer += Time.deltaTime;
        updateHP();

        if (automaticJumping && isGrounded)
        {

            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower));
            //GetComponent<AudioSource>().clip = jumpFX;
            //GetComponent<AudioSource>().Play();
           // anim.SetBool("Ground", isGrounded);
        }

        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpPower));
           // anim.SetBool("Ground", isGrounded);
        }

        if (Input.GetMouseButtonDown(0))
        {
            shooting();
        }
    }

    void FixedUpdate()
    {
        //Ground checking 2D
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.5f, layerMask);
       // anim.SetBool("Ground", isGrounded);


        //     Cant move in the air. Just add one more bool if you want to enable this part as one of the gameplaY Elements.
        //		if(isGrounded)
        //		{
        //	
        //		movement();
        //
        //		}
       // anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);
        //Sprite facing left or right based on the local scale.
        if (movementDirection > 0)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = -1;
            transform.localScale = newScale;

        }

        else if (movementDirection < 0)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = 1;
            transform.localScale = newScale;
        }

        movement();
    }

    void movement()
    {

        movementDirection = Input.GetAxis(axisName);

        //anim.SetFloat("Speed", Mathf.Abs(movementDirection));
        GetComponent<Rigidbody2D>().velocity = new Vector2(movementDirection * speed * Time.deltaTime, GetComponent<Rigidbody2D>().velocity.y);

    }

    void shooting()
    {
        //fireRateTimer += Time.deltaTime;

        if (fireRateTimer >= fireRate)
        {

            fireRateTimer -= fireRate;
            GetComponent<AudioSource>().clip = beamFX;
            GetComponent<AudioSource>().Play();
            GameObject obj = ((GameObject)Instantiate(bullet, shootingPoint.position, Quaternion.identity)).gameObject;
            //Camera.main.GetComponent<CameraShake>().Shaker();
            obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletForce, obj.GetComponent<Rigidbody2D>().velocity.y));
            obj.GetComponent<Rigidbody2D>().AddForceAtPosition(obj.transform.right * bulletForce, obj.transform.position);
            Destroy(obj, 2f);

        }
    }

    void updateHP()
    {
        if (health <= 2)
        {
            health = 0;
        }
       // healthTxt.text = "HP: " + health + "%";
    }

    void OnTriggerEnter2D(Collider2D otherObject)
    {

        if (otherObject.tag == "Enemy")
        {
            //PlayerTakeDamage(1);
        }

    }

}
