﻿using UnityEngine;
using System.Collections;

public class TargetableAwareness : MonoBehaviour 
{
	private Targetting targettingBehavior;
	private MeshRenderer compareRend;
	private GameObject playerObj;
	public MeshRenderer LockMarker;

	// Use this for initialization
	void Start () 
	{
		LockMarker.GetComponent<MeshRenderer> ();
		LockMarker.enabled = false;

		if (GameObject.FindWithTag ("Player"))
		{
			
			playerObj = GameObject.FindGameObjectWithTag("Player");
			print (playerObj.name);
			targettingBehavior = playerObj.GetComponent<Targetting> ();
			//targettingBehavior.selectedTarget.GetComponent<MeshRenderer> ();
		
				
			print ("Found Player");

			CheckStatus ();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void CheckStatus()
	{
		if (targettingBehavior.CurrentSelectedTargetMeshRenderer () != null)
		{
			if (targettingBehavior.CurrentSelectedTargetMeshRenderer ().receiveShadows == false) 
			{
				LockMarker.enabled = true;
				print (" enemy transform is the current selected target");
			} 
		}
//		else 
//		{
//			//LockMarker.GetComponent<Renderer> ();
//			LockMarker.enabled = false;
//			print ("Enemy is not a target anymore");
//		}
	}

}
