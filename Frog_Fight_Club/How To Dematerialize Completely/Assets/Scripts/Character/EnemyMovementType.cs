﻿using UnityEngine;
using System.Collections;

public class EnemyMovementType : MonoBehaviour 
{
	public Rigidbody rb;
	
	public Transform target;
	public float speed;
	public float amplitude;
	public float frequency;
	public float offSetSinPhase = 0.0f;
	
	private float startTime;
	private Vector3 direction;
	private Vector3 fixDir;
	private Vector3 orthogonal;

	public enum Enemy_Movement_Type
	{
		ZigZagSine,
		FlyLike,
		StraightFollow

	}

	public Enemy_Movement_Type moveType;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{

		switch (moveType) 
		{

		case Enemy_Movement_Type.StraightFollow:

			break;

		case Enemy_Movement_Type.FlyLike:
			
			break;

		case Enemy_Movement_Type.ZigZagSine:
			
			break;

		default:

			break;

		}
	
	}

	
	/// <summary>
	/// This function will lerp the transform of this game object to an end position at a certain time
	/// </summary>
	/// <param name="thisTransform"></param>
	/// <param name="startPos"></param>
	/// <param name="endPos"></param>
	/// <param name="time"></param>
	/// <returns></returns>
	IEnumerator FromAToB(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
	{
		//iTweenExtensions.MoveTo(thisTransform.gameObject, endPos, time, 1, EaseType.spring);
		
		//gameObject.MoveTo(endPos, time, punchDelay, punchEaseType);
		
		yield return null;
		float i = 0.0f;
		float rate = 1.0f / time;
		while (i < 1.0f)
		{
			i += Time.deltaTime * rate;
			//The way the smoothStep is set up allow us to have a ease in and ease out for a smoother lerp
			thisTransform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, i)));
			
			yield return null;
		}
	}

	/// <summary>
	/// Zigs the zag sine.
	/// </summary>
	/// <returns>The zag sine.</returns>
	void ZigZagSine()
	{
		fixDir = new Vector3 (target.position.x, 
		                      this.transform.position.y, 
		                      target.position.z);
		
		transform.LookAt (fixDir);
		

			float t = Time.time - startTime;
			direction = (target.position - transform.position).normalized;
			direction.y = 0;
			orthogonal = new Vector3 (-direction.z, 0, direction.x); //inversing dir 
			
			rb.velocity = direction * speed + orthogonal * amplitude * Mathf.Sin (frequency * t + offSetSinPhase);
	}

	/// <summary>
	/// Organize all enemy movement type into a class
	/// Flies like movement(Random).
	/// </summary>
	/// <returns>The like movement.</returns>
	void FlyLikeMovement()
	{

			transform.position = Vector3.Lerp (transform.position, transform.position 
			                                   + new Vector3 ((Random.value - 0.5f) * speed,
			               0, (Random.value - 0.5f) * speed), Time.time);

	}



}
