using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemy : MonoBehaviour 
{
	public bool CanChase;
	public bool ChaseSin;
	public bool FlyLikeMove;
	public GameObject target;
	public float amplitudeSin;
	public float speedMovement = 5.0f;
	public float speedFlyMovement = 5.0f;
	//	public float randMinFlyMove;
	//	public float randMaxFlyMove;
	public float rotSpeed = 2.0f;
	public float chaseHeight = 1.074f;
	public float warningJumpHeight = 4.0f;
	
	public float MoveSpeed = 5.0f;
	public float chargeSpeed = 5.0f;
	public float chargeExcTime = 0.5f;
	public float preChargeDist = 5.0f;
	public float extraChargeDist = 5.0f;
	
	
	public float frequency = 20.0f;  // Speed of sine movement
	public float magnitude = 0.5f;   // Size of sine movement
	private Vector3 axis;
	
	private Vector3 pos;
	public Vector3 lookTargetPos;
	public Vector3 offsetB;
	public Quaternion rotateToTarget;
	
	public FollowSine followSine;
	public List<Renderer> OldRends;
	public List<Renderer> NewRends;
	
	
	public Color flashColor;
	
	public Material material;
	
	public enum Enemy_State
	{
		STATE_DAMAGED,
		STATE_PRE_ATTACK,
		STATE_ATTACKING,
		STATE_CHASE,
		STATE_GRABBED,
		STATE_DEATH,
		
	};
	
	Enemy_State state;
	
	public enum Enemy_Type
	{
		DRAGONFLY,
		BEAR,
		SEEDLING,
		PLANT,
		
	}
	
	Enemy_Type enemyType;
	
	// Use this for initialization
	void Start () 
	{
		OldRends = new List <Renderer>();
		NewRends = new List<Renderer> ();
		OldRends = Helper.getChildRenderer (this.gameObject);
		
		//		foreach (Color col in thisObjColor) 
		//		{
		//			print (col);
		//		}
	}
	
	// Update is called once per frame
	
	void Update () 
	{
		//Chase ();
		//FlyLikeMovement ();
		StartCoroutine(ChargeAttack());
		
		if(Input.GetKeyDown(KeyCode.Space))
		{
			
//			foreach(Renderer rend in OldRends)
//			{
//				rend.material.shader = Shader.Find("Specular");
//				rend.material.SetColor("_Color", Color.red);
//				NewRends.Add();
//			}
			
			//this.gameObject.GetComponentsInChildren<MeshRenderer>();
			//WarningBeforeAttack ();
		}
	}
	
	void EnemyState()
	{
		switch (state) 
		{
			
			/// 0. If player hit enemy with normal attacks when in pre-attack state, player normal attack won't stun the enemy
			/// 1. Will not move for a half of seconds (Immobile) - Use coroutine to gradually lower speed and then lock x,y,z
			/// 2. Basic one will flash white before attacking (Color change) - Use coroutine to flash for 0.3f seconds 
		case  Enemy_State.STATE_PRE_ATTACK:
			
			break;
			
			/// 0. If player attack enemy with normal attacks during attacking, his normal attack won't stun the enemy
		case Enemy_State.STATE_ATTACKING:
			
			break;
			
		case Enemy_State.STATE_CHASE:
			
			break;
			
		case Enemy_State.STATE_DAMAGED:
			
			break;
			
		case Enemy_State.STATE_DEATH:
			
			break;
			
			/// 0. Will not be able to move/chase/attack etc.. until released
		case Enemy_State.STATE_GRABBED:
			
			break;
			
			
		default:
			
			break;
			
		}
	}
	
	/// <summary>
	/// Looks the target direction.
	/// </summary>
	/// <returns>The target direction.</returns>
	void LookTargetDirection()
	{
		if (target != null) 
		{
			lookTargetPos = target.transform.position - transform.position;
			lookTargetPos.y = 0;
			rotateToTarget = Quaternion.LookRotation(lookTargetPos);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotateToTarget, 1);
		}
	}
	
	
	
	void Chase()
	{
		if (CanChase) 
		{
			
			
			
			if (ChaseSin) 
			{
				
			} 
			else 
			{
				
			}
		}
		
		
	}
	
	/// <summary>
	/// Organize all enemy movement type into a class
	/// Flies like movement(Random).
	/// </summary>
	/// <returns>The like movement.</returns>
	void FlyLikeMovement()
	{
		if (FlyLikeMove) 
		{
			transform.position = Vector3.Lerp (transform.position, transform.position 
			                                   + new Vector3 ((Random.value - 0.5f) * speedFlyMovement,
			               0, (Random.value - 0.5f) * speedFlyMovement), Time.time);
		}
	}
	
	
	
	/// <summary>
	/// This function will lerp the transform of this game object to an end position at a certain time
	/// </summary>
	/// <param name="thisTransform"></param>
	/// <param name="startPos"></param>
	/// <param name="endPos"></param>
	/// <param name="time"></param>
	/// <returns></returns>
	IEnumerator FromAToB(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
	{
		//iTweenExtensions.MoveTo(thisTransform.gameObject, endPos, time, 1, EaseType.spring);
		
		//gameObject.MoveTo(endPos, time, punchDelay, punchEaseType);
		
		yield return null;
		float i = 0.0f;
		float rate = 1.0f / time;
		while (i < 1.0f)
		{
			i += Time.deltaTime * rate;
			//The way the smoothStep is set up allow us to have a ease in and ease out for a smoother lerp
			thisTransform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, i)));
			
			yield return null;
		}
	}
	
	/// <summary>
	/// Warnings the before attack. Add dfferent type of warning later. Flash before attack, Jump, BackAway, Charging
	/// </summary>
	/// <returns>The before attack.</returns>
	IEnumerator WarningBeforeAttack()
	{
		
		CanChase = false;
		
		//		yield return FromAToB (FirstA.transform, pointA.transform.position, new Vector3 (PositionClicked ().x
		//			+ Random.Range (-0.5f, 0.5f), PositionClicked ().y, PositionClicked ().z
		//			+ Random.Range (-0.5f, -0.5f)), timeRate);
		//		yield return FromAToB (FirstA.transform, PositionClicked (), pointA.transform.position, timeRate);
		
		return null;
	}
	
	/// <summary>
	/// Attack an instance. Add different type of Attack later. Quick charge, Shoot bullet, laser, Jump on player
	/// </summary>
	void Attack()
	{
		
	}
	
	IEnumerator ChargeAttack()
	{
		while (true) 
		{
			
			//		float initFollowSpeed = followSine.speed;
			//
			//		this.transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z - preChargeDist);
			//
			//		followSine.speed = 0;
			//
			//		yield return new WaitForRealSeconds (1.2f);
			//
			//		followSine.speed = initFollowSpeed;
			//
			//
			//
			//			if (target != null) 
			//			{
			//				yield return FromAToB (this.transform, transform.position, new Vector3 (target.transform.position.x, transform.position.y,
			//				target.transform.position.z + extraChargeDist) , chargeExcTime);
			//
			//			} 
			
			yield return new WaitForRealSeconds (Random.Range(1, 4));
		}
		
	}
	
	void Health()
	{
		
	}
	
	/// <summary>
	/// Takes the damage. Normal mob position will freeze when getting hit by leftClick punches. Fly away by charging attacks. Duplicate.
	/// </summary>
	/// <returns>The damage.</returns>
	void TakeDamage()
	{
		
	}
	
	void HandleActionBoolean()
	{
		
	}
	
	/// <summary>
	/// Make a state machine
	/// States this instance.
	/// Chase
	/// Slow down briefly after attacking to reposition himself
	/// Run Away (shooters)
	/// Getting Damaged
	/// </summary>
	void CurrentState()
	{
		
	}
	
}
