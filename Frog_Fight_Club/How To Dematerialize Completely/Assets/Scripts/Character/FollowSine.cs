﻿using UnityEngine;
using System.Collections;

public class FollowSine : MonoBehaviour 
{
	public bool shouldFollow;
	public Rigidbody rb;

	public Transform target;
	public float speed;
	public float amplitude;
	public float frequency;
	public float offSetSinPhase = 0.0f;

	private float startTime;
	private Vector3 direction;
	private Vector3 fixDir;
	private Vector3 orthogonal;

	void Start() 
	{
		startTime = Time.time;

	}

	void FixedUpdate () 
	{
		fixDir = new Vector3 (target.position.x, 
		                      this.transform.position.y, 
		                      target.position.z);
		
		transform.LookAt (fixDir);

		if (shouldFollow) 
		{
			float t = Time.time - startTime;
			direction = (target.position - transform.position).normalized;
			direction.y = 0;
			orthogonal = new Vector3 (-direction.z, 0, direction.x); //inversing dir 





			rb.velocity = direction * speed + orthogonal * amplitude * Mathf.Sin (frequency * t + offSetSinPhase);
		}
	}
}