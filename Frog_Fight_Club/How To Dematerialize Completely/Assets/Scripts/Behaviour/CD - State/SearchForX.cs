﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SearchForX : IState 
{
	public string searchTag = "";
	private LayerMask searchLayer;
	private GameObject targetGameObject;
	private float searchRadius;
	private GameObject ownerGameObject;
	private Transform tTransform;

	public SearchForX(LayerMask searchLayer, GameObject owner, float searchRadius, string searchTag)
	{
		this.searchLayer = searchLayer;
		this.ownerGameObject = ownerGameObject;
		this.searchRadius = searchRadius;
		this.searchTag = searchTag;
	}

	public void Enter()
	{

	}
	
	public void Execute()
	{
		var HitObjects = Physics.OverlapSphere (this.ownerGameObject.transform.position, 
		                                       searchRadius);


		for (int i = 0; i < HitObjects.Length; i++) 
		{
			if(HitObjects[i].CompareTag(searchTag))
			{

				//Move to target location but excute it here, it is not the purpose of this class
				//

			
			}

			break;
		}
	}
	
	public void Exit()
	{

	}
}
