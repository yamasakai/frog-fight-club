﻿using UnityEngine;
using System.Collections;

/// <summary>
/// I state. Each state has three functions (Enter, Execute, Exit). We can call it the triple E trifecta.
/// </summary>
public interface IState
{
	void Enter();

	void Execute();

	void Exit();
}
