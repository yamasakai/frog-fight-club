﻿using UnityEngine;
using System.Collections;

/// <summary>
/// State machine. His role is to change state. Look at him like a console, and state are like cds.
/// He is aware of the currently running state and previous one.
/// </summary>
public class StateMachine : MonoBehaviour 
{
	private IState currentRunningState;
	private IState previousState;


	/// <summary>
	/// Changes the state.
	/// </summary>
	/// <param name="newState">New state.</param>
	public void ChangeState(IState newState)
	{
		if(this.currentRunningState != null)
		{
		
			this.currentRunningState.Exit();
		}

		this.previousState = this.currentRunningState;

		this.currentRunningState = newState;
		this.currentRunningState.Enter();

	}

	/// <summary>
	/// Executes the state update. 
	/// </summary>
	public void ExecuteStateUpdate()
	{
		var runningState = this.currentRunningState;

		if (runningState != null) 
		{
			runningState.Execute();
		}
	}

	/// <summary>
	/// Switch to previous state.
	/// </summary>
	public void SwitchToPreviousState()
	{
		//This
//		this.currentRunningState.Exit ();
//		currentRunningState = this.previousState;
//		this.currentRunningState.Enter ();

		//Or

		//This
		ChangeState (this.previousState);
	}

}
