﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Functions : MonoBehaviour {

    public static float ParabolaBob(float _time)
    {
        float value = Mathf.Clamp((-Mathf.Pow(_time, 2) + (0.3f * _time)) * 10, 0f, 1f);
        return value;
    }
    public static float RandomValue(float Range)
    {
        float value = Random.Range(-Range, Range);
        return value;
    }
    
    public static Texture2D TakeScreenshot(int _width, int _height, Camera _screenshotCamera)
    {
        if (_width <= 0 || _height <= 0) return null;
        if (_screenshotCamera == null) _screenshotCamera = Camera.main;
        Texture2D screenshot = new Texture2D(_width, _height, TextureFormat.RGB24, false);
        RenderTexture renderTex = new RenderTexture(_width, _height, 24);
        _screenshotCamera.targetTexture = renderTex;
        _screenshotCamera.Render();
        RenderTexture.active = renderTex;
        screenshot.ReadPixels(new Rect(0, 0, _width, _height), 0, 0);
        screenshot.Apply(false);
        _screenshotCamera.targetTexture = null;
        RenderTexture.active = null;
        Destroy(renderTex);
        return screenshot;
    }

    public static Vector3[] MakeSmoothCurve(Vector3[] _arrayToCurve, float _smoothness)
    {
        List<Vector3> points;
        List<Vector3> curvedPoints;
        int pointsLength = 0;
        int curvedLength = 0;

        _smoothness = Mathf.Clamp(_smoothness, 1f, 10f);

        pointsLength = _arrayToCurve.Length;

        curvedLength = (pointsLength * Mathf.RoundToInt(_smoothness)) - 1;
        curvedPoints = new List<Vector3>(curvedLength);

        float t = 0f;
        for (int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
        {
            t = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);

            points = new List<Vector3>(_arrayToCurve);

            for (int j = pointsLength - 1; j > 0; j--)
            {
                for (int i = 0; i < j; i++)
                {
                    points[i] = (1 - t) * points[i] + t * points[i + 1];
                }
            }

            curvedPoints.Add(points[0]);
        }

        return (curvedPoints.ToArray());
    }
    public static Color RandomColor()
    {
        Color MyNewColor = new Color();
        int ColorNumber = Random.Range(0, 10);
        switch (ColorNumber)
        {
            case 1:
                MyNewColor = Color.red;
                break;
            case 2:
                MyNewColor = Color.magenta;
                break;
            case 3:
                MyNewColor = Color.grey;
                break;
            case 4:
                MyNewColor = Color.cyan;
                break;
            case 5:
                MyNewColor = Color.green;
                break;
            case 6:
                MyNewColor = new Color32(1, 116, 10, 255);  //dark green
                break;
            case 7:
                MyNewColor = Color.blue;
                break;
            case 8:
                MyNewColor = new Color32(224, 103, 10, 255);  //orange
                break;
            case 9:
                MyNewColor = new Color32(255, 1, 153, 255);  //pink
                break;
            default:
                MyNewColor = Color.red;
                break;
        }
        return MyNewColor;
    }

}
