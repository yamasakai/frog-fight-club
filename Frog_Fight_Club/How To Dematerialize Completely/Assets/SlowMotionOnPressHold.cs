﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionOnPressHold : MonoBehaviour
{
    public LineRendererDrag lineRendererDrag;
    public bool useSlowMotion;
    public float targetTimeScaleValueMin = 0.02f;

    // Use this for initialization
    void Start ()
    {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (useSlowMotion && lineRendererDrag.isDragging)
        {
            if (Input.GetMouseButton(1) || Input.GetMouseButton(0))
            {
                //Slow mo
                Time.timeScale = 0.15f;
               
                Time.fixedDeltaTime = targetTimeScaleValueMin * Time.timeScale;
            }
            else if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(0))
            {
                //slow mooo back to normal
                Time.timeScale = 1;
               
                Time.fixedDeltaTime = targetTimeScaleValueMin * Time.timeScale;
            }

        }

    }


}
